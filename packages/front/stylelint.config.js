//First off we need to install two extensions for Visual Studio Code

//https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint
//https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss

// Finally, you have to put these three lines into your settings.json of Visual Studio Code. They disable all built-in CSS validations. These are now handled by Stylelint.

// "css.validate": false,
// "less.validate": false,
// "scss.validate": false

// Article link => https://andrich.me/vscode-stylelint-tailwind-css-are-love?cn-reloaded=1

module.exports = {
  extends: ["stylelint-config-standard"],
  rules: {
    "at-rule-no-unknown": [
      true,
      {
        ignoreAtRules: [
          "tailwind",
          "apply",
          "variants",
          "responsive",
          "screen",
        ],
      },
    ],
    "declaration-block-trailing-semicolon": null,
    "no-descending-specificity": null,
    "property-no-unknown": null,
  },
};
