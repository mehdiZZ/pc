const plugin = require("tailwindcss/plugin");

module.exports = {
  mode: "jit",
  purge: {
    enabled: true, //process.env.NODE_ENV === "production",
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
    options: {
      safelist: [
        /^sm:w-/,
        /^md:w-/,
        /^lg:w-/,
        /^xl:w-/,
        /^2xl:w-/,
        /^flex/,
        /^order-/,
      ],
    },
  },
  // target: "relaxed",
  prefix: "",
  important: false,
  separator: ":",
  theme: {
    colors: {
      //theme colors:
      primary: {
        DEFAULT: "var(--color-primary)",
        inverse: "var(--color-primary-inverse)",
      },
      secondary: {
        DEFAULT: "var(--color-secondary)",
        inverse: "var(--color-secondary-inverse)",
      },
      tertiary: {
        DEFAULT: "var(--color-tertiary)",
        inverse: "var(--color-tertiary-inverse)",
      },
      //
      transparent: "transparent",
      current: "currentColor",
      content: {
        DEFAULT: "var(--color-content)",
      },
      white: {
        DEFAULT: "#f4f4f5",
        transparent: "rgba(255,255,255,.07)",
      },
      violetDeep: "#1B1E3C",
      gray: {
        DEFAULT: "#595D78",
        100: "#f8f9fa",
        200: "#e9ecef",
        300: "#d6dee5",
        400: "#c1ccd3",
        500: "#a3aeb7",
        600: "#798892",
        700: "#495057",
        800: "#3c484f",
        900: "#29323a",
      },
      // text color based on dark/light mode
      inverse: {
        higher: "var(--color-inverse-higher)",
        high: "var(--color-inverse-high)",
        medium: "var(--color-inverse-medium)",
      },
      black: {
        DEFAULT: "#000",
        transparent: "rgba(0,0,0,.8)",
      },
      red: {
        DEFAULT: "#F45722",
        dark: "#d04f4f",
      },
      blue: {
        DEFAULT: "#1870DC",
        dark: "#145eba",
        link: "#1870dc",
      },
      lime: {
        DEFAULT: "#8cbf26",
      },
      pink: { DEFAULT: "#e671b8" },
      purple: { DEFAULT: "#a700ae" },
      brown: { DEFAULT: "#a05000" },
      teal: {
        DEFAULT: "#4ebfbb",
        dark: "#3da9a6",
      },
      green: {
        DEFAULT: "#58D777",
        dark: "#39d05e",
      },
      orange: {
        DEFAULT: "#f0af03",
        dark: "#ca9303",
      },
    },
    boxShadow: {
      DEFAULT: "0 0.5rem 1rem rgba(0,0,0,.175)",
      sm: "0 1px 2px 0 rgba(0, 0, 0, 0.05)",
      md:
        "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
      lg:
        "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
      xl:
        "0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)",
      "2xl": "0 25px 50px -12px rgba(0, 0, 0, 0.25)",
      content: "0 25px 20px -20px rgba(0,0,0,0.1), 0 0 15px rgba(0,0,0,0.06)",
      none: "none",
      outline: "0 0 0 2px rgba(255,255,225,.15)",
    },
    flexGrow: {
      0: "0",
      DEFAULT: "1",
      max: "100",
    },
    fontFamily: {
      sans: [
        "iranyekan",
        "system-ui",
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
      serif: [
        "ui-serif",
        "Georgia",
        "Cambria",
        '"Times New Roman"',
        "Times",
        "serif",
      ],
      mono: [
        "ui-monospace",
        "SFMono-Regular",
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace",
      ],
    },
    fontSize: {
      xxs: ["0.6rem", { lineHeight: ".9rem" }],
      xs: ["0.75rem", { lineHeight: "1rem" }],
      sm: ["0.875rem", { lineHeight: "1.25rem" }],
      base: ["1rem", { lineHeight: "1.5rem" }],
      lg: ["1.125rem", { lineHeight: "1.75rem" }],
      xl: ["1.25rem", { lineHeight: "1.75rem" }],
      "2xl": ["1.5rem", { lineHeight: "2rem" }],
      "3xl": ["1.875rem", { lineHeight: "2.25rem" }],
      "4xl": ["2.25rem", { lineHeight: "2.5rem" }],
      "5xl": ["3rem", { lineHeight: "1" }],
      "6xl": ["3.75rem", { lineHeight: "1" }],
      "7xl": ["4.5rem", { lineHeight: "1" }],
      "8xl": ["6rem", { lineHeight: "1" }],
      "9xl": ["8rem", { lineHeight: "1" }],
    },
    zIndex: {
      auto: "auto",
      0: "0",
      10: "10",
      20: "20",
      30: "30",
      40: "40",
      50: "50",
      max: "9999999",
    },
  },
  // variants: {
  //   extends: {
  //     // remember to remove this
  //     backgroundColor: ["checked"],
  //     borderColor: ["checked"],
  //     fontWeight: ["hover", "focus"],
  //   },
  // },
  corePlugins: {},
  plugins: [
    plugin(function ({ addUtilities }) {
      const filters = {
        ".filter-dark": {
          filter: "brightness(85%)",
        },
        ".filter-backdrop": {
          backdropFilter: "blur(3px)",
        },
      };
      addUtilities(filters, ["hover"]);
    }),
  ],
};
