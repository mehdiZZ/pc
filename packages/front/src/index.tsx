import { StrictMode } from "react";
import ReactDOM from "react-dom";
import "./assets/styles/tailwind.css";
import App from "./App";
import {
  setCssVariables,
  getStorageSetting,
  setStorageSetting,
  getThemeFromStorage,
  getLangFromStorage,
} from "utils";
import { themes, langs } from "config";
//
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { en, fa } from "locales";

// get data from local storage and save it in memory
if (!getStorageSetting("theme")) {
  setStorageSetting("theme", themes[0]);
  setCssVariables(themes[0]);
} else {
  setCssVariables(getThemeFromStorage());
}

if (!getStorageSetting("lang")) {
  setStorageSetting("lang", langs[0]);
}

const { lang, dir } = getLangFromStorage();

i18n.use(initReactI18next).init(
  {
    resources: {
      en,
      fa,
    },
    fallbackLng: "fa",
    debug: false,
    lng: lang,
    ns: ["common", "routes", "forms", "crud"],
    defaultNS: "common",
    saveMissing: false,
    keySeparator: false,
    interpolation: {
      escapeValue: false,
    },
  },
  function () {
    document.documentElement.setAttribute("lang", lang);
    document.documentElement.setAttribute("dir", dir);
    document.body.classList.add(dir);
  }
);

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById("root")
);
