import clsx from "clsx";
import { HrProps } from "./types";

/**
 * @param color - {string} separator color
 * @param width - {string} separator width
 * @param verticalMargin - {string} separator margin
 */
export const Hr = ({
  className,
  color = "border-inverse-medium",
  width = "w-full",
  verticalMargin = "my-4",
  ...props
}: HrProps) => {
  return (
    <hr {...props} className={clsx(verticalMargin, color, width, className)} />
  );
};
