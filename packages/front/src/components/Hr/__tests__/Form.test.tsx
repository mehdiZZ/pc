import { render } from "utils";
import { Hr } from "../Hr";

describe("👉 <Hr>", () => {
  test("Renders hr tag", () => {
    const { container } = render(<Hr />);
    expect(container.querySelector("hr")).toBeInTheDocument();
  });
});
