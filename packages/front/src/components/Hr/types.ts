export type HrProps = React.PropsWithoutRef<JSX.IntrinsicElements["hr"]> & {
  width?:
    | "w-full"
    | "w-1/4"
    | "w-2/4"
    | "w-3/4"
    | "w-4"
    | "w-5"
    | "w-6"
    | "w-8"
    | "w-10"
    | "w-12"
    | "w-16"
    | "w-20";

  color?:
    | "border-primary"
    | "border-secondary"
    | "border-tertiary"
    | "border-red"
    | "border-teal"
    | "border-orange"
    | "border-white"
    | "border-green"
    | "border-gray"
    | "border-black"
    | "border-black-transparent"
    | "border-inverse-higher"
    | "border-inverse-hight"
    | "border-inverse-medium";

  verticalMargin?:
    | "my-0"
    | "my-1"
    | "my-2"
    | "my-3"
    | "my-4"
    | "my-5"
    | "my-6"
    | "my-8"
    | "mb-1"
    | "mb-2"
    | "mb-3"
    | "mb-4"
    | "mb-5"
    | "mb-6"
    | "mb-8";
};
