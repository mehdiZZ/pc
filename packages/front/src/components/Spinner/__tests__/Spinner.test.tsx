import { render } from "utils";
import { Spinner } from "../Spinner";

describe("👉 <Spinner>", () => {
  test("Renders in correct width and height", () => {
    const { container } = render(
      <Spinner data-testid="spinner" width="300" height="400"></Spinner>
    );

    expect(container.querySelector("svg")).toHaveAttribute("width", "300");
    expect(container.querySelector("svg")).toHaveAttribute("height", "400");
  });
});
