import { useMemo } from "react";
import { SpinnerProps } from "./types";
import { getSpinner } from "./utils";
import clsx from "clsx";
import { useRoundedClass } from "utils";

/**
 * @param name - {string} spinner name
 * @param width - {string | number} svg width
 * @param height - {string | number} svg height
 */
export const Spinner = ({
  name = "gooey",
  width = "60",
  height = "60",
  ...props
}: SpinnerProps) => {
  let spinner = useMemo(() => getSpinner({ width, height, name }), [
    width,
    height,
    name,
  ]);

  const classes = [
    "spinner flex z-max justify-center items-center absolute inset-0 h-full bg-content filter-backdrop",
    useRoundedClass(),
  ];

  return (
    <div {...props} className={clsx(classes)}>
      {spinner}
    </div>
  );
};
