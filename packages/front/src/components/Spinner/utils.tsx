import { SpinnerProps } from "./types";

export const DoubleRingSpinner = (
  width: string | number,
  height: string | number
) => {
  return (
    <svg
      width={width}
      height={height}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
      className="lds-double-ring"
    >
      <circle
        cx="50"
        cy="50"
        fill="none"
        strokeLinecap="round"
        r="25"
        strokeWidth="5"
        stroke="#ff7c81"
        strokeDasharray="39.269908169872416 39.269908169872416"
        transform="rotate(360 -8.10878e-8 -8.10878e-8)"
      >
        <animateTransform
          attributeName="transform"
          type="rotate"
          calcMode="linear"
          values="0 50 50;360 50 50"
          keyTimes="0;1"
          dur="1s"
          begin="0s"
          repeatCount="indefinite"
        ></animateTransform>
      </circle>
      <circle
        cx="50"
        cy="50"
        fill="none"
        strokeLinecap="round"
        r="19"
        strokeWidth="5"
        stroke="#fac090"
        strokeDasharray="29.845130209103033 29.845130209103033"
        strokeDashoffset="29.845130209103033"
        transform="rotate(-360 -8.10878e-8 -8.10878e-8)"
      >
        <animateTransform
          attributeName="transform"
          type="rotate"
          calcMode="linear"
          values="0 50 50;-360 50 50"
          keyTimes="0;1"
          dur="1s"
          begin="0s"
          repeatCount="indefinite"
        ></animateTransform>
      </circle>
    </svg>
  );
};

export const GooeySpinner = (
  width: string | number,
  height: string | number
) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      className="lds-gooeyring"
      preserveAspectRatio="xMidYMid"
      viewBox="0 0 100 100"
      style={{
        WebkitAnimationPlayState: "running",
        animationPlayState: "running",
        WebkitAnimationDelay: "0s",
        animationDelay: "0s",
        background: "none",
      }}
    >
      <defs
        style={{
          WebkitAnimationPlayState: "running",
          animationPlayState: "running",
          WebkitAnimationDelay: "0s",
          animationDelay: "0s",
        }}
      >
        <filter
          id="gooey"
          width="300%"
          height="300%"
          x="-100%"
          y="-100%"
          colorInterpolationFilters="sRGB"
          style={{
            WebkitAnimationPlayState: "running",
            animationPlayState: "running",
            WebkitAnimationDelay: "0s",
            animationDelay: "0s",
          }}
        >
          <feGaussianBlur
            in="SourceGraphic"
            stdDeviation="4"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          ></feGaussianBlur>
          <feComponentTransfer
            result="cutoff"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          >
            <feFuncA intercept="-5" slope="10" type="linear"></feFuncA>
          </feComponentTransfer>
        </filter>
      </defs>
      <g
        filter="url(#gooey)"
        style={{
          WebkitAnimationPlayState: "running",
          animationPlayState: "running",
          WebkitAnimationDelay: "0s",
          animationDelay: "0s",
        }}
        transform="translate(50 50)"
      >
        <g
          transform="rotate(258)"
          style={{
            WebkitAnimationPlayState: "running",
            animationPlayState: "running",
            WebkitAnimationDelay: "0s",
            animationDelay: "0s",
          }}
        >
          <circle
            cx="25"
            r="10.533"
            fill="#fcb711"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          >
            <animate
              attributeName="r"
              begin="-4s"
              dur="4s"
              keyTimes="0;0.5;1"
              repeatCount="indefinite"
              values="6;14;6"
            ></animate>
          </circle>
          <animateTransform
            attributeName="transform"
            begin="0s"
            dur="4s"
            keyTimes="0;1"
            repeatCount="indefinite"
            type="rotate"
            values="0;360"
          ></animateTransform>
        </g>
        <g
          transform="rotate(276)"
          style={{
            WebkitAnimationPlayState: "running",
            animationPlayState: "running",
            WebkitAnimationDelay: "0s",
            animationDelay: "0s",
          }}
        >
          <circle
            cx="25"
            r="7.6"
            fill="#f37021"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          >
            <animate
              attributeName="r"
              begin="-3.3333333333333335s"
              dur="2s"
              keyTimes="0;0.5;1"
              repeatCount="indefinite"
              values="6;14;6"
            ></animate>
          </circle>
          <animateTransform
            attributeName="transform"
            begin="-0.6666666666666666s"
            dur="2s"
            keyTimes="0;1"
            repeatCount="indefinite"
            type="rotate"
            values="0;360"
          ></animateTransform>
        </g>
        <g
          transform="rotate(54)"
          style={{
            WebkitAnimationPlayState: "running",
            animationPlayState: "running",
            WebkitAnimationDelay: "0s",
            animationDelay: "0s",
          }}
        >
          <circle
            cx="25"
            r="8.4"
            fill="#cc004c"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          >
            <animate
              attributeName="r"
              begin="-2.6666666666666665s"
              dur="1.3333333333333333s"
              keyTimes="0;0.5;1"
              repeatCount="indefinite"
              values="6;14;6"
            ></animate>
          </circle>
          <animateTransform
            attributeName="transform"
            begin="-1.3333333333333333s"
            dur="1.3333333333333333s"
            keyTimes="0;1"
            repeatCount="indefinite"
            type="rotate"
            values="0;360"
          ></animateTransform>
        </g>
        <g
          transform="rotate(312)"
          style={{
            WebkitAnimationPlayState: "running",
            animationPlayState: "running",
            WebkitAnimationDelay: "0s",
            animationDelay: "0s",
          }}
        >
          <circle
            cx="25"
            r="8.133"
            fill="#6460aa"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          >
            <animate
              attributeName="r"
              begin="-2s"
              dur="1s"
              keyTimes="0;0.5;1"
              repeatCount="indefinite"
              values="6;14;6"
            ></animate>
          </circle>
          <animateTransform
            attributeName="transform"
            begin="-2s"
            dur="1s"
            keyTimes="0;1"
            repeatCount="indefinite"
            type="rotate"
            values="0;360"
          ></animateTransform>
        </g>
        <g
          transform="rotate(330)"
          style={{
            WebkitAnimationPlayState: "running",
            animationPlayState: "running",
            WebkitAnimationDelay: "0s",
            animationDelay: "0s",
          }}
        >
          <circle
            cx="25"
            r="10"
            fill="#0089d0"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          >
            <animate
              attributeName="r"
              begin="-1.3333333333333333s"
              dur="0.8s"
              keyTimes="0;0.5;1"
              repeatCount="indefinite"
              values="6;14;6"
            ></animate>
          </circle>
          <animateTransform
            attributeName="transform"
            begin="-2.6666666666666665s"
            dur="0.8s"
            keyTimes="0;1"
            repeatCount="indefinite"
            type="rotate"
            values="0;360"
          ></animateTransform>
        </g>
        <g
          transform="rotate(108)"
          style={{
            WebkitAnimationPlayState: "running",
            animationPlayState: "running",
            WebkitAnimationDelay: "0s",
            animationDelay: "0s",
          }}
        >
          <circle
            cx="25"
            r="10.8"
            fill="#0db14b"
            style={{
              WebkitAnimationPlayState: "running",
              animationPlayState: "running",
              WebkitAnimationDelay: "0s",
              animationDelay: "0s",
            }}
          >
            <animate
              attributeName="r"
              begin="-0.6666666666666666s"
              dur="0.6666666666666666s"
              keyTimes="0;0.5;1"
              repeatCount="indefinite"
              values="6;14;6"
            ></animate>
          </circle>
          <animateTransform
            attributeName="transform"
            begin="-3.3333333333333335s"
            dur="0.6666666666666666s"
            keyTimes="0;1"
            repeatCount="indefinite"
            type="rotate"
            values="0;360"
          ></animateTransform>
        </g>
      </g>
    </svg>
  );
};

export const CurveBarsSpinner = (
  width: string | number,
  height: string | number
) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      className="lds-curve-bars"
      preserveAspectRatio="xMidYMid"
      viewBox="0 0 100 100"
    >
      <g fill="none" strokeWidth="4" transform="translate(50 50)">
        <circle
          r="8.333"
          stroke="#ffffcb"
          strokeDasharray="26.179938779914945 26.179938779914945"
          transform="rotate(308.129)"
        >
          <animateTransform
            attributeName="transform"
            begin="0"
            calcMode="spline"
            dur="1s"
            keySplines="0.2 0 0.8 1"
            repeatCount="indefinite"
            type="rotate"
            values="0 0 0;360 0 0"
          ></animateTransform>
        </circle>
        <circle
          r="16.667"
          stroke="#fac090"
          strokeDasharray="52.35987755982989 52.35987755982989"
          transform="rotate(360)"
        >
          <animateTransform
            attributeName="transform"
            begin="-0.2"
            calcMode="spline"
            dur="1s"
            keySplines="0.2 0 0.8 1"
            repeatCount="indefinite"
            type="rotate"
            values="0 0 0;360 0 0"
          ></animateTransform>
        </circle>
        <circle
          r="25"
          stroke="#ff7c81"
          strokeDasharray="78.53981633974483 78.53981633974483"
          transform="rotate(51.87)"
        >
          <animateTransform
            attributeName="transform"
            begin="-0.4"
            calcMode="spline"
            dur="1s"
            keySplines="0.2 0 0.8 1"
            repeatCount="indefinite"
            type="rotate"
            values="0 0 0;360 0 0"
          ></animateTransform>
        </circle>
        <circle
          r="33.333"
          stroke="#c0f6d2"
          strokeDasharray="104.71975511965978 104.71975511965978"
          transform="rotate(135.238)"
        >
          <animateTransform
            attributeName="transform"
            begin="-0.6"
            calcMode="spline"
            dur="1s"
            keySplines="0.2 0 0.8 1"
            repeatCount="indefinite"
            type="rotate"
            values="0 0 0;360 0 0"
          ></animateTransform>
        </circle>
        <circle
          r="41.667"
          stroke="#dae4bf"
          strokeDasharray="130.89969389957471 130.89969389957471"
          transform="rotate(224.762)"
        >
          <animateTransform
            attributeName="transform"
            begin="-0.8"
            calcMode="spline"
            dur="1s"
            keySplines="0.2 0 0.8 1"
            repeatCount="indefinite"
            type="rotate"
            values="0 0 0;360 0 0"
          ></animateTransform>
        </circle>
      </g>
    </svg>
  );
};

export const getSpinner = ({
  name,
  width = "60",
  height = "60",
}: Pick<SpinnerProps, "name" | "width" | "height">) => {
  let spinner = null;
  switch (name) {
    case "doubleRing":
      spinner = DoubleRingSpinner(width, height);
      break;
    case "curve":
      spinner = CurveBarsSpinner(width, height);
      break;
    case "gooey":
    default:
      spinner = GooeySpinner(width, height);
      break;
  }

  return spinner;
};
