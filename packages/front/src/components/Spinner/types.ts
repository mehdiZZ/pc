export type SpinnerProps = {
  name?: "doubleRing" | "curve" | "gooey";
  width?: string | number;
  height?: string | number;
};
