import { screen, render } from "utils";
import userEvent from "@testing-library/user-event";
import { Wizard } from "../Wizard";

describe("👉 <Wizard>", () => {
  test("Renders correctly", () => {
    render(
      <Wizard
        activeStep={"step1"}
        steps={[
          {
            name: "step 1",
            id: "step1",
            component: <div>Content1</div>,
          },
          {
            name: "step 2",
            id: "step2",
            component: <div>Content2</div>,
          },
        ]}
      />
    );

    expect(screen.getByText("Content1")).toBeInTheDocument();
    expect(screen.queryByText("Content2")).not.toBeInTheDocument();

    userEvent.click(screen.getByText("step 2"));

    expect(screen.queryByText("Content1")).not.toBeInTheDocument();
    expect(screen.getByText("Content2")).toBeInTheDocument();
  });
});
