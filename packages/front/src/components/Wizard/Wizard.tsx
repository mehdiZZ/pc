import { useEffect } from "react";
import { Link, useLocation, useHistory } from "react-router-dom";
import clsx from "clsx";
import { useRoundedClass } from "utils";
import { motion } from "framer-motion";
import { Step, WizardProps } from "./types";

/**
 *
 * @param steps - {Step[]} array of steps
 * @param activeStep - {string} active step id
 * @param navigateWidthTabs - {string} handle navigation with navs
 * @example
   const [activeStep, setStep] = React.useState("step1");
   <Wizard
     activeStep={activeStep}
     steps={[
         {
         name: "step 1",
         id: "step1",
         component: (
             <div>
             <p>Lorem1</p>
             <button onClick={() => setStep("step2")}>
                 Go Next
             </button>
             </div>
         ),
         },
     ]}
     />
 */
export const Wizard = ({
  steps,
  activeStep,
  navigateWidthTabs = true,
}: WizardProps) => {
  const { search } = useLocation();
  const { push } = useHistory();

  useEffect(() => {
    setActive(activeStep);
  }, [activeStep]);

  const setActive = (stepId: string) => {
    const step = steps.find((step: Step) => step.id === stepId);
    if (step) {
      push({
        search: `?wizard=${step.id}`,
      });
    }
  };

  const isActive = (stepID: string): boolean => {
    const searchQuery = new URLSearchParams(search);
    const currentStepID = searchQuery.get("wizard");
    if (currentStepID) {
      if (currentStepID === stepID) {
        return true;
      }
    } else {
      if (stepID === activeStep) {
        return true;
      }
    }
    return false;
  };

  const stepsLength = steps.length;
  const activeIndex = steps.findIndex((step: Step) => isActive(step.id));
  const progress = ((activeIndex + 1) / stepsLength) * 100;

  const rounded = useRoundedClass();

  return (
    <div>
      <ul className="flex flex-wrap">
        {steps.map((step: Step) => {
          return (
            <li key={step.id} className={clsx("flex flex-1 mx-2")}>
              <Link
                className={clsx(
                  "block w-full p-4 text-center transition duration-300 text-lg text-inverse-higher",
                  rounded,
                  navigateWidthTabs && "hover:bg-primary",
                  isActive(step.id) ? "bg-primary" : "bg-content"
                )}
                to={{ search: `?wizard=${step.id}` }}
                onClick={(e) => {
                  if (!navigateWidthTabs) e.preventDefault();
                }}
              >
                {step.name}
              </Link>
            </li>
          );
        })}
      </ul>
      <div
        style={{ width: `calc(${progress}% - 1rem)` }}
        className={clsx(
          "relative z-10 bg-red flex h-2 mx-2 mt-1 transition-all duration-500",
          rounded && "rounded"
        )}
      />
      <div
        className={clsx(
          "flex bg-tertiary h-2 mx-2 -mt-2",
          rounded && "rounded"
        )}
      />
      <div className="px-2 py-6">
        {steps.map((step: Step) => {
          if (isActive(step.id)) {
            return (
              <motion.div
                key={step.id}
                initial="collapsed"
                animate="open"
                exit="collapsed"
                variants={{
                  open: { opacity: 1, y: 0 },
                  collapsed: { opacity: 0, y: 30 },
                }}
                className="overflow-hidden"
              >
                {step.component}
              </motion.div>
            );
          } else {
            return false;
          }
        })}
      </div>
    </div>
  );
};
