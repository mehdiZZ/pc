export type WizardProps = {
  steps: Step[];
  activeStep: string;
  navigateWidthTabs?: boolean;
};

export type Step = {
  id: string;
  component: React.ReactNode;
  name: React.ReactNode;
};
