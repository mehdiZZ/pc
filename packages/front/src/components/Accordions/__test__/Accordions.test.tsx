import { screen, waitFor } from "@testing-library/react";
import { render } from "utils";
import { Accordion } from "../Accordion";
import { Accordions } from "../Accordions";
import userEvent from "@testing-library/user-event";

describe("👉 <Accordion> & <Accordions>", () => {
  test("Renders correctly", async () => {
    render(
      <Accordions>
        <Accordion header={"first-header"} id="first">
          first-content
        </Accordion>
        <Accordion header={"second-header"} id="second">
          second-content
        </Accordion>
        <Accordion header={"third-header"} id="third">
          third-content
        </Accordion>
      </Accordions>
    );

    //headers
    expect(screen.getByText("first-header")).toBeInTheDocument();
    expect(screen.getByText("second-header")).toBeInTheDocument();
    expect(screen.getByText("third-header")).toBeInTheDocument();

    // => click on the first header
    userEvent.click(screen.getByText("first-header"));
    expect(screen.getByText("first-content")).toBeInTheDocument();
    expect(screen.queryByText("second-content")).not.toBeInTheDocument();
    expect(screen.queryByText("third-content")).not.toBeInTheDocument();

    // => click on the third header
    userEvent.click(screen.getByText("third-header"));
    expect(screen.getByText("third-content")).toBeInTheDocument();
    expect(screen.queryByText("second-content")).not.toBeInTheDocument();
    // wait for remove
    await waitFor(() => {
      expect(screen.queryByText("first-content")).not.toBeInTheDocument();
    });
  });
});
