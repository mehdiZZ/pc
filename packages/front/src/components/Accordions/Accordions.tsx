import { useState, createContext } from "react";
import { AccordionContextType, AccordionsProps } from "./types";
import { useRoundedClass } from "utils";
import clsx from "clsx";

export const AccordionContext = createContext<AccordionContextType>({
  expanded: "",
  setExpanded: () => {},
});

/**
 * @param initialValue - {string | number} init id
 * @example
  <Accordions initialValue="Test1">
    <Accordion header={<div>Title 1</div>}>
      Lorem ipsum dolor sit, amet consectetur adipisicing elit.
    </Accordion>
    <Accordion header={<div>Title 2</div>}>
      Lorem ipsum dolor sit, amet consectetur adipisicing elit.
    </Accordion>
    <Accordion header={<div>Title 3</div>}>
      Lorem ipsum dolor sit, amet consectetur adipisicing elit.
    </Accordion>
  </Accordions>
 */
export const Accordions = ({
  children,
  initialValue = "",
  className,
}: AccordionsProps) => {
  const classes = ["shadow-content overflow-hidden", useRoundedClass()];
  const [expanded, setExpanded] = useState(initialValue);

  return (
    <AccordionContext.Provider value={{ expanded, setExpanded }}>
      <div className={clsx(classes, className)}>
        <ul>{children}</ul>
      </div>
    </AccordionContext.Provider>
  );
};
