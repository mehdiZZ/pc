import { Dispatch } from "react";

export type AccordionsProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["div"]
> & {
  initialValue?: string | number;
};

export type AccordionContextType = {
  expanded: string | number;
  setExpanded: Dispatch<any>;
};

export type AccordionProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["li"]
> & {
  id: string | number;
  header: React.ReactNode;
};
