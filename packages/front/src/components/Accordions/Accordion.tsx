import { useContext } from "react";
import { AccordionContext } from "components/Accordions";
import { AnimatePresence, motion } from "framer-motion";
import { AccordionProps } from "./types";

/**
 * @param id - {string | number} unique id
 * @param header - {string | React.ReactNode} accordion header
 */
export const Accordion = ({
  children,
  id,
  header,
  className,
}: AccordionProps) => {
  const accordionContext = useContext(AccordionContext);
  const isOpen = accordionContext.expanded === id;
  return (
    <li className={className}>
      <div
        onClick={() => accordionContext.setExpanded(isOpen ? "" : id)}
        className={"cursor-pointer bg-content text-inverse-higher select-none"}
      >
        <div className="transition duration-300 hover:bg-content py-3 px-6">
          {header}
        </div>
      </div>
      <AnimatePresence initial={false}>
        {isOpen && (
          <motion.div
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: "auto" },
              collapsed: { opacity: 0, height: 0 },
            }}
            transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
            className="overflow-hidden"
          >
            <div className="py-3 px-6">{children}</div>
          </motion.div>
        )}
      </AnimatePresence>
    </li>
  );
};
