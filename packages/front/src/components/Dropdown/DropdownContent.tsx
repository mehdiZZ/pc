import { PropsWithoutRef, useContext } from "react";
import { DropdownContext } from ".";
import clsx from "clsx";
import { useRoundedClass } from "utils";
import { motion, AnimatePresence } from "framer-motion";

export const DropdownContent = ({
  children,
  className,
  ...props
}: PropsWithoutRef<JSX.IntrinsicElements["div"]>) => {
  const { isOpen } = useContext(DropdownContext);
  const classes = [
    "absolute x-center top-1 text-sm shadow z-50 min-w-full overflow-hidden",
    useRoundedClass(),
  ];

  return (
    <div {...props} className={clsx("relative", className)}>
      <AnimatePresence>
        {isOpen && (
          <motion.div
            animate={isOpen ? "open" : "collapsed"}
            initial={"collapsed"}
            exit={"collapsed"}
            variants={{
              open: { opacity: 1, height: "auto" },
              collapsed: { opacity: 0, height: 0 },
            }}
            className={clsx(classes)}
          >
            <div>{children}</div>
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
};
