export type DropdownProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["div"]
> & {
  onToggle?: (open: boolean) => void;
  isOpen?: boolean;
  children?: React.ReactNode;
};

export type DropdownToggleProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["button"]
> & {
  children?: React.ReactNode;
  caret?: boolean;
};
