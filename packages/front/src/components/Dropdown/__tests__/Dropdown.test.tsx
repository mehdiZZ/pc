import { render, screen } from "utils";
import userEvent from "@testing-library/user-event";
import { Dropdown } from "../Dropdown";
import { DropdownToggle } from "../DropdownToggle";
import { DropdownContent } from "../DropdownContent";

describe("👉 <Dropdown> & <DropdownContent> & <DropdownToggle>", () => {
  test("Renders children", async () => {
    render(
      <Dropdown>
        <DropdownToggle>Click me</DropdownToggle>
        <DropdownContent>Any content</DropdownContent>
      </Dropdown>
    );

    expect(screen.queryByText("Any content")).not.toBeInTheDocument();
    expect(screen.getByText("Click me")).toBeInTheDocument();

    userEvent.click(screen.getByRole("button"));

    expect(screen.getByText("Any content")).toBeInTheDocument();
  });
});
