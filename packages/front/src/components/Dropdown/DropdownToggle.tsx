import { useContext } from "react";
import { DropdownToggleProps } from "./types";
import { ButtonProps } from "../Button/types";
import { DropdownContext } from ".";
import { Button } from "../Button";

/**
 * @param caret - {boolean} show down caret
 */
export const DropdownToggle = ({
  children = null,
  caret = false,
  ...props
}: DropdownToggleProps & ButtonProps) => {
  const buttonContext = useContext(DropdownContext);

  return (
    <Button
      {...props}
      theme="transparent"
      onClick={() => buttonContext.toggle(!buttonContext.isOpen)}
    >
      {children} {caret && <span className="inline-block mx-1">&#8595;</span>}
    </Button>
  );
};
