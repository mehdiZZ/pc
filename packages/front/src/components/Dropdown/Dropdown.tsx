import { useEffect, useRef, createContext, useState } from "react";
import { DropdownProps } from "./types";

export const DropdownContext = createContext({
  toggle: (isOpen: boolean) => {},
  isOpen: false,
});

/**
 * @param isOpen - {boolean} dropdown status (controlled dropdown)
 * @param onToggle -  {function} handle toggling (controlled dropdown)
 *
 * @example
   <Dropdown>
    <DropdownToggle>Click me</DropdownToggle>
    <DropdownContent>Any content</DropdownContent>
   </Dropdown>
 */
export const Dropdown = ({
  children = null,
  onToggle,
  isOpen,
  ...props
}: DropdownProps) => {
  const node = useRef<HTMLDivElement>(null);

  const [isDropdownOpen, setDropdown] = useState(false);

  useEffect(() => {
    typeof isOpen !== "undefined" && setDropdown(isOpen);
  }, [isOpen]);

  const localSetDropdown = (isOpen: boolean) => {
    if (onToggle) {
      onToggle(isOpen);
    } else {
      setDropdown(isOpen);
    }
  };

  const handleClick = (event: any) => {
    if (node && node.current) {
      if (node.current.contains(event.target)) {
        // inside click
        return;
      }
      // outside click
      localSetDropdown(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClick);
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, []);

  return (
    <DropdownContext.Provider
      value={{
        toggle: localSetDropdown,
        isOpen: isDropdownOpen,
      }}
    >
      <div {...props} ref={node}>
        {children}
      </div>
    </DropdownContext.Provider>
  );
};
