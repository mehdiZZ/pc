import { motion } from "framer-motion";
import { ButtonProps } from "./types";

export const Loading = () => {
  const spinTransition = {
    loop: Infinity,
    ease: "linear",
    duration: 0.8,
  };
  return (
    <motion.span
      className="btn-spinner w-4 h-4 inline-flex border-t-2 rounded-full origin-center mx-2"
      animate={{ rotate: 360 }}
      transition={spinTransition}
    ></motion.span>
  );
};

export const getClassNames = ({
  disabled,
  loading,
  block,
  size,
  outline,
  theme,
}: Pick<
  ButtonProps,
  "disabled" | "loading" | "block" | "size" | "outline" | "theme"
>) => {
  const classes = ["button transition duration-200 items-center"];
  if (disabled || loading) {
    classes.push(
      "opacity-50 cursor-not-allowed hover:outline-none focus:outline-none"
    );
  }

  if (block) {
    classes.push("flex w-full justify-center");
  } else {
    classes.push("inline-flex");
  }

  switch (size) {
    case "medium":
      classes.push("px-4 py-2 text-base");
      break;
    case "tiny":
      classes.push("p-1 text-xs");
      break;
    case "small":
      classes.push("px-2 py-1 text-sm");
      break;
    case "large":
      classes.push("px-6 py-3 text-lg");
      break;
    default:
      break;
  }
  if (!outline) {
    switch (theme) {
      case "default":
        classes.push("bg-tertiary text-tertiary-inverse");
        break;
      case "primary":
        classes.push("bg-primary text-primary-inverse");
        break;
      case "secondary":
        classes.push("bg-secondary text-secondary-inverse");
        break;
      case "info":
        classes.push("bg-teal text-white");
        break;
      case "success":
        classes.push("bg-green text-white");
        break;
      case "danger":
        classes.push("bg-red text-white");
        break;
      case "warning":
        classes.push("bg-orange text-white");
        break;
      case "transparent":
        classes.push("bg-transparent");
        break;
    }
    if (!disabled) classes.push("hover:filter-dark focus:filter-dark");
  } else {
    switch (theme) {
      case "default":
        classes.push(
          "text-tertiary border-tertiary hover:bg-tertiary focus:bg-tertiary hover:text-tertiary-inverse focus:text-tertiary-inverse"
        );
        break;
      case "primary":
        classes.push(
          "text-primary border-primary hover:bg-primary focus:bg-primary hover:text-primary-inverse focus:text-primary-inverse"
        );
        break;
      case "secondary":
        classes.push(
          "text-secondary border-secondary hover:bg-secondary focus:bg-secondary hover:text-secondary-inverse focus:text-secondary-inverse"
        );
        break;
      case "info":
        classes.push(
          "text-teal border-teal hover:bg-teal focus:bg-teal hover:text-violetDeep focus:text-violetDeep"
        );
        break;
      case "success":
        classes.push(
          "text-green border-green hover:bg-green focus:bg-green hover:text-violetDeep focus:text-violetDeep"
        );
        break;
      case "danger":
        classes.push(
          "text-red border-red hover:bg-red focus:bg-red hover:text-violetDeep focus:text-violetDeep"
        );
        break;
      case "warning":
        classes.push(
          "text-orange border-orange hover:bg-orange focus:bg-orange hover:text-violetDeep focus:text-violetDeep"
        );
        break;
    }
    classes.push("font-semibold bg-transparent border");
  }

  return classes;
};
