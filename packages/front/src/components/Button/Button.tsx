import { useMemo } from "react";
import clsx from "clsx";
import { ButtonProps } from "./types";
import { useRoundedClass } from "utils";
import { Loading, getClassNames } from "./utils";
import { useHistory } from "react-router-dom";

/**
 * @param theme - {string} theme name
 * @param disabled - {boolean} enable/disable button
 * @param rounded - {boolean | string} buttons border radius
 * @param size - {string} button size
 * @param block - {boolean} set width to 100%
 * @param outline -  {boolean} enable/disable button outline
 * @param onClick - {function} handle click event
 * @param linkTo - {string} a URL for acting like a link
 * @param loading - {boolean} a loading status for showing spinner
 *
 */
export const Button = ({
  theme = "default",
  disabled = false,
  size = "medium",
  block = false,
  outline = false,
  children,
  loading = false,
  rounded,
  onClick,
  linkTo,
  className,
  ...props
}: ButtonProps) => {
  //
  const classes = [
    useRoundedClass(rounded),
    ...useMemo(() => {
      return getClassNames({ disabled, loading, block, size, outline, theme });
    }, [disabled, loading, block, size, outline, theme]),
  ];

  const { push } = useHistory();

  const onClickAction = (e) => {
    if (onClick) {
      e.preventDefault();
      !disabled && !loading && onClick(e);
    }

    if (linkTo) {
      push(linkTo);
    }
  };

  return (
    <button
      {...props}
      disabled={disabled || loading}
      onClick={onClickAction}
      className={`${clsx(classes, className)}`}
    >
      {children}
      {loading && (
        <>
          {" "}
          <Loading />
        </>
      )}
    </button>
  );
};
