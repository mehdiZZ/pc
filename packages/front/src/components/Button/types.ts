import { General } from "utils";

export type ButtonProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["button"]
> & {
  rounded?: General["rounded"];
  theme?: General["theme"] | "transparent";
  outline?: boolean;
  size?: "tiny" | "small" | "medium" | "large";
  block?: boolean;
  loading?: boolean;
  linkTo?: string;
};
