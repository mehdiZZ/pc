import { render, screen } from "utils";
import { Button } from "../Button";
import { ButtonGroup } from "../ButtonGroup";

describe("👉 <ButtonGroup>", () => {
  test("Renders children", () => {
    render(
      <ButtonGroup data-testid="button-group">
        <Button>Hey</Button>
        <Button>Hi</Button>
      </ButtonGroup>
    );
    expect(screen.getByText("Hey")).toBeInTheDocument();
    expect(screen.getByText("Hey")).toBeInTheDocument();
  });
});
