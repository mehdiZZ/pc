import { screen } from "@testing-library/react";
import { render } from "utils";
import { Button } from "../Button";
import userEvent from "@testing-library/user-event";

describe("👉 <Button>", () => {
  test("Renders Children", () => {
    render(
      <Button data-testid="button">
        <span>Yo</span>
      </Button>
    );
    expect(screen.getByText("Yo")).toBeInTheDocument();

    expect(screen.getByTestId("button").innerHTML).toMatch(`<span>Yo</span>`);
  });

  //
  test("Disables button", () => {
    render(<Button disabled>Disabled</Button>);
    expect(screen.getByText("Disabled")).toBeDisabled();
  });

  //
  test("Showing spinner", () => {
    render(<Button loading>Loading</Button>);

    // disable it when loading is true
    expect(screen.getByText(/^Loading/)).toBeDisabled();

    // check for spinner
    expect(
      screen
        .getByText("Loading")
        .firstElementChild.classList.contains("btn-spinner")
    );
  });

  //
  test("Triggers click callback", () => {
    const onClick = jest.fn();
    render(<Button onClick={onClick}>Clicked</Button>);
    const button = screen.getByText("Clicked");
    userEvent.click(button);

    expect(onClick).toHaveBeenCalled();
  });
});
