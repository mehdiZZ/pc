import { FC, PropsWithoutRef } from "react";
import clsx from "clsx";

/**
 * @example
   <ButtonGroup>
    <Button>Hey</Button>
    <Button>Hi</Button>
    <Button>Bye</Button>
   </ButtonGroup>
 */
export const ButtonGroup: FC = ({
  children,
  className,
  ...props
}: PropsWithoutRef<JSX.IntrinsicElements["div"]>) => {
  return (
    <div {...props} className={clsx("btn-group inline-flex", className)}>
      {children}
    </div>
  );
};
