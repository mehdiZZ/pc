import { LabelProps } from "./types";
import clsx from "clsx";

export const Label = ({
  for: htmlFor,
  className,
  children,
  block = false,
  ...props
}: LabelProps) => {
  const classes = [
    "mb-2 font-semi-bold text-inverse-higher select-none items-center",
    block ? "flex" : "inline-flex",
  ];

  return (
    <label {...props} htmlFor={htmlFor} className={clsx(classes, className)}>
      {children}
    </label>
  );
};
