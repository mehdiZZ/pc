export type LabelProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["label"]
> & {
  for?: string | undefined;
  block?: boolean;
};
