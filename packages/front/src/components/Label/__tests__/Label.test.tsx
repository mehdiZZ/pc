import { render, screen } from "utils";
import { Label } from "../Label";

describe("👉 <Label>", () => {
  test("Renders correctly", () => {
    render(<Label for="someId">Label text</Label>);

    // text and for attr
    expect(screen.getByText("Label text")).toHaveAttribute("for", "someId");
  });
});
