import { InputProps } from "./types";

export const getClassNames = ({
  inputSize,
  disabled,
  readOnly,
}: Pick<InputProps, "inputSize" | "disabled" | "readOnly">) => {
  const classes = ["input bg-content focus:outline-none"];

  switch (inputSize) {
    case "small":
      classes.push("px-2 py-1 text-sm");
      break;
    case "medium":
    default:
      classes.push("px-3 py-2");
      break;
    case "large":
      classes.push("px-4 py-3 text-lg");
      break;
  }

  if (!disabled && !readOnly) {
    classes.push("focus:shadow");
  } else {
    classes.push("opacity-50 cursor-not-allowed pointer-events-none");
  }

  return classes;
};
