import { render, screen } from "utils";
import { Input } from "../Input";
import { InputGroup } from "../InputGroup";

describe("👉 <InputGroup>", () => {
  test("Renders children", () => {
    render(
      <InputGroup>
        <Input placeholder="one" />
        <Input placeholder="two" />
      </InputGroup>
    );

    expect(screen.queryByPlaceholderText("one")).toBeInTheDocument();
    expect(screen.queryByPlaceholderText("two")).toBeInTheDocument();
  });
});
