import { render, screen, waitFor } from "utils";
import { Input } from "../Input";
import userEvent from "@testing-library/user-event";

describe("👉 <Input>", () => {
  test("Default behavior", async () => {
    render(<Input data-testid="input" />);

    userEvent.type(screen.getByTestId("input"), "newValue");

    expect(screen.getByTestId("input")).toHaveValue("newValue");
  });

  test("Props", async () => {
    const changeHandler = jest.fn();

    render(
      <Input
        data-testid="input"
        placeholder={"placeholder"}
        value={"value"}
        onChange={changeHandler}
      />
    );

    expect(screen.getByTestId("input")).toHaveAttribute("value", "value");
    expect(screen.getByTestId("input")).toHaveAttribute(
      "placeholder",
      "placeholder"
    );

    userEvent.type(screen.getByTestId("input"), "newValue");

    expect(changeHandler).toHaveBeenCalled();
  });
});
