import { General } from "utils";

export type InputProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["input"]
> & {
  name?: string;
  type?:
    | "number"
    | "text"
    | "email"
    | "password"
    | "search"
    | "tel"
    | "url"
    | "checkbox"
    | "hidden";
  inputSize?: "small" | "medium" | "large";
  rounded?: General["rounded"];
  error?: string;
};

export type InputGroupProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["div"]
> & {
  block?: boolean;
};
