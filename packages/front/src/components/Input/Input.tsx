import { useMemo, forwardRef } from "react";
import clsx from "clsx";
import { InputProps } from "./types";
import { useRoundedClass } from "utils";
import { getClassNames } from "./utils";
import { Typography } from "components/Typography";

export const Input = forwardRef<HTMLInputElement, InputProps>(
  (
    {
      type = "text",
      className,
      inputSize = "medium",
      disabled = false,
      readOnly = false,
      rounded,
      ...props
    },
    ref
  ) => {
    const classes = [
      useRoundedClass(rounded),
      ...useMemo(() => {
        return getClassNames({ inputSize, disabled, readOnly });
      }, [inputSize, disabled, readOnly]),
    ];

    return (
      <>
        <input
          {...props}
          type={type}
          disabled={disabled}
          readOnly={readOnly}
          className={clsx(classes, className)}
          ref={ref}
        />
        {props.error && (
          <Typography color="text-red" tag="span" className="text-xxs mx-2">
            {props.error}
          </Typography>
        )}
      </>
    );
  }
);
