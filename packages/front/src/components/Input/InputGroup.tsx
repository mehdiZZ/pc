import clsx from "clsx";
import { InputGroupProps } from "./types";

/**
 * @example
  <InputGroup>
    <Button>
      <PlusOutlined />
    </Button>
    <Input name="name" />
    <Button>
      <CloseOutlined />
    </Button>
  </InputGroup>
 */
export const InputGroup = ({
  children,
  className,
  block = false,
}: InputGroupProps) => {
  return (
    <div
      className={clsx(
        className,
        `input-group ${block ? "flex" : "inline-flex"}`
      )}
    >
      {children}
    </div>
  );
};
