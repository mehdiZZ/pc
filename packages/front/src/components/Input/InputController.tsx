import { Controller } from "react-hook-form";
import { Input } from "./Input";
import { InputProps } from "./types";

export const InputController = ({
  inputProps,
  controllerProps,
}: {
  inputProps: InputProps;
  controllerProps: any;
}) => {
  return (
    <Controller
      {...controllerProps}
      name={controllerProps.name}
      control={controllerProps.control}
      defaultValue={controllerProps.defaultValue}
      render={({ onChange, value }) => {
        return (
          <Input
            {...inputProps}
            onChange={(e) => onChange(e.target.value)}
            value={value}
            error={inputProps.error}
          />
        );
      }}
    />
  );
};
