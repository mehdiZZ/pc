export type ListProps = React.PropsWithoutRef<JSX.IntrinsicElements["ul"]> & {
  ordered?: boolean;
  listDisc?: boolean;
  listDecimal?: boolean;
  inline?: boolean;
};
