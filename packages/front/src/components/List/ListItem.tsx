import { PropsWithoutRef } from "react";
import clsx from "clsx";

export const ListItem = ({
  className,
  children,
  ...props
}: PropsWithoutRef<JSX.IntrinsicElements["li"]>) => {
  return (
    <li {...props} className={clsx(className)}>
      {children}
    </li>
  );
};
