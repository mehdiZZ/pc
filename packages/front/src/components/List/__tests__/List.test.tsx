import { screen, render } from "utils";
import { List } from "../List";
import { ListItem } from "../ListItem";

describe("👉 <List> & <ListItem>", () => {
  test("Renders list", () => {
    render(
      <List data-testid="list">
        <ListItem>One</ListItem>
        <ListItem>Two</ListItem>
      </List>
    );

    const list = screen.getByTestId("list");
    expect(list.childElementCount).toBe(2);

    //
    expect(screen.getByText("One")).toBeInTheDocument();
    expect(screen.getByText("Two")).toBeInTheDocument();
  });
});
