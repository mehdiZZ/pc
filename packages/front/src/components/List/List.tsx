import { ListProps } from "./types";
import clsx from "clsx";

/**
 * @param ordered - {boolean} use ol instead of ul
 * @param listDisc - {boolean} use ● before list item
 * @param listDecimal - {boolean} count list items with digits
 *
 * @example
  <List ordered listDisc>
    <ListItem className="mb-2 bg-content p-3 rounded">hello</ListItem>
  </List>
 */
export const List = ({
  ordered = false,
  listDisc = false,
  listDecimal = false,
  inline = false,
  children,
  className,
  ...props
}: ListProps) => {
  const Component = ordered ? "ol" : "ul";

  return (
    <Component
      {...props}
      className={clsx(
        listDisc
          ? "list-disc list-inside"
          : listDecimal
          ? "list-decimal list-inside"
          : "",
        inline ? "inline-flex" : "flex",
        className
      )}
    >
      {children}
    </Component>
  );
};
