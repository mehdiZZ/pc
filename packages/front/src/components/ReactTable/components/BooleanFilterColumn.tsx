import { CloseCircleOutlined } from "@ant-design/icons";
import { Switch } from "components/Switch";
import { FilterProps } from "react-table";

export const BooleanFilterColumn = ({
  column: { filterValue, setFilter },
}: FilterProps<object>) => {
  return (
    <div className="flex items-center">
      <Switch
        switchSize="small"
        className={!filterValue ? "opacity-25" : ""}
        checked={filterValue?.is}
        onChange={(e) => setFilter({ is: e.target.checked, type: "$eq" })}
      />
      <span
        className="inline-flex mx-2"
        onClick={() => {
          setFilter(undefined);
        }}
      >
        <CloseCircleOutlined className="text-sm text-red-dark" />
      </span>
    </div>
  );
};
