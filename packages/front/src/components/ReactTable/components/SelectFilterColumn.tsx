import { CloseCircleOutlined } from "@ant-design/icons";
import { Select } from "components/Select";
import { FilterProps } from "react-table";

export const SelectFilterColumn = ({
  props: {
    column: { filterValue, setFilter },
  },
  options,
}: {
  props: FilterProps<object>;
  options: { value: any; label: string }[];
}) => {
  return (
    <div className="flex items-center">
      <Select
        inputSize="small"
        onChange={(e) => {
          if (e.target.value) {
            setFilter({ is: e.target.value, type: "$eq" });
          } else {
            setFilter(undefined);
          }
        }}
        className={!filterValue ? "opacity-25" : ""}
      >
        {options?.map((op, i) => (
          <option key={op.value || i} value={op.value}>
            {op.label}
          </option>
        ))}
      </Select>
      <span
        className="inline-flex mx-2"
        onClick={() => {
          setFilter(undefined);
        }}
      >
        <CloseCircleOutlined className="text-sm text-red-dark" />
      </span>
    </div>
  );
};
