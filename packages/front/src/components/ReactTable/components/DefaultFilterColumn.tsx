import { CloseCircleOutlined } from "@ant-design/icons";
import { Input } from "components/Input";
import { useTranslation } from "react-i18next";
import { FilterProps } from "react-table";

export const DefaultFilterColumn = ({
  column: { filterValue, setFilter },
}: FilterProps<object>) => {
  const { t } = useTranslation();
  return (
    <div className="inline-flex items-center">
      <Input
        inputSize="small"
        className={!filterValue ? "opacity-25" : ""}
        value={filterValue?.is || ""}
        style={{ maxWidth: "150px" }}
        placeholder={t("crud:filter")}
        onChange={(e) => {
          if (e.target.value) {
            setFilter({ is: e.target.value, type: "$cont" });
          } else {
            setFilter(undefined);
          }
        }}
      />
      <span
        className="inline-flex mx-2"
        onClick={() => {
          setFilter(undefined);
        }}
      >
        <CloseCircleOutlined className="text-sm text-red-dark" />
      </span>
    </div>
  );
};
