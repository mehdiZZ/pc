import { render, screen } from "utils";
import { ReactTable } from "../ReactTable";

describe("👉 <ReactTable>", () => {
  test("Snapshot ReactTable", async () => {
    render(
      <div data-testid="table">
        <ReactTable
          columns={[
            {
              Header: "Title",
              accessor: "title",
            },
            {
              Header: "Content",
              accessor: "content",
            },
          ]}
          data={[
            {
              title: "title",
              content: "content",
            },
            {
              title: "title2",
              content: "content2",
            },
          ]}
        />
      </div>
    );
    expect(screen.getByTestId("table")).toMatchSnapshot();
  });
});
