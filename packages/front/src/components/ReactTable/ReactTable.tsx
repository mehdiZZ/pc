import { forwardRef, useRef, useEffect, useMemo, Fragment } from "react";
import {
  useTable,
  usePagination,
  useSortBy,
  useFilters,
  useResizeColumns,
  useExpanded,
  HeaderGroup,
  TableOptions,
  useFlexLayout,
  useRowSelect,
  useAsyncDebounce,
} from "react-table";
import clsx from "clsx";
import SimpleBar from "simplebar-react";
import { Spinner, Button, Input, Select } from "components";
import { ReactTableProps } from "./types";
import { useLanguage } from "utils";
import { useTranslation, Trans } from "react-i18next";
import {
  CaretDownOutlined,
  CaretUpOutlined,
  LeftOutlined,
  RightOutlined,
} from "@ant-design/icons";
import { DefaultFilterColumn } from "./components";

/**
 *
 * @param columns - {object[]} table columns
 * @param data - {object[]} table rows
 * @param fetchData - {function} get data from server(Controlled table)
 * @param loading - {boolean} set loading while fetching data from server(Controlled table)
 * @param pageCount - {number} number of pages(Controlled table)
 * @param hasFooter - {boolean} set it to true if your columns have footer
 * @param filters - {object[]} filtered rows by user
 * @param renderRowSubComponent - {ReactNode} react component for rendering sub components
 * @param height - {string | number} table height
 * @param selectable - {boolean} is rows selectable
 * @param setSelectedFiles - {function} set selected rows
 *
 */

const IndeterminateCheckbox = forwardRef(
  ({ indeterminate, ...rest }: any, ref) => {
    const defaultRef = useRef();
    const resolvedRef: any = ref || defaultRef;

    useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
      <>
        <Input type="checkbox" ref={resolvedRef} {...rest} />
      </>
    );
  }
);

export const ReactTable = ({
  columns,
  data,
  fetchData,
  loading,
  pageCount: controlledPageCount,
  hasFooter = false,
  filters: userFilters,
  renderRowSubComponent,
  height = "600px",
  selectable = false,
  setSelectedFiles,
}: ReactTableProps) => {
  const { isRtl } = useLanguage();
  const { t } = useTranslation();

  const options: TableOptions<object> = {
    columns,
    data,
    initialState: { pageIndex: 0 },
  };

  if (controlledPageCount) {
    options["manualPagination"] = true;
    options["pageCount"] = controlledPageCount;
  }

  options["manualSortBy"] = true;

  if (userFilters) {
    options["manualFilters"] = true;
  }

  const plugins = [];
  if (userFilters) {
    //
    plugins.push(useFilters);
    //
    if (options.initialState) options["initialState"]["filters"] = userFilters;
  }

  plugins.push(useSortBy);

  if (renderRowSubComponent) {
    plugins.push(useExpanded);
  }

  plugins.push(usePagination, useFlexLayout, useResizeColumns);

  if (selectable) {
    const hooks = (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: "selection",
          width: 60,
          Header: ({ getToggleAllPageRowsSelectedProps }) => (
            <div className="flex h-full items-center">
              <IndeterminateCheckbox {...getToggleAllPageRowsSelectedProps()} />
            </div>
          ),
          // The cell can use the individual row's getToggleRowSelectedProps method
          // to the render a checkbox
          Cell: ({ row }) => (
            <div className="flex h-full items-center">
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            </div>
          ),
        },
        ...columns,
      ]);
    };
    plugins.push(useRowSelect, hooks);
  }
  //
  const defaultColumn = useMemo(
    () => ({
      minWidth: 20,
      width: 150,
      maxWidth: 300,
      Filter: (props) => <DefaultFilterColumn {...props} />,
    }),
    []
  );
  options["defaultColumn"] = defaultColumn;

  //
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    footerGroups,
    selectedFlatRows,
    state: { pageIndex, pageSize, sortBy, filters },
  } = useTable(options, ...plugins);

  const onFetchDataDebounced = useAsyncDebounce(fetchData, 400);

  // Listen for changes in pagination and use the state to fetch our new data
  useEffect(() => {
    if (fetchData && !loading) {
      onFetchDataDebounced({
        page: pageIndex + 1,
        limit: pageSize,
        sort: sortBy,
        filter: filters,
      });
    }
  }, [onFetchDataDebounced, pageIndex, pageSize, sortBy, filters]);

  useEffect(() => {
    setSelectedFiles && setSelectedFiles(selectedFlatRows);
  }, [selectedFlatRows?.length]);

  /* /////////////////START RENDER///////////////////// */
  return (
    <>
      <SimpleBar style={{ maxHeight: height }}>
        <div
          {...getTableProps()}
          className={clsx("data-table table-striped relative")}
        >
          <div className="select-none thead">
            {headerGroups.map((headerGroup: HeaderGroup<object>) => (
              <div {...headerGroup.getHeaderGroupProps()} className="tr">
                {headerGroup.headers.map((column) => (
                  <div
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                    className="py-3 px-2 text-inverse-higher th relative flex flex-col"
                  >
                    <div className="flex items-center h-full">
                      {column.render("Header")}
                      <span className="inline-flex">
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <CaretDownOutlined className="mx-2" />
                          ) : (
                            <CaretUpOutlined className="mx-2" />
                          )
                        ) : (
                          ""
                        )}
                      </span>
                    </div>
                    <div onClick={(e) => e.stopPropagation()}>
                      {column.canFilter ? column.render("Filter") : null}
                    </div>
                    <div
                      onClick={(e) => e.stopPropagation()}
                      {...column.getResizerProps()}
                      className={clsx(
                        "table-resizer absolute w-3 h-full top-0 z-10 inline-block",
                        isRtl ? "left-0" : "right-0"
                      )}
                    />
                  </div>
                ))}
              </div>
            ))}
          </div>
          {/* /////////////////START TBODY///////////////////// */}
          <div {...getTableBodyProps()} className="tbody max-w-full">
            {page.map((row) => {
              prepareRow(row);
              const { key, ...others } = row.getRowProps();
              return (
                <Fragment key={key}>
                  <div {...others} className="tr rounded">
                    {row.cells.map((cell) => {
                      return (
                        <div
                          {...cell.getCellProps()}
                          className="py-3 px-2 flex td"
                        >
                          {cell.render("Cell")}
                        </div>
                      );
                    })}
                  </div>

                  {row.isExpanded ? (
                    <div className="tr p-4">
                      <div className="td">
                        {renderRowSubComponent &&
                          renderRowSubComponent({ row })}
                      </div>
                    </div>
                  ) : null}
                </Fragment>
              );
            })}
          </div>
          {hasFooter && (
            <div className="select-none tfoot">
              {footerGroups.map((group) => (
                <div {...group.getFooterGroupProps()} className="tr">
                  {group.headers.map((column) => (
                    <div
                      {...column.getFooterProps()}
                      className="py-3 px-2 text-inverse-higher td"
                    >
                      {column.render("Footer")}
                    </div>
                  ))}
                </div>
              ))}
            </div>
          )}
          {loading && <Spinner />}
        </div>
      </SimpleBar>

      {data?.length ? (
        <div className="py-4 flex justify-between mt-3">
          <div className="flex">
            <div className="inline-block mx-2">
              {t("crud:goToPage")}
              <Input
                type="number"
                inputSize="small"
                defaultValue={pageIndex + 1}
                min={1}
                max={pageCount}
                onChange={(e) => {
                  const page = e.target.value ? Number(e.target.value) - 1 : 0;
                  gotoPage(page);
                }}
                style={{ width: "100px" }}
              />
            </div>
            <div>
              <Select
                value={pageSize}
                inputSize="small"
                onChange={(e) => {
                  setPageSize(Number(e.target.value));
                }}
              >
                {[10, 20, 30, 40, 50, 100].map((pageSize) => (
                  <option key={pageSize} value={pageSize}>
                    {pageSize} {t("crud:rows")}
                  </option>
                ))}
              </Select>
            </div>
          </div>
          <div>
            <Button
              theme="primary"
              onClick={() => previousPage()}
              disabled={!canPreviousPage}
            >
              {isRtl ? <RightOutlined /> : <LeftOutlined />}
            </Button>
            <small className="inline-block mx-2">
              <Trans i18nKey="crud:currentPage">
                Page
                <strong>
                  {{ pageIndex: pageIndex + 1 }} of {{ pageCount: pageCount }}
                </strong>
              </Trans>
            </small>
            <Button
              theme="primary"
              onClick={() => nextPage()}
              disabled={!canNextPage}
            >
              {isRtl ? <LeftOutlined /> : <RightOutlined />}
            </Button>
          </div>
        </div>
      ) : null}
    </>
  );
};
