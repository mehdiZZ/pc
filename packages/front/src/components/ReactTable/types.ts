import { Column, Filters, Row } from "react-table";

export type FetchDataProps = {
  page?: number;
  limit: number;
  sort: Array<{}>;
  filter: Array<{}>;
};

export type ReactTableProps = {
  columns: Column<object>[];
  data: Array<{}>;
  fetchData?: (props: FetchDataProps) => void;
  loading?: boolean;
  pageCount?: number;
  hasFooter?: boolean;
  filters?: Filters<object>;
  renderRowSubComponent?: (props: { row: any }) => React.ReactNode;
  height?: string | number;
  selectable?: boolean;
  setSelectedFiles?: (selectedRows) => void;
};
