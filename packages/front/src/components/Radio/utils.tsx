import { RadioProps } from "./types";

export const getClassNames = ({
  theme,
  disabled,
  readOnly,
}: Pick<RadioProps, "theme" | "disabled" | "readOnly">) => {
  const classes = [
    "radio w-4 h-4 bg-primary-inverse bg-center cursor-pointer border-4 border-transparent border-solid bg-no-repeat bg-full select-none inline-flex justify-center items-center focus:ring focus:outline-none appearance-none transition-all duration-300 rounded-full",
  ];

  switch (theme) {
    case "default":
      classes.push("checked:border-tertiary");
      break;
    case "primary":
      classes.push("checked:border-primary");
      break;
    case "secondary":
      classes.push("checked:border-secondary");
      break;
    case "info":
      classes.push("checked:border-teal");
      break;
    case "success":
      classes.push("checked:border-green");
      break;
    case "danger":
      classes.push("checked:border-red");
      break;
    case "warning":
      classes.push("checked:border-orange");
      break;
  }

  if (disabled || readOnly) {
    classes.push("opacity-50 cursor-not-allowed");
  }

  return classes;
};
