import { forwardRef, useMemo } from "react";
import clsx from "clsx";
import { RadioProps } from "./types";
import { getClassNames } from "./utils";
/**
 * @param theme - {string} checkbox theme
 * @param disabled - {boolean} is disabled
 * @param readOnly - {boolean} is readonly
 *
 * @example
   <Label>
     <Typography>label</Typography>
     <Radio className="mx-2" />
   </Label>
 */
export const Radio = forwardRef<HTMLInputElement, RadioProps>(
  (
    {
      theme = "default",
      disabled = false,
      readOnly = false,
      className,
      ...props
    },
    ref
  ) => {
    const classes = useMemo(() => {
      return getClassNames({ theme, readOnly, disabled });
    }, [theme, readOnly, disabled]);

    return (
      <input
        {...props}
        disabled={disabled}
        readOnly={readOnly}
        className={clsx(classes, className)}
        ref={ref}
        type="radio"
      />
    );
  }
);
