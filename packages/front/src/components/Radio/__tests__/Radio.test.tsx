import { render, screen } from "utils";
import userEvent from "@testing-library/user-event";
import { Radio } from "../Radio";

describe("👉 <Radio>", () => {
  test("Toggle element by selecting radio", () => {
    render(
      <div>
        <Radio name={"radio"} />
      </div>
    );

    expect(screen.getByRole("radio")).not.toBeChecked();

    userEvent.click(screen.getByRole("radio"));

    expect(screen.getByRole("radio")).toBeChecked();
  });
});
