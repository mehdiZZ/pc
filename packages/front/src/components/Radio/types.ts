import { General } from "utils";

export type RadioProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["input"]
> & {
  theme?: General["theme"];
};
