import { useMemo } from "react";
import { BoxProps } from "./types";
import clsx from "clsx";
import { getClassNames } from "./utils";

export const Box = ({
  sm,
  md,
  lg,
  xl,
  xxl,
  className,
  children,
  tag = "div",
  noMargin = false,
  noPadding = false,
}: BoxProps) => {
  const classes = useMemo(() => {
    return getClassNames({ sm, md, lg, xl, xxl, noMargin, noPadding });
  }, [sm, md, lg, xl, xxl, noMargin, noPadding]);

  const Component = tag;

  return <Component className={clsx(classes, className)}>{children}</Component>;
};
