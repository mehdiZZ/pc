import { BoxProps } from "./types";

export const getClassNames = ({
  sm,
  md,
  lg,
  xl,
  xxl,
  noMargin,
  noPadding,
}: Pick<
  BoxProps,
  "sm" | "md" | "lg" | "xl" | "xxl" | "noMargin" | "noPadding"
>) => {
  const classes = [!noMargin && "mb-6", !noPadding && "px-4"];

  if (sm || md || lg || xl || xxl) {
    // 100% in mobile
    classes.push("w-full");
    //
    if (sm) {
      if (typeof sm === "string") {
        classes.push(`sm:${sm}`);
      } else {
        Object.values(sm).forEach((val) => {
          classes.push(`sm:${val}`);
        });
      }
    }

    if (md) {
      if (typeof md === "string") {
        classes.push(`md:${md}`);
      } else {
        Object.values(md).forEach((val) => {
          classes.push(`md:${val}`);
        });
      }
    }

    if (lg) {
      if (typeof lg === "string") {
        classes.push(`lg:${lg}`);
      } else {
        Object.values(lg).forEach((val) => {
          classes.push(`lg:${val}`);
        });
      }
    }

    if (xl) {
      if (typeof xl === "string") {
        classes.push(`xl:${xl}`);
      } else {
        Object.values(xl).forEach((val) => {
          classes.push(`xl:${val}`);
        });
      }
    }

    if (xxl) {
      if (typeof xxl === "string") {
        classes.push(`2xl:${xxl}`);
      } else {
        Object.values(xxl).forEach((val) => {
          classes.push(`2xl:${val}`);
        });
      }
    }
  } else {
    // auto size
    classes.push("flex-1");
  }

  return classes;
};
