import { screen, render } from "utils";
import { Box } from "../Box";

describe("👉 <Box>", () => {
  test("Renders children with correct tag", () => {
    const { container, rerender } = render(<Box>Box</Box>);
    expect(screen.getByText("Box")).toBeInTheDocument();
    // default tag is div
    expect(container.querySelector("div")).toBeInTheDocument();

    // with span
    rerender(<Box tag="span">with custom tag</Box>);
    expect(container.querySelector("span")).toBeInTheDocument();
  });
});
