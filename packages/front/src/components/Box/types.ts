type WidthType =
  | "w-full"
  | "w-1/2"
  | "w-1/3"
  | "w-2/3"
  | "w-1/4"
  | "w-2/4"
  | "w-3/4"
  | "w-1/5"
  | "w-2/5"
  | "w-3/5"
  | "w-4/5"
  | "w-1/6"
  | "w-2/6"
  | "w-3/6"
  | "w-4/6"
  | "w-5/6"
  | "w-1/12"
  | "w-2/12"
  | "w-3/12"
  | "w-4/12"
  | "w-5/12"
  | "w-6/12"
  | "w-7/12"
  | "w-8/12"
  | "w-9/12"
  | "w-10/12"
  | "w-11/12";

// type GrowType = "flex-grow" | "flex-grow-0" | "flex-grow-max";

// type ShrinkType = "flex-shrink" | "flex-shrink-0";

type OrderType =
  | "order-first"
  | "order-last"
  | "order-none"
  | "order-1"
  | "order-2"
  | "order-3"
  | "order-4"
  | "order-5"
  | "order-6"
  | "order-7"
  | "order-8"
  | "order-9"
  | "order-10"
  | "order-11"
  | "order-12";

// type FlexType = "flex-initial" | "flex-1" | "flex-auto" | "flex-none";

export type BoxProps = React.PropsWithoutRef<JSX.IntrinsicElements["div"]> & {
  sm?: WidthType | { width?: WidthType; order?: OrderType };
  // | {
  //     shrink?: ShrinkType;
  //     grow?: GrowType;
  //     order?: OrderType;
  //   };
  md?: WidthType | { width?: WidthType; order?: OrderType };
  // | {
  //     shrink?: ShrinkType;
  //     grow?: GrowType;
  //     order?: OrderType;
  //   };
  lg?: WidthType | { width?: WidthType; order?: OrderType };
  // | {
  //     shrink?: ShrinkType;
  //     grow?: GrowType;
  //     order?: OrderType;
  //   };
  xl?: WidthType | { width?: WidthType; order?: OrderType };
  // | {
  //     shrink?: ShrinkType;
  //     grow?: GrowType;
  //     order?: OrderType;
  //   };
  xxl?: WidthType | { width?: WidthType; order?: OrderType };
  tag?: "div" | "span";
  noMargin?: boolean;
  noPadding?: boolean;
};
