import { useMemo } from "react";
import clsx from "clsx";
import { useRoundedClass } from "utils";
import { TypographyProps } from "./types";
import { getClassNames } from "./utils";
/**
 *
 * @param tag - {string} element name
 * @param color - {string} text color class
 * @param lineHeight - {string} lineHeight class
 * @param bold - {boolean | string} activate font weight|font weight class
 * @param marginBottom - {boolean | string} activate margin bottom | custom margin class
 *
 */
export const Typography = ({
  tag = "div",
  color,
  size,
  lineHeight,
  bold = false,
  marginBottom = false,
  children,
  className,
}: TypographyProps) => {
  const classes = [
    useRoundedClass(),
    ...useMemo(() => {
      return getClassNames({
        size,
        color,
        bold,
        lineHeight,
        marginBottom,
        tag,
      });
    }, [size, color, bold, lineHeight, marginBottom, tag]),
  ];

  const Component = tag as keyof JSX.IntrinsicElements;

  return <Component className={clsx(classes, className)}>{children}</Component>;
};
