import { screen, render } from "utils";
import { Typography } from "../Typography";

describe("👉 <Typography>", () => {
  test("Renders with correct tag", () => {
    const { container } = render(<Typography tag="span">Content</Typography>);
    expect(container.querySelector("span").innerHTML).toBe("Content");
  });
});
