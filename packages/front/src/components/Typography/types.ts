export type TypographyProps = {
  children?: React.ReactNode;
  className?: string;
  tag?:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "p"
    | "del"
    | "em"
    | "ins"
    | "small"
    | "span"
    | "mark"
    | "code"
    | "div";

  size?:
    | "text-xxs"
    | "text-xs"
    | "text-sm"
    | "text-base"
    | "text-lg"
    | "text-xl"
    | "text-2xl"
    | "text-3xl"
    | "text-4xl"
    | "text-5xl"
    | "text-6xl";

  bold?:
    | boolean
    | "font-light"
    | "font-normal"
    | "font-medium"
    | "font-semibold"
    | "font-bold"
    | "font-extrabold"
    | "font-black";

  color?:
    | "text-primary"
    | "text-secondary"
    | "text-tertiary"
    | "text-red"
    | "text-teal"
    | "text-orange"
    | "text-white"
    | "text-green"
    | "text-gray"
    | "text-black"
    | "text-black-transparent"
    | "text-inverse-higher"
    | "text-inverse-hight"
    | "text-inverse-medium";

  marginBottom?:
    | boolean
    | "mb-px"
    | "mb-1"
    | "mb-2"
    | "mb-3"
    | "mb-4"
    | "mb-5"
    | "mb-6"
    | "mb-8"
    | "mb-10"
    | "mb-12";

  lineHeight?:
    | "leading-none"
    | "leading-tight"
    | "leading-snug"
    | "leading-normal"
    | "leading-relaxed"
    | "leading-loose";
};
