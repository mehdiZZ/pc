import { TypographyProps } from "./types";

export const getClassNames = ({
  size,
  tag = "div",
  color,
  lineHeight,
  bold,
  marginBottom,
}: Pick<
  TypographyProps,
  "size" | "tag" | "color" | "lineHeight" | "bold" | "marginBottom"
>) => {
  const classes = [];
  if (!size) {
    switch (tag) {
      case "h1":
        classes.push("text-3xl");
        break;
      case "h2":
        classes.push("text-2xl");
        break;
      case "h3":
        classes.push("text-xl");
        break;
      case "h4":
        classes.push("text-lg");
        break;
      case "h5":
        classes.push("text-lg");
        break;
      case "code":
        classes.push("bg-gray-100 text-black-transparent rounded py-1 px-2");
        break;
      case "mark":
        classes.push("bg-yellow text-black-transparent rounded py-1 px-2");
        break;
      case "h6":
      case "p":
      case "del":
      case "em":
      case "ins":
      case "span":
      case "div":
      default:
        classes.push("text-base");
        break;
      case "small":
        classes.push("text-sm");
    }
  } else {
    classes.push(size);
  }

  if (color) {
    classes.push(color);
  } else {
    if (["h1", "h2", "h3"].includes(tag)) {
      classes.push("text-inverse-higher");
    }
  }

  if (lineHeight) {
    classes.push(lineHeight);
  }

  if (bold) {
    if (typeof bold === "string") {
      classes.push(bold);
    } else {
      classes.push("font-semibold");
    }
  }

  if (marginBottom) {
    if (typeof marginBottom === "string") {
      classes.push(marginBottom);
    } else {
      classes.push(tag === "p" ? "mb-4" : "mb-2");
    }
  }

  return classes;
};
