import { render, screen, waitFor } from "utils";
import { Img } from "../Img";

describe("👉 <Img>", () => {
  test('Img must have src = "/test/test.png" and alt = "Logo"', async () => {
    render(<Img src="/test/test.png" alt="alternative" preload={false} />);
    const image = screen.getByRole("img");
    expect(image).toHaveAttribute("src", "/test/test.png");
    expect(image).toHaveAttribute("alt", "alternative");
  });
});
