export type ImageProps = React.PropsWithoutRef<JSX.IntrinsicElements["img"]> & {
  preload?: boolean;
};

export type ImageStatus = "idle" | "loading" | "loaded" | "error";
