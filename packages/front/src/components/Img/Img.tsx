import { useState, useEffect } from "react";
import { ImageStatus, ImageProps } from "./types";
import loading from "./images/loading.png";
import failed from "./images/failed.png";
import clsx from "clsx";

export const Img = ({
  src = "",
  preload = true,
  alt,
  width,
  height,
  className,
  ...props
}: ImageProps) => {
  const [status, setStatus] = useState<ImageStatus>("idle");

  useEffect(() => {
    if (preload) {
      const image: HTMLImageElement = new Image();
      image.onloadstart = () => setStatus("loading");
      image.onload = () => setStatus("loaded");
      image.onerror = () => setStatus("error");
      image.src = src;
    }
  }, []);

  let imgSrc = src;
  if (preload) {
    if (status === "loaded") {
      imgSrc = src;
    } else if (status === "idle" || status === "loading") {
      imgSrc = loading;
    } else if (status === "error") {
      imgSrc = failed;
    }
  }

  return (
    <img
      src={imgSrc}
      width={width}
      height={height}
      className={clsx("max-w-full", className)}
      alt={alt}
      {...props}
    />
  );
};
