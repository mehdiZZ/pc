import { render, screen, waitFor } from "utils";
import userEvent from "@testing-library/user-event";
import { Tabs } from "../Tabs";
import { TabNavs } from "../TabNavs";
import { Tab } from "../Tab";
import { TabContent } from "../TabContent";
import { TabPanel } from "../TabPanel";

describe("👉 <Tab> & <Tabs> & <TabContent> & <TabNavs> & <TabPanel>", () => {
  test("Renders and handling tabs change", async () => {
    render(
      <Tabs initialValue="1">
        <TabNavs>
          <Tab id="1">TabOne</Tab>
          <Tab id="2">TabTwo</Tab>
        </TabNavs>
        <TabContent>
          <TabPanel id="1">
            <p>ContentOne</p>
          </TabPanel>
          <TabPanel id="2">
            <p>ContentTwo</p>
          </TabPanel>
        </TabContent>
      </Tabs>
    );

    expect(screen.getByText("ContentOne")).toBeInTheDocument();
    expect(screen.queryByText("ContentTwo")).not.toBeInTheDocument();

    userEvent.click(screen.getByText("TabTwo"));

    expect(screen.getByText("ContentTwo")).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.queryByText("ContentOne")).not.toBeInTheDocument();
    });
  });
});
