import { PropsWithoutRef, useContext } from "react";
import clsx from "clsx";
import { TabContext } from "./Tabs";

export const TabNavs = ({
  children,
  className,
  ...props
}: PropsWithoutRef<JSX.IntrinsicElements["ul"]>) => {
  const classes = ["flex flex-wrap rounded-t-xl"];

  const tabContext = useContext(TabContext);

  if (tabContext.vertical) {
    classes.push("flex-col items-stretch justify-start");
  }

  return (
    <ul {...props} className={clsx(className, classes)}>
      {children}
    </ul>
  );
};
