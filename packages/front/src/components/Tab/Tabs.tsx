import { useState, createContext } from "react";
import { TabContextDefaultsType, TabsProps } from "./types";
import clsx from "clsx";
import { useRoundedClass } from "utils";

export const TabContext = createContext<TabContextDefaultsType>({
  activeTab: 0,
  setActiveTab: () => {},
  justified: false,
  vertical: false,
});

/**
 *
 * @param initialValue - {string | number} init active item
 * @param justified - {boolean} justify nav items
 * @param vertical - {boolean} verticals the tab
 *
 * @example
  <Tabs initialValue="0">
     <TabNavs>
       <Tab id="0">test1</Tab>
       <Tab id="1">test2</Tab>
     </TabNavs>
     <TabContent>
       <TabPanel id="0">
         <p>1111</p>
       </TabPanel>
       <TabPanel id="1">
         <p>2222</p>
       </TabPanel>
     </TabContent>
   </Tabs>
 */
export const Tabs = ({
  initialValue,
  children,
  justified = false,
  vertical = false,
}: TabsProps) => {
  const [activeTab, setActiveTab] = useState(initialValue);
  const classes = ["shadow-content", useRoundedClass("rounded-xl")];

  if (vertical) {
    classes.push("flex");
  }

  return (
    <TabContext.Provider
      value={{ activeTab, setActiveTab, justified, vertical }}
    >
      <div className={clsx(classes)}>{children}</div>
    </TabContext.Provider>
  );
};
