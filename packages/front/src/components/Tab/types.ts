import { Dispatch } from "react";

export type TabProps = React.PropsWithoutRef<JSX.IntrinsicElements["li"]> & {
  id: string | number;
  onClick?: () => void;
};

export type TabsProps = React.PropsWithoutRef<JSX.IntrinsicElements["div"]> & {
  initialValue: string | number;
  justified?: boolean;
  vertical?: boolean;
};

export type TabContextDefaultsType = {
  activeTab: string | number;
  setActiveTab: Dispatch<any>;
  justified: boolean;
  vertical: boolean;
};

export type TabPanelProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["div"]
> & {
  id: string | number;
};
