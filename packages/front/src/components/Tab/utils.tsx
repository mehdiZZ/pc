import { TabContextDefaultsType } from "./types";

export const getClassNames = (
  tabContext: TabContextDefaultsType,
  id: string,
  isRtl: boolean
) => {
  return [
    "py-4 px-6 cursor-pointer",
    tabContext.vertical
      ? isRtl
        ? "rounded-r-xl"
        : "rounded-l-xl"
      : "rounded-t-xl",
    tabContext.justified && "flex-1 flex justify-center items-center",
    tabContext.activeTab === id && "bg-content",
    "hover:bg-content transition duration-300",
  ];
};
