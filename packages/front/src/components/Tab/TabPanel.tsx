import { useContext } from "react";
import { TabContext } from "./Tabs";
import clsx from "clsx";
import { TabPanelProps } from "./types";
import { AnimatePresence, motion } from "framer-motion";

/**
 * @param id - {string | number}  unique id
 */
export const TabPanel = ({ children, id, className }: TabPanelProps) => {
  const tabContext = useContext(TabContext);

  return (
    <AnimatePresence initial={false}>
      {tabContext.activeTab === id && (
        <motion.div
          initial="collapsed"
          animate="open"
          exit="collapsed"
          variants={{
            open: { opacity: 1, height: "auto" },
            collapsed: { opacity: 0, height: 0 },
          }}
          className={clsx(className, "overflow-hidden")}
        >
          {children}
        </motion.div>
      )}
    </AnimatePresence>
  );
};
