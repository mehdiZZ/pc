import { MouseEvent, useContext, useMemo } from "react";
import { TabContext } from "./Tabs";
import clsx from "clsx";
import { TabProps } from "./types";
import { getClassNames } from "./utils";
import { useLanguage } from "utils";

/**
 * @param  id - {string | number} unique id
 * @param onClick - {function} additional click handler for the tap
 */
export const Tab = ({
  children,
  id,
  onClick = () => {},
  className,
  ...props
}: TabProps) => {
  const { isRtl } = useLanguage();

  const tabContext = useContext(TabContext);
  const classes = useMemo(() => {
    return getClassNames(tabContext, id, isRtl);
  }, [tabContext, id, isRtl]);

  const handleClick = (event: MouseEvent<HTMLLIElement>) => {
    tabContext.setActiveTab(id);
    onClick(event);
  };

  return (
    <li {...props} onClick={handleClick} className={clsx(classes, className)}>
      {children}
    </li>
  );
};
