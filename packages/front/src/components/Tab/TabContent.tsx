import { PropsWithoutRef, useContext } from "react";
import clsx from "clsx";
import { TabContext } from "./Tabs";
import { useLanguage } from "utils";

export const TabContent = ({
  children,
  className,
  ...props
}: PropsWithoutRef<JSX.IntrinsicElements["div"]>) => {
  const { isRtl } = useLanguage();
  const tabContext = useContext(TabContext);
  const classes = ["bg-content", "p-10", "flex-1"];

  if (tabContext.vertical) {
    classes.push(isRtl ? "rounded-l-xl" : "rounded-r-xl");
  } else {
    classes.push("rounded-b-xl");
  }

  return (
    <div {...props} className={clsx(classes, className)}>
      {children}
    </div>
  );
};
