export const getClassNames = (theme?: string) => {
  const classes = [
    "flex text-sm px-5 py-4 text-white items-center justify-between overflow-hidden",
  ];
  switch (theme) {
    default:
    case "info":
      classes.push("bg-teal");
      break;
    case "success":
      classes.push("bg-green");
      break;
    case "danger":
      classes.push("bg-red");
      break;
    case "warning":
      classes.push("bg-orange");
      break;
  }

  return classes;
};

export const variants = {
  open: { scaleY: 1 },
  close: { scaleY: 0 },
};
