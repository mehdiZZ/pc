import { General } from "utils";

export type AlertProps = React.PropsWithoutRef<JSX.IntrinsicElements["div"]> & {
  theme?: "info" | "success" | "warning" | "danger";
  hidCloseButton?: boolean;
  rounded?: General["rounded"];
  closed?: boolean;
  onClose?: (closed: boolean) => void;
};
