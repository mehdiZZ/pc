import userEvent from "@testing-library/user-event";
import { render, screen, waitFor } from "utils";
import { Alert } from "../Alert";

describe("👉 <Alert>", () => {
  test("Renders uncontrolled alert", async () => {
    const { rerender } = render(<Alert>Alert Content</Alert>);
    // with close button
    expect(screen.getByText(/^Alert Content/i)).toBeInTheDocument();

    userEvent.click(screen.getByTestId("close-alert"));

    await waitFor(() => {
      expect(screen.queryByText(/^Alert Content/i)).not.toBeInTheDocument();
    });

    // without close button
    rerender(<Alert hidCloseButton>Alert Content</Alert>);
    expect(screen.queryByTestId("close-alert")).not.toBeInTheDocument();
  });

  test("Renders Controlled alert", async () => {
    let closed = false;
    const onClose = jest.fn();

    render(
      <Alert closed={closed} onClose={onClose}>
        Controlled Alert Content
      </Alert>
    );

    expect(screen.getByText(/^Controlled Alert Content/i)).toBeInTheDocument();

    userEvent.click(screen.getByTestId("close-alert"));

    // Closing alert
    expect(onClose).toBeCalledWith(!closed);
  });
});
