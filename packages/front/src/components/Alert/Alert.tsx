import { AlertProps } from "./types";
import { motion, AnimatePresence } from "framer-motion";
import clsx from "clsx";
import { useRoundedClass } from "utils";
import { getClassNames, variants } from "./utils";
import { CloseOutlined } from "@ant-design/icons";
import { useEffect, useMemo, useState } from "react";

/**
 * @param theme - {string} alert theme
 * @param rounded - {string | boolean} set border radius
 * @param hidCloseButton - {boolean} remove close button
 * @param closed - {boolean} is alert closed (controlled alert)
 * @param onClose - {function} set alert status (controlled alert)
 * @example
  const [closed, setClose] = React.useState(false);
  <Alert closed={closed} onClose={() => setClose(true)}>
    lorem ipsum inventore quidem
  </Alert>
 */
export const Alert = ({
  theme,
  rounded,
  closed,
  onClose,
  hidCloseButton = false,
  children,
  className,
}: AlertProps) => {
  const classes = [
    useRoundedClass(rounded),
    ...useMemo(() => {
      return getClassNames(theme);
    }, [theme]),
  ];

  const [alertClosed, setAlertClosed] = useState(false);

  useEffect(() => {
    typeof closed !== "undefined" && setAlertClosed(closed);
  }, [closed]);

  const onAlertClose = () => {
    if (onClose) {
      onClose(!alertClosed);
    } else {
      setAlertClosed(true);
    }
  };

  return (
    <AnimatePresence>
      {!alertClosed && (
        <motion.div
          className={clsx(classes, className)}
          animate={alertClosed ? "close" : "open"}
          variants={variants}
          exit={"close"}
          role="alert"
        >
          <>{children}</>
          {!hidCloseButton && (
            <div className="mb-auto">
              <button
                data-testid="close-alert"
                aria-label="close"
                onClick={onAlertClose}
              >
                <CloseOutlined />
              </button>
            </div>
          )}
        </motion.div>
      )}
    </AnimatePresence>
  );
};
