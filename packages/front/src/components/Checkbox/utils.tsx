import { CheckboxProps } from "./types";

export const getClassNames = ({
  theme,
  disabled,
  readOnly,
}: Pick<CheckboxProps, "theme" | "disabled" | "readOnly">) => {
  const classes = [
    "checkbox w-4 h-4 bg-primary-inverse bg-center cursor-pointer bg-no-repeat bg-full select-none inline-flex justify-center items-center focus:ring focus:outline-none appearance-none transition-all duration-300",
  ];

  switch (theme) {
    case "default":
      classes.push("checked:bg-tertiary ");
      break;
    case "primary":
      classes.push("checked:bg-primary");
      break;
    case "secondary":
      classes.push("checked:bg-secondary");
      break;
    case "info":
      classes.push("checked:bg-teal");
      break;
    case "success":
      classes.push("checked:bg-green");
      break;
    case "danger":
      classes.push("checked:bg-red");
      break;
    case "warning":
      classes.push("checked:bg-orange");
      break;
  }

  if (disabled || readOnly) {
    classes.push("opacity-50 cursor-not-allowed");
  }

  return classes;
};
