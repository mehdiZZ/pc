import { General } from "utils";

export type CheckboxProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["input"]
> & {
  theme?: General["theme"];
};
