import { render, screen } from "utils";
import userEvent from "@testing-library/user-event";
import { Checkbox } from "../Checkbox";

describe("👉 <Checkbox>", () => {
  test("Toggle element by selecting checkbox", () => {
    render(<Checkbox />);

    expect(screen.getByRole("checkbox")).not.toBeChecked();

    userEvent.click(screen.getByRole("checkbox"));

    expect(screen.getByRole("checkbox")).toBeChecked();
  });
});
