import { forwardRef, useMemo } from "react";
import clsx from "clsx";
import { useRoundedClass } from "utils";
import { CheckboxProps } from "./types";
import { getClassNames } from "./utils";

/**
 * @param theme - {string} checkbox theme
 * @param disabled - {boolean} is disabled
 * @param readOnly - {boolean} is readonly
 * @param checked - {boolean} is checked
 *
 * @example
  <Label>
    <Typography>label</Typography>
    <Checkbox className="mx-2" />
  </Label>
 */
export const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>(
  (
    {
      theme = "default",
      disabled = false,
      readOnly = false,
      className,
      ...props
    },
    ref
  ) => {
    const classes = [
      useRoundedClass(),
      ...useMemo(() => {
        return getClassNames({ theme, disabled, readOnly });
      }, [theme, disabled, readOnly]),
    ];

    return (
      <input
        {...props}
        disabled={disabled}
        readOnly={readOnly}
        className={clsx(classes, className)}
        ref={ref}
        type="checkbox"
      />
    );
  }
);
