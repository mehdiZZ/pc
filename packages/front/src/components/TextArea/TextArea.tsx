import { forwardRef, useMemo, ChangeEvent, useState } from "react";
import { TextAreaProps } from "./types";
import clsx from "clsx";
import { useRoundedClass } from "utils";
import { getClassNames } from "./utils";
import { Typography } from "components/Typography";
/**
 * @param value - {string} textarea value
 * @param defaultValue - {string} textarea default value
 * @param rows - {number} default rows
 * @param minRows - {number} min rows
 * @param maxRows - {number} max rows
 *
 */
export const TextArea = forwardRef<HTMLTextAreaElement, TextAreaProps>(
  (
    {
      children,
      value,
      rows = 4,
      minRows = 4,
      maxRows = 10,
      className,
      disabled = false,
      readOnly = false,
      rounded,
      ...props
    },
    ref
  ) => {
    const classes = [
      useRoundedClass(rounded),
      ...useMemo(() => {
        return getClassNames({ disabled, readOnly });
      }, [disabled, readOnly]),
    ];

    const [autoRows, setRows] = useState<number>(rows);

    const onChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
      //
      const lineHeight = parseInt(
        window.getComputedStyle(event.target).getPropertyValue("line-height"),
        10
      );

      //
      const previousRows = event.target.rows;
      event.target.rows = minRows;
      const currentRows = ~~(event.target.scrollHeight / lineHeight);

      //
      if (currentRows === previousRows) {
        event.target.rows = currentRows;
      }

      if (currentRows >= maxRows) {
        event.target.rows = maxRows;
        event.target.scrollTop = event.target.scrollHeight;
      }

      //
      setRows(currentRows < maxRows ? currentRows : maxRows);

      //
      if (props.onChange) {
        props.onChange(event);
      }
    };

    return (
      <>
        <textarea
          {...props}
          disabled={disabled}
          readOnly={readOnly}
          value={value}
          className={clsx(classes, className)}
          onChange={onChange}
          rows={autoRows}
          ref={ref}
        >
          {children}
        </textarea>
        {props.error && (
          <Typography color="text-red" tag="span" className="text-xxs mx-2">
            {props.error}
          </Typography>
        )}
      </>
    );
  }
);
