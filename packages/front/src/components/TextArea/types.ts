import { General } from "utils";

export type TextAreaProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["textarea"]
> & {
  minRows?: number;
  maxRows?: number;
  rounded?: General["rounded"];
  error?: string;
};
