import { TextAreaProps } from "./types";

export const getClassNames = ({
  disabled,
  readOnly,
}: Pick<TextAreaProps, "disabled" | "readOnly">) => {
  const classes = [
    "textarea bg-content p-2 resize-none focus:shadow focus:outline-none",
  ];

  if (!disabled && !readOnly) {
    classes.push("focus:shadow");
  } else {
    classes.push("opacity-50 cursor-not-allowed pointer-events-none");
  }

  return classes;
};
