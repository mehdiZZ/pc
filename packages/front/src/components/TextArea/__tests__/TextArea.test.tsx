import { render, screen } from "utils";
import { TextArea } from "../TextArea";
import userEvent from "@testing-library/user-event";

describe("👉 <TextArea>", () => {
  test("Default behavior", async () => {
    render(<TextArea data-testid="textArea" />);

    userEvent.type(screen.getByTestId("textArea"), "newValue");

    expect(screen.getByTestId("textArea")).toHaveValue("newValue");
  });

  test("Props", async () => {
    const changeHandler = jest.fn();

    render(
      <TextArea
        data-testid="textArea"
        placeholder={"placeholder"}
        onChange={changeHandler}
      ></TextArea>
    );

    expect(screen.getByTestId("textArea")).toHaveAttribute(
      "placeholder",
      "placeholder"
    );

    userEvent.type(screen.getByTestId("textArea"), "newValue");

    expect(changeHandler).toHaveBeenCalled();
  });
});
