import { ReactNode } from "react";
import { WidgetProps } from "./types";
import { useRoundedClass } from "utils";
import clsx from "clsx";
import { motion, AnimatePresence } from "framer-motion";
import { Spinner } from "components/Spinner";
import { CloseOutlined, DownOutlined, SyncOutlined } from "@ant-design/icons";
import { Button } from "components/Button";

/**
 *  @param title - {string | JSX} widget title
 *  @param footer - {string | JSX} footer content
 *  @param customHeader - {string | JSX} your custom header
 *  @param hideCollapseButton - {boolean} hide collapse icon from header,
 *  @param isCollapsed - {boolean} is widget open
 *  @param hideRemoveButton - {boolean} hide remove button from header
 *  @param isRemoved - {boolean} remove widget from page
 *  @param hideRefreshButton - {boolean} hide refresh button from header
 *  @param onRefresh - {Function} handle refreshing widget
 *  @param loading - {boolean} show spinner instead of widget content
 *
 *  @example
    const [loading, setLoading] = useState(false);
    const [removed, setRemove] = React.useState(false);
    const [collapsed, setCollapse] = React.useState(false);
      <Widget
           title="Test"
           footer={<h3>Test</h3>}
           loading={loading}
           isRemoved={removed}
           onRemove={() => {
             setRemove(true);
           }}
           isCollapsed={collapsed}
           onCollapse={() => {
             setCollapse(!collapsed);
           }}
           onRefresh={() => {
             setLoading(true);
             console.log("hello");
             setTimeout(() => {
               setLoading(false);
             }, 3000);
           }}
         >
           Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur
           perferendis, iste omnis tempora placeat necessitatibus incidunt,
           magni, reprehenderit saepe ullam ut deleniti modi repellat.
           Accusantium atque sunt optio non deleniti.
     </Widget>
  
 */

export const Widget = ({
  children,
  widgetTitle = "",
  footer = "",
  customHeader,
  hideCollapseButton = false,
  isCollapsed = false,
  onCollapse = () => {},
  isRemoved = false,
  onRemove = () => {},
  hideRemoveButton = false,
  hideRefreshButton = false,
  onRefresh = () => {},
  loading = false,
}: WidgetProps) => {
  const classes = [
    "shadow-content text-inverse-medium bg-content relative flex flex-col",
    useRoundedClass("rounded-xl"),
  ];

  const getHeader = () => {
    if (customHeader) {
      return customHeader;
    } else {
      return (
        <header className="py-3 px-5 text-inverse-higher flex select-none">
          <div className="text-lg">
            {typeof widgetTitle === "string" ? (
              <h3>{widgetTitle}</h3>
            ) : (
              widgetTitle
            )}
          </div>
          <div className="flex-grow flex justify-end items-center">
            {/* collapse button */}
            {!hideCollapseButton && (
              <Button
                className="widget-collapse"
                theme={"transparent"}
                size="small"
                onClick={onCollapse}
              >
                <motion.i animate={{ rotateX: isCollapsed ? 180 : 0 }}>
                  <DownOutlined className="text-sm" />
                </motion.i>
              </Button>
            )}
            {/* onRefresh */}
            {!hideRefreshButton && (
              <Button
                className="widget-refresh"
                theme={"transparent"}
                size="small"
                onClick={onRefresh}
              >
                <SyncOutlined className="text-sm" />
              </Button>
            )}
            {/* removed */}
            {!hideRemoveButton && (
              <Button
                className="widget-remove"
                theme={"transparent"}
                size="small"
                onClick={onRemove}
              >
                <CloseOutlined className="text-sm" />
              </Button>
            )}
          </div>
        </header>
      );
    }
  };

  const getFooter = (footer: ReactNode) => {
    return (
      <footer className="bg-content rounded-b-xl py-3 px-5 text-inverse-high">
        {footer}
      </footer>
    );
  };

  return (
    <AnimatePresence>
      {!isRemoved && (
        <motion.section exit={{ opacity: 0 }} className={clsx(classes)}>
          {getHeader()}
          <motion.div
            className={"overflow-hidden flex-col"}
            initial="open"
            animate={!isCollapsed ? "open" : "collapsed"}
            variants={{
              open: { opacity: 1, height: "auto" },
              collapsed: { opacity: 0, height: 0 },
            }}
            transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
          >
            <div className="py-3 px-5">{children}</div>
            {footer && getFooter(footer)}
          </motion.div>
          {loading && <Spinner />}
        </motion.section>
      )}
    </AnimatePresence>
  );
};
