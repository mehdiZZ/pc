import { screen, render } from "utils";
import userEvent from "@testing-library/user-event";
import { Widget } from "../Widget";

describe("👉 <Widget>", () => {
  test("Renders children", () => {
    const onRemove = jest.fn();
    const onRefresh = jest.fn();
    const onCollapse = jest.fn();
    const { container } = render(
      <Widget
        widgetTitle={"Title"}
        footer={<h3>Footer</h3>}
        onRemove={() => {
          onRemove();
        }}
        onRefresh={() => {
          onRefresh();
        }}
        onCollapse={() => {
          onCollapse();
        }}
      >
        Content
      </Widget>
    );

    // title & Content
    expect(screen.getByText("Title")).toBeInTheDocument();
    expect(screen.getByText("Content")).toBeInTheDocument();

    // footer
    expect(container.querySelector("footer").innerHTML).toBe("<h3>Footer</h3>");

    // event handlers
    userEvent.click(container.querySelector(".widget-remove"));
    expect(onRemove).toHaveBeenCalled();
    userEvent.click(container.querySelector(".widget-refresh"));
    expect(onRefresh).toHaveBeenCalled();
    userEvent.click(container.querySelector(".widget-collapse"));
    expect(onCollapse).toHaveBeenCalled();
  });
});
