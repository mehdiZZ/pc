export type WidgetProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["section"]
> & {
  widgetTitle?: React.ReactNode;
  customHeader?: React.ReactNode;
  footer?: React.ReactNode;
  hideCollapseButton?: boolean;
  isCollapsed?: boolean;
  onCollapse?: () => void;
  loading?: boolean;
  hideRefreshButton?: boolean;
  isRemoved?: boolean;
  onRemove?: () => void;
  hideRemoveButton?: boolean;
  onRefresh?: () => void | undefined;
};
