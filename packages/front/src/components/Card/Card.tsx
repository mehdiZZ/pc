import clsx from "clsx";
import { useRoundedClass } from "utils";
import { CardProps } from "./types";
import { Spinner } from "components/Spinner";

/**
 * @param loading - {boolean} loading
 */
export const Card = ({ children, className, loading = false }: CardProps) => {
  const classes = [
    "flex flex-col relative shadow-content bg-content",
    useRoundedClass("rounded-xl"),
  ];

  return (
    <section className={clsx(classes, className)}>
      <>
        {children}
        {loading && <Spinner />}
      </>
    </section>
  );
};
