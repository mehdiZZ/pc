import { screen } from "@testing-library/react";
import { render } from "utils";
import { Card, CardBody } from "../";

describe("👉 <Card> & <CardBody>", () => {
  test("Renders Children", () => {
    const { rerender, container } = render(
      <Card>
        <CardBody>Yo</CardBody>
      </Card>
    );
    expect(screen.getByText("Yo")).toBeInTheDocument();

    rerender(
      <Card loading>
        <CardBody>Loading</CardBody>
      </Card>
    );

    // check for spinner
    expect(container.querySelector(".spinner")).toBeInTheDocument();
  });
});
