import { PropsWithoutRef } from "react";
import { useRoundedClass } from "utils";
import clsx from "clsx";

export const CardBody = ({
  children,
  className,
  ...props
}: PropsWithoutRef<JSX.IntrinsicElements["div"]>) => {
  const classes = [
    "sm:p-5 sm:pb-6 md:p-6 md:pb-8 flex-auto",
    useRoundedClass("rounded-xl"),
  ];

  return (
    <div {...props} className={clsx(classes, className)}>
      {children}
    </div>
  );
};
