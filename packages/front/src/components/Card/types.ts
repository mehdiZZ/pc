export type CardProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["section"]
> & {
  loading?: boolean;
};
