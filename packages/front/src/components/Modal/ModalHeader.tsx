import { FC } from "react";

export const ModalHeader: FC = ({ children }) => {
  return (
    <div className="text-center pt-5 px-5 font-semibold text-3xl text-inverse-higher">
      {children}
    </div>
  );
};
