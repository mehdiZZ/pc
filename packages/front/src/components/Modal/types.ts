export type ModalProps = React.PropsWithoutRef<JSX.IntrinsicElements["div"]> & {
  isOpen?: boolean;
  onClose?: () => void;
  hideCloseButton?: boolean;
  size?: "small" | "medium" | "large" | "full";
};
