import { getByText, render, screen, waitFor } from "utils";
import userEvent from "@testing-library/user-event";
import { Modal } from "../Modal";
import { ModalHeader } from "../ModalHeader";
import { ModalContent } from "../ModalContent";
import { ModalFooter } from "../ModalFooter";

describe("👉 <Modal> & <ModalHeader> & <ModalContent> & <ModalFooter>", () => {
  test("Renders and fires onClose ", async () => {
    const onClose = jest.fn();
    render(
      <Modal isOpen={true}>
        <ModalHeader>Hey its header</ModalHeader>
        <ModalContent>ModalContent</ModalContent>
        <ModalFooter>
          <button onClick={onClose}>Close</button>
        </ModalFooter>
      </Modal>
    );

    expect(screen.getByText("ModalContent")).toBeInTheDocument();

    userEvent.click(screen.getByText("Close"));

    expect(onClose).toBeCalled();
  });
});
