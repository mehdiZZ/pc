import { useMemo } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { useRoundedClass } from "utils";
import clsx from "clsx";
import SimpleBar from "simplebar-react";
import { ModalProps } from "./types";
import { getClassNames } from "./utils";
import { CloseOutlined } from "@ant-design/icons";
/**
 *
 * @param isOpen - {boolean} is modal open
 * @param onClose - {function} handle closing modal
 * @param hidCloseButton - {boolean} hidden close button
 * @param size - {string} size of modal
 *
 * @example
  const [modal, setModal] = React.useState(false);
  <Modal onClose={() => setModal(false)} isOpen={modal} size="small">
    <ModalHeader>
      <h1>
        <CheckOutlined className="text-green" />{" "}
        <Typography tag="span" className="text-xl">
          Hey its header
        </Typography>
      </h1>
    </ModalHeader>
    <ModalContent>
      <p className="text-center">
        Lorem, ipsum dolor sit amet consectetur
      </p>
    </ModalContent>
    <ModalFooter>
      <Button theme="danger" onClick={() => setModal(false)}>
        Close
      </Button>
    </ModalFooter>
  </Modal>
 */
export const Modal = ({
  isOpen = false,
  onClose = () => {},
  hideCloseButton = false,
  children,
  size = "medium",
  className,
}: ModalProps) => {
  const classes = [
    useRoundedClass("rounded-xl"),
    ...useMemo(() => {
      return getClassNames({ size });
    }, [size]),
  ];

  return (
    <AnimatePresence>
      {isOpen && (
        <div
          onClick={onClose}
          className="fixed inset-0 h-screen w-screen z-max flex justify-center items-center bg-opacity-75 filter-backdrop"
        >
          <motion.div
            onClick={(e) => e.stopPropagation()}
            initial={{ scale: 0 }}
            animate={{ scale: 1 }}
            exit={{ scale: 0 }}
            transition={{ duration: 0.2 }}
            className={clsx(classes, className)}
            style={{ background: "var(--gradient-primary)" }}
          >
            {!hideCloseButton && (
              <span
                onClick={onClose}
                className="absolute top-0 right-0 m-2 z-10"
              >
                <CloseOutlined className="cursor-pointer" />
              </span>
            )}
            <SimpleBar className="modal-body">{children}</SimpleBar>
          </motion.div>
        </div>
      )}
    </AnimatePresence>
  );
};
