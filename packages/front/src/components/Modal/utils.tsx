import { ModalProps } from "./types";

export const getClassNames = ({ size }: Pick<ModalProps, "size">) => {
  const classes = ["modal overflow-hidden w-5/6 gradient-primary relative"];
  switch (size) {
    case "small":
      classes.push("md:w-3/6 xl:w-2/6");
      break;
    case "medium":
    default:
      classes.push("md:w-4/6 xl:w-3/6");
      break;
    case "large":
      classes.push("md:w-5/6");
      break;
    case "full":
      classes.push("xl:w-full");
      break;
  }

  return classes;
};
