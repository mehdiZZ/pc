import { FC } from "react";

export const ModalContent: FC = ({ children }) => {
  return <div className="p-5 text-xl leading-loose">{children}</div>;
};
