import { FC } from "react";

export const ModalFooter: FC = ({ children }) => {
  return <div className="py-3 px-5 bg-content text-center">{children}</div>;
};
