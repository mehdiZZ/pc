import { PropsWithoutRef, forwardRef } from "react";

export const Form = forwardRef<
  HTMLFormElement,
  PropsWithoutRef<JSX.IntrinsicElements["form"]>
>(({ children, ...props }, ref) => {
  return (
    <form {...props} ref={ref}>
      {children}
    </form>
  );
});
