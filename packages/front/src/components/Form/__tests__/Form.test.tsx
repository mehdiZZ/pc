import { screen, render } from "utils";
import { Form } from "../Form";

describe("👉 <Form>", () => {
  test("Renders children", () => {
    render(<Form>Form</Form>);
    expect(screen.getByText("Form")).toBeInTheDocument();
  });
});
