import { render } from "utils";
import { Upload } from "../Upload";

describe("👉 <Upload>", () => {
  test("Snapshot upload", () => {
    const { container } = render(<Upload uploadFiles={jest.fn} />);

    expect(container).toMatchSnapshot();
  });
});
