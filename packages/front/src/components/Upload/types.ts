export interface IFile extends File {
  preview: string;
}

export type UploadProps = {
  maxFileNumber?: number;
  loading?: boolean;
  uploadFiles?: (files: File[]) => void;
  clearFiles?: boolean;
  uploadingProgress?: number;
};
