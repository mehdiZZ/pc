import { useMemo, useEffect, useState } from "react";
import { useDropzone, DropzoneOptions } from "react-dropzone";
import clsx from "clsx";
import { IFile, UploadProps } from "./types";
import { clearThumbs, getClassNames } from "./utils";
import { Circle } from "rc-progress";
import { useTranslation } from "react-i18next";
import { CloudUploadOutlined } from "@ant-design/icons";

/**
 * 
 * @example
   const [isLoading, setLoading] = React.useState<boolean>(false);
    const [clearFiles, setClearFiles] = React.useState<boolean>(false);
  
     const uploadFiles = (acceptedFiles: File[]) => {
     setLoading(true);
     setTimeout(() => {
       setLoading(false);
       setClearFiles(true);
     }, 2500);
   };
  
    <Upload
     loading={isLoading}
     clearFiles={clearFiles}
     uploadFiles={uploadFiles}
   />

 */
export const Upload = ({
  accept = ["image/*", "video/*", "audio/*", ".pdf"],
  multiple = true,
  maxFileNumber = 20,
  uploadFiles,
  uploadingProgress,
  loading = false,
  clearFiles = false,
  ...props
}: DropzoneOptions & UploadProps) => {
  const { t } = useTranslation();

  const [files, setFiles] = useState<IFile[]>([]);

  useEffect(
    () => () => {
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  useEffect(() => {
    clearFiles && files.length && setFiles(clearThumbs(files));
  }, [clearFiles, files]);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    accept,
    multiple,
    onDrop: (acceptedFiles) => {
      if (!acceptedFiles.length || acceptedFiles.length >= maxFileNumber)
        return;
      // save files locally for showing previews
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );

      uploadFiles && uploadFiles(acceptedFiles);
    },
    ...props,
  });

  const classes = useMemo(
    () => getClassNames(isDragActive, isDragReject, isDragAccept),
    [isDragActive, isDragReject, isDragAccept]
  );

  return (
    <section className="upload">
      <div className="relative">
        <main
          {...getRootProps({
            className: clsx(classes),
          })}
        >
          <input {...getInputProps()} />
          <div className="text-center">
            {loading ? (
              <div className="w-8 mx-auto mb-1 text-center">
                <Circle
                  strokeWidth={8}
                  strokeColor="currentColor"
                  percent={uploadingProgress}
                />
                <span>{uploadingProgress}%</span>
              </div>
            ) : (
              <CloudUploadOutlined className="text-4xl" />
            )}

            <p>{t("forms:dragFilesHere")}</p>
          </div>
        </main>
      </div>
      {/* <aside className={"flex flex-row flex-wrap mt-4"}>
        {getThumbs(files, clearThumbs, setFiles)}
      </aside> */}
    </section>
  );
};
