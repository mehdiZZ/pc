import { ReactNode } from "react";
import { IFile } from "./types";
import { FileOutlined, PlayCircleOutlined } from "@ant-design/icons";
import { Img } from "components";
import { SERVER_URL } from "utils";
import { getThumbnail } from "routes/media/utils";

export const isImage = (mimeType: string) => {
  return [
    "image/apng",
    "image/bmp",
    "image/gif",
    "image/x-icon",
    "image/jpeg",
    "image/png",
    "image/svg+xml",
    "image/tiff",
    "image/webp",
  ].includes(mimeType);
};

export const isVideoOrAudio = (mimeType: string) => {
  return [
    "audio/wave",
    "audio/wav",
    "audio/x-wav",
    "audio/x-pn-wav",
    "audio/webm",
    "video/webm",
    "audio/ogg",
    "video/ogg",
    "application/ogg",
    "video/x-matroska",
    "video/mp4",
    "video/x-flv",
    "application/x-mpegURL",
    "video/MP2T",
    "video/3gpp",
    "video/quicktime",
    "video/x-msvideo",
    "video/x-ms-wmv",
  ].includes(mimeType);
};

export const ShowSingleFile = ({ type, filename, path, cropped }) => {
  if (isImage(type)) {
    return (
      <Img
        alt={filename}
        src={SERVER_URL + "/" + getThumbnail({ path, cropped })}
        className={"object-cover file-preview w-full h-full"}
      />
    );
  } else {
    return (
      <div className="overflow-hidden text-center file-preview flex">
        {isVideoOrAudio(type) ? <PlayCircleOutlined /> : <FileOutlined />}
        <div
          title={filename}
          className="text-xxs overflow-hidden no-wrap overflow-ellipsis whitespace-nowrap file-name"
        >
          {filename}
        </div>
      </div>
    );
  }
};

export const clearThumbs = (
  files: IFile[],
  shouldRemove?: IFile[]
): IFile[] | [] => {
  if (shouldRemove) {
    return files.filter((file) => {
      const find = shouldRemove.find((f) => f.preview === file.preview);
      if (find) {
        URL.revokeObjectURL(find.preview);
        return false;
      }
      return true;
    });
  } else {
    files.forEach((file) => URL.revokeObjectURL(file.preview));
    return [];
  }
};

export const getClassNames = (
  isDragActive: boolean,
  isDragReject: boolean,
  isDragAccept: boolean
) => {
  return [
    [
      "mb-4 cursor-pointer flex flex-1 flex-col p-6 border-2 border-inverse-medium rounded border-dashed bg-transparent text-gray-600 outline-none transition-all duration-300",
    ],
    ...(isDragActive ? ["border-primary"] : []),
    ...(isDragAccept ? ["border-green"] : []),
    ...(isDragReject ? ["border-red"] : []),
  ];
};
