import { screen, render } from "utils";
import { Flex } from "../Flex";

describe("👉 <Flex>", () => {
  test("Renders children with correct tag", () => {
    const { container, rerender } = render(<Flex>Flex</Flex>);
    expect(screen.getByText("Flex")).toBeInTheDocument();

    rerender(<Flex inline>Flex</Flex>);
    expect(container.querySelector("span")).toBeInTheDocument();
  });
});
