export type FlexProps = React.PropsWithoutRef<JSX.IntrinsicElements["div"]> & {
  col?: boolean;
  reverse?: boolean;
  inline?: boolean;
  wrap?: boolean;
  wrapReverse?: boolean;
  tag?: "div" | "span";
};
