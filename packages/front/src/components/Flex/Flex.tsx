import { useMemo } from "react";
import { FlexProps } from "./types";
import clsx from "clsx";
import { getClassNames } from "./utils";
/**
 * @param col - {boolean} is flex direction column
 * @param reverse - {boolean} is direction reverse
 * @param wrap - {boolean} flex wrap
 * @param wrapReverse - {boolean} wrap reverse
 * @param tag - {string} render element as
 * @param inline - {boolean} flex inline
 */
export const Flex = ({
  col = false,
  reverse = false,
  wrap = true,
  wrapReverse = false,
  tag = "div",
  children,
  className,
  inline = false,
  ...props
}: FlexProps) => {
  const classes = useMemo(() => {
    return getClassNames({ col, reverse, wrap, wrapReverse });
  }, [col, reverse, wrap, wrapReverse]);

  const Component = tag === "div" && inline ? "span" : tag;

  return (
    <Component
      {...props}
      className={clsx(classes, className, !inline ? "flex" : "inline-flex")}
    >
      {children}
    </Component>
  );
};
