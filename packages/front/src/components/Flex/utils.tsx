import { FlexProps } from "./types";

export const getClassNames = ({
  col,
  reverse,
  wrap,
  wrapReverse,
}: FlexProps) => {
  const classes = ["-mx-4"];
  if (col) {
    classes.push(reverse ? "flex-col-reverse" : "flex-col");
  } else if (reverse) {
    classes.push("flex-row-reverse");
  }

  if (wrap || wrapReverse) {
    classes.push(wrap ? "flex-wrap" : "flex-wrap-reverse");
  }
  return classes;
};
