import "tinymce/tinymce";
import "tinymce/icons/default";
import "tinymce/themes/silver";
import "tinymce/skins/ui/oxide/skin.min.css";

//plugins
import "tinymce/plugins/advlist";
import "tinymce/plugins/autolink";
import "tinymce/plugins/lists";
import "tinymce/plugins/link";
import "tinymce/plugins/image";
import "tinymce/plugins/print";
import "tinymce/plugins/preview";
import "tinymce/plugins/anchor";
import "tinymce/plugins/searchreplace";
import "tinymce/plugins/visualblocks";
import "tinymce/plugins/code";
import "tinymce/plugins/fullscreen";
import "tinymce/plugins/insertdatetime";
import "tinymce/plugins/media";
import "tinymce/plugins/table";
import "tinymce/plugins/paste";
import "tinymce/plugins/code";
import "tinymce/plugins/help";
import "tinymce/plugins/wordcount";
import "tinymce/plugins/charmap";
import "tinymce/plugins/textpattern";
import "tinymce/plugins/imagetools";
import "tinymce/plugins/autoresize";
//
import { Editor } from "@tinymce/tinymce-react";
import { Typography } from "components/Typography";
import { SERVER_URL, uploadRequest, useLanguage } from "utils";
import { ReactEditorProps } from "./types";
import { MediaModal, Media } from "routes/media";
import { useState } from "react";

// when importing everything locally (without api key) you have to import these two files as raw and pass it to content_style
import raw from "raw.macro";
const contentCss = raw("tinymce/skins/ui/oxide/content.min.css");
const contentUiCss = raw("tinymce/skins/content/default/content.min.css");

export const TinyMCEEditor = ({ onChange, error, value }: ReactEditorProps) => {
  const { isRtl } = useLanguage();
  const [isOpen, setModal] = useState(false);
  const [selectedFiles, setSelectedFiles] = useState<Media[]>([]);

  return (
    <>
      <Editor
        initialValue={value}
        init={{
          skin: false,
          object_resizing: true,
          content_css: false,
          max_height: 750,
          menubar: true,
          image_title: true,
          image_advtab: true,
          directionality: isRtl ? "rtl" : "ltr",
          file_browser_callback_types: "image",
          content_style: [contentCss, contentUiCss].join("\n"),
          setup: (editor) => {
            editor.ui.registry.addButton("addMedia", {
              icon: "image",
              onAction: function (api) {
                setModal(true);
              },
            });
          },
          file_picker_callback: (cb, value, meta) => {
            const input = document.createElement("input") as any;
            input.setAttribute("type", "file");
            input.setAttribute("accept", "image/*");

            input.onchange = function () {
              const file = this.files[0];

              const reader = new FileReader() as any;
              reader.readAsDataURL(file);
              reader.onload = function () {
                const id = "blobid" + new Date().getTime();
                // @ts-ignore
                const bl = window.tinymce.activeEditor.editorUpload.blobCache;
                const base64 = reader.result.split(",")[1];
                const blobInfo = bl.create(id, file, base64);
                bl.add(blobInfo);
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            input.click();
          },
          images_upload_handler: async (
            blobInfo,
            success,
            failure
            //progress
          ) => {
            try {
              const data = await uploadRequest({ files: [blobInfo.blob()] });
              success(SERVER_URL + "/" + data[0]["path"]);
            } catch (err) {
              failure(err);
              return;
            }
          },
          plugins: [
            "advlist autolink lists link image charmap print preview anchor imagetools autoresize searchreplace visualblocks code fullscreen insertdatetime media table paste code help wordcount textpattern",
          ],
          toolbar:
            "undo redo | fontselect formatselect fontsizeselect | bold italic backcolor | addMedia |  alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent | removeformat | fullscreen | wordcount | help",
        }}
        onEditorChange={onChange}
      />
      {error && (
        <Typography color="text-red" tag="span" className="text-xxs mx-2">
          {error}
        </Typography>
      )}

      <MediaModal
        isModalOpen={isOpen}
        onModalChange={(status) => {
          if (status === false) {
            if (selectedFiles?.length) {
              // @ts-ignore
              window.tinymce.activeEditor.insertContent(
                `<img src=${SERVER_URL + "/" + selectedFiles[0].path}>`
              );
              setSelectedFiles([]);
            }
          }
          setModal(status);
        }}
        selectedFiles={selectedFiles}
        justOneFile={true}
        setSelectedFiles={setSelectedFiles}
      />
    </>
  );
};
