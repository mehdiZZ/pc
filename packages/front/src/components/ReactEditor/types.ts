import { MyControllerProps } from "utils";

export type ReactEditorProps = {
  onChange: (a: string, editor: any) => void;
  value?: any;
  error?: string;
};

export type ReactEditorControllerProps = {
  editorProps: Partial<ReactEditorProps>;
  controllerProps: MyControllerProps<string>;
};
