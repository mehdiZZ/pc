import { Controller } from "react-hook-form";
import { TinyMCEEditor } from "./TinyMCEEditor";
import { ReactEditorControllerProps } from "./types";

export const ReactEditorController = ({
  editorProps,
  controllerProps,
}: ReactEditorControllerProps) => {
  return (
    <Controller
      {...controllerProps}
      name={controllerProps.name}
      control={controllerProps.control}
      defaultValue={controllerProps.defaultValue || ""}
      render={({ onChange, value }) => {
        return (
          <TinyMCEEditor
            {...editorProps}
            onChange={(val) => onChange(val)}
            value={value}
            error={editorProps.error}
          />
        );
      }}
    />
  );
};
