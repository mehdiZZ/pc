import { render, screen } from "utils";
import { TinyMCEEditor } from "../TinyMCEEditor";

describe("👉 <TinyMCEEditor>", () => {
  test("Snapshot TinyMCEEditor", async () => {
    render(
      <div data-testid="editor">
        <TinyMCEEditor
          onChange={jest.fn}
          value="In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content"
        />
      </div>
    );
    expect(screen.getByTestId("editor")).toMatchSnapshot();
  });
});
