import { render, screen } from "utils";
import userEvent from "@testing-library/user-event";
import { Switch } from "../Switch";

describe("👉 <Switch>", () => {
  test("Renders and calls onChange", () => {
    const onChange = jest.fn();
    render(<Switch defaultChecked={false} onChange={onChange} />);

    expect(screen.getByRole("checkbox")).not.toBeChecked();

    userEvent.click(screen.getByRole("checkbox"));

    expect(onChange).toBeCalled();
  });
});
