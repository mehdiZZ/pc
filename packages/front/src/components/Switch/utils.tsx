export const getClassNames = ({
  switchSize,
}: {
  switchSize: "small" | "medium" | "large";
}) => {
  const classes = [
    "inline-flex relative rounded-full cursor-pointer transition-all duration-300 shadow-sm dir-ltr",
  ];

  const childClasses = ["bg-white rounded-full absolute"];

  switch (switchSize) {
    case "small":
      classes.push("w-8 h-4");
      childClasses.push("w-4 h-4");
      break;
    case "medium":
    default:
      classes.push("w-12 h-6");
      childClasses.push("w-6 h-6");
      break;
    case "large":
      classes.push("w-16 h-8");
      childClasses.push("w-8 h-8");
      break;
  }

  return [classes, childClasses];
};
