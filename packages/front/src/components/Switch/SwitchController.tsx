import { Controller } from "react-hook-form";
import { Switch } from "./Switch";
import { SwitchControllerProps } from "./types";

export const SwitchController = ({
  switchProps,
  controllerProps,
}: SwitchControllerProps) => {
  return (
    <Controller
      {...controllerProps}
      name={controllerProps.name}
      control={controllerProps.control}
      defaultValue={controllerProps.defaultValue}
      render={({ onChange, value }) => {
        return (
          <Switch
            {...switchProps}
            onChange={(e) => onChange(e.target.checked)}
            checked={value}
            error={switchProps.error}
          />
        );
      }}
    />
  );
};
