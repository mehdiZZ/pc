import { forwardRef, useMemo } from "react";
import { SwitchProps } from "./types";
import clsx from "clsx";
import { motion } from "framer-motion";
import { getClassNames } from "./utils";
import check from "../../assets/images/check.png";
import cross from "../../assets/images/cross.png";
import { Typography } from "components/Typography";
/**
 * @param checked - {boolean} is switch on
 * @param defaultChecked - {boolean} is switch on (init)
 * @param onChange - {function} onClick handler
 * @param size - {string} switch size
 * @param onBg - {string} background of parent when switch is on
 * @param offBg - {string} background of parent when switch is off
 * @example

 */
export const Switch = forwardRef<HTMLInputElement, SwitchProps>(
  (
    {
      checked,
      defaultChecked = false,
      switchSize = "medium",
      onBg = `url(${check}) 80% center/30% no-repeat #39d05e`,
      offBg = `url(${cross}) 20% center/30% no-repeat #d04f4f`,
      className,
      error,
      ...props
    },
    ref
  ) => {
    //
    const [classes, childClasses] = useMemo(() => {
      return getClassNames({ switchSize });
    }, [switchSize]);

    const variants = {
      isOn: {
        left: 0,
        right: "auto",
      },
      isOff: { right: 0, left: "auto" },
    };

    let isChecked = typeof checked === "undefined" ? defaultChecked : checked;

    //
    return (
      <>
        <label>
          <input
            {...props}
            tabIndex={-1}
            className={"w-0 h-0 invisible opacity-0"}
            type={"checkbox"}
            checked={isChecked}
            ref={ref}
          />
          <div
            style={{ background: `${isChecked ? onBg : offBg}` }}
            className={clsx(classes, className)}
          >
            <motion.div
              animate={isChecked ? "isOn" : "isOff"}
              transition={{ ease: "easeOut", duration: 0.2 }}
              initial={"isOff"}
              variants={variants}
              className={clsx(childClasses)}
            />
          </div>
        </label>
        {error && (
          <Typography color="text-red" tag="span" className="text-xxs mx-2">
            {error}
          </Typography>
        )}
      </>
    );
  }
);
