import { MyControllerProps } from "utils";

export type SwitchProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["input"]
> & {
  onChange?: (ic: boolean) => void;
  switchSize?: "small" | "medium" | "large";
  onBg?: string;
  offBg?: string;
  error?: string;
};

export type SwitchControllerProps = {
  switchProps: SwitchProps;
  controllerProps: MyControllerProps<boolean>;
};
