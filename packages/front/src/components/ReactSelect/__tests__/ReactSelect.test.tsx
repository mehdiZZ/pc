import { render, screen } from "utils";
import { ReactSelect } from "../ReactSelect";

describe("👉 <ReactSelect>", () => {
  test("Snapshot ReactSelect", async () => {
    render(
      <div data-testid="select">
        <ReactSelect
          defaultValue={{
            id: "one",
            name: "one",
          }}
          options={[
            {
              id: "one",
              name: "one",
            },
            { id: "two", name: "two" },
          ]}
        />
      </div>
    );
    expect(screen.getByTestId("select")).toMatchSnapshot();
  });
});
