import { Props as ReactSelectProps } from "react-select";
import { MyControllerProps } from "utils";

export type ReactSelectControllerProps = {
  selectProps: ReactSelectProps<any, boolean>;
  controllerProps: MyControllerProps<any>;
};
