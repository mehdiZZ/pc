export const customStyle = {
  container: (provided, state) => {
    return { ...provided, width: "100%", opacity: state.isDisabled ? 0.5 : 1 };
  },
  control: (provided) => {
    return {
      ...provided,
      borderRadius: "0.3rem",
      ":hover": {
        borderColor: "transparent",
        boxShadow: "0 0.5rem 1rem rgba(0,0,0,.175)",
      },
      backgroundColor: "var(--color-content)",
      border: "none",
      boxShadow: "none",
      minHeight: "36px",
    };
  },

  // menuList: (provided, state) => {
  //   return { ...provided, backgroundColor: "red" };
  // },
  // menuPortal: (provided, state) => {
  //   return { ...provided, backgroundColor: "blue" };
  // },

  // indicatorsContainer: (provided, state) => {
  //   return { ...provided, color: "var(--color-inverse-high)" };
  // },
  indicatorSeparator: (provided) => {
    return { ...provided, backgroundColor: "var(--color-inverse-medium)" };
  },
  dropdownIndicator: (provided) => {
    return {
      ...provided,
      color: "var(--color-inverse-medium)",
      ":hover": { color: "var(--color-inverse-hight)" },
      cursor: "pointer",
    };
  },
  clearIndicator: (provided) => {
    return {
      ...provided,
      color: "var(--color-inverse-medium)",
      ":hover": { color: "var(--color-inverse-hight)" },
      cursor: "pointer",
    };
  },
  menu: (provided, state) => {
    return {
      ...provided,
      backgroundColor: "var(--color-tertiary)",
      boxShadow: "0 0.5rem 1rem rgba(0,0,0,.175)",
    };
  },
  input: (provided, state) => {
    return { ...provided, color: "var(--color-inverse-high)" };
  },
  placeholder: (provided) => {
    return { ...provided, color: "var(--color-inverse-medium)", opacity: 0.6 };
  },
  singleValue: (provided) => {
    return { ...provided, color: "var(--color-inverse-high)" };
  },
  multiValue: (styles, { data }) => {
    return {
      ...styles,
      backgroundColor: "var(--color-primary-inverse)",
      color: "var(--color-primary)",
    };
  },
  multiValueLabel: (styles, { data }) => ({
    ...styles,
    fontWeight: "bold",
  }),
  multiValueRemove: (styles, { data }) => ({
    ...styles,
    color: "#F45722",
    ":hover": {
      color: "white",
      backgroundColor: "#F45722",
    },
  }),
  option: (provided, state) => {
    return {
      ...provided,
      color: "var(--color-tertiary-reverse)",
      ":active": { backgroundColor: "var(--color-content)" },
      backgroundColor:
        state.isFocused || state.isSelected
          ? "var(--color-content)"
          : "transparent",
    };
  },
  noOptionsMessage: (provided) => {
    return {
      ...provided,
      color: "var(--color-inverse-higher)",
    };
  },
};
