import Select, {
  OptionTypeBase,
  Props as ReactSelectProps,
} from "react-select";
import { customStyle } from "./utils";
import { useLanguage } from "utils";
import { useTranslation } from "react-i18next";
import { Typography } from "components/Typography";

const defaultGetOptionValue = (option: any) => option.id;
const defaultGetOptionLabel = (option: any) => option.name;

export const ReactSelect = ({
  isClearable = true,
  error,
  ...props
}: ReactSelectProps<any, boolean>) => {
  const { isRtl } = useLanguage();
  const { t } = useTranslation();

  return (
    <>
      <Select
        getOptionValue={props.getOptionValue || defaultGetOptionValue}
        getOptionLabel={props.getOptionLabel || defaultGetOptionLabel}
        isClearable={isClearable}
        isRtl={isRtl}
        styles={customStyle}
        placeholder={props.placeholder || t("forms:select")}
        {...props}
      />
      {error && (
        <Typography color="text-red" tag="span" className="text-xxs mx-2">
          {error}
        </Typography>
      )}
    </>
  );
};
