import { Controller } from "react-hook-form";
import { ReactSelect } from "./ReactSelect";
import { ReactSelectControllerProps } from "./types";

export const ReactSelectController = ({
  selectProps,
  controllerProps,
}: ReactSelectControllerProps) => {
  return (
    <Controller
      {...controllerProps}
      name={controllerProps.name}
      control={controllerProps.control}
      defaultValue={controllerProps.defaultValue}
      render={({ onChange, value }) => {
        return (
          <ReactSelect
            {...selectProps}
            onChange={(val) => onChange(val)}
            value={value}
            isLoading={selectProps.isLoading}
            error={selectProps.error}
          />
        );
      }}
    />
  );
};
