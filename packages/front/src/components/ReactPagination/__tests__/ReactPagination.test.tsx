import { render, screen } from "utils";
import { ReactPagination } from "../ReactPagination";

describe("👉 <ReactPagination>", () => {
  test("Snapshot ReactPagination", async () => {
    render(
      <div data-testid="pagination">
        <ReactPagination
          onChange={jest.fn}
          current={2}
          pageSize={10}
          total={130}
        />
      </div>
    );
    expect(screen.getByTestId("pagination")).toMatchSnapshot();
  });
});
