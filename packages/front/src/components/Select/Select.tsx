import { useMemo, forwardRef } from "react";
import { SelectProps } from "./types";
import { useRoundedClass } from "utils";
import clsx from "clsx";
import { getClassNames } from "./utils";
import { Typography } from "components/Typography";

export const Select = forwardRef<HTMLSelectElement, SelectProps>(
  (
    {
      className,
      inputSize = "medium",
      disabled = false,
      rounded,
      children,
      ...props
    },
    ref
  ) => {
    const classes = [
      useRoundedClass(rounded),
      ...useMemo(() => {
        return getClassNames({ disabled, inputSize });
      }, [disabled, inputSize]),
    ];

    return (
      <>
        <select ref={ref} {...props} className={clsx(classes, className)}>
          {children}
        </select>
        {props.error && (
          <Typography color="text-red" tag="span" className="text-xxs mx-2">
            {props.error}
          </Typography>
        )}
      </>
    );
  }
);
