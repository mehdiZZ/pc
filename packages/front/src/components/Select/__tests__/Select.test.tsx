import { render, screen } from "utils";
import userEvent from "@testing-library/user-event";

describe("👉 <Select>", () => {
  test("Select multiple values", () => {
    render(
      <select multiple data-testid="select-multiple">
        <option data-testid="val1" value="1">
          1
        </option>
        <option data-testid="val2" value="2">
          2
        </option>
        <option data-testid="val3" value="3">
          3
        </option>
      </select>
    );

    userEvent.selectOptions(screen.getByTestId("select-multiple"), ["1", "3"]);

    expect(
      (screen.getByTestId("val1") as HTMLOptionElement).selected
    ).toBeTruthy();
    expect(
      (screen.getByTestId("val2") as HTMLOptionElement).selected
    ).toBeFalsy();
    expect(
      (screen.getByTestId("val3") as HTMLOptionElement).selected
    ).toBeTruthy();
  });
});
