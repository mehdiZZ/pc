import { General } from "utils";

export type SelectProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["select"]
> & {
  name?: string;
  inputSize?: "small" | "medium" | "large";
  rounded?: General["rounded"];
  error?: string;
};
