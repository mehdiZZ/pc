import "react-modern-calendar-datepicker/lib/DatePicker.css";
import MyDatePicker from "react-modern-calendar-datepicker";
import { ReactDatePickerProps } from "./types";
import { useTranslation } from "react-i18next";
import { useLanguage } from "utils";

/**
 * @example
   const [date, setDate] = React.useState<DateValue>(null);
   const onChange = (date: DateValue) => {
     setDate(date);
   };
   <ReactDatePicker value={date} onChange={onChange} />
 */
export const ReactDatePicker = ({
  inputProps,
  colorPrimary = "var(--color-primary)",
  value,
  inputClassName = "date-picker focus:outline-none focus:shadow text-inverse-high text-inherit",
  wrapperClassName = "w-full",
  inputPlaceholder,
  locale,
  ...props
}: ReactDatePickerProps) => {
  const { t } = useTranslation();
  const { lang } = useLanguage();
  return (
    <div className="text-black-transparent">
      <MyDatePicker
        value={value}
        colorPrimary={colorPrimary}
        inputClassName={inputClassName}
        wrapperClassName={wrapperClassName}
        inputPlaceholder={inputPlaceholder || t("forms:selectDate")}
        locale={locale ? locale : lang === "fa" ? "fa" : "en"}
        {...props}
      />
    </div>
  );
};
