import {
  DayValue,
  DatePickerProps,
  Day,
  DayRange,
} from "react-modern-calendar-datepicker";
import { InputProps } from "components/Input/types";

export type DateValue = DayValue;

type Optional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

export type ReactDatePickerProps = Optional<
  DatePickerProps<DayValue>,
  "value"
> & {
  inputProps?: InputProps;
};
