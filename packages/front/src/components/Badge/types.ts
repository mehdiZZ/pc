import { General } from "utils";

export type BadgeProps = React.PropsWithoutRef<
  JSX.IntrinsicElements["span"]
> & {
  rounded?: General["rounded"];
  theme?: General["theme"];
};
