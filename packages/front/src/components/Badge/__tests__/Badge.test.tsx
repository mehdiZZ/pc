import { render, screen } from "utils";
import { Badge } from "../Badge";

describe("👉 <Badge>", () => {
  test("Renders children", () => {
    render(<Badge>Badge</Badge>);
    expect(screen.getByText("Badge")).toBeInTheDocument();
  });
});
