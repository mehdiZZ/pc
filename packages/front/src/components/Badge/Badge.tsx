import { useMemo } from "react";
import { BadgeProps } from "./types";
import { useRoundedClass } from "utils";
import clsx from "clsx";
import { getClassNames } from "./utils";

/**
 * @param theme - {string} badge theme
 * @param rounded - {boolean | string} set border radius
 */
export const Badge = ({
  theme = "default",
  rounded,
  className,
  ...props
}: BadgeProps) => {
  const classes = [
    useRoundedClass(rounded),
    ...useMemo(() => {
      return getClassNames(theme);
    }, [theme]),
  ];

  return (
    <span {...props} className={clsx(classes, className)}>
      {props.children}
    </span>
  );
};
