export const getClassNames = (theme?: string) => {
  const classes = ["text-xs px-1 py-px font-semibold inline-flex items-center"];
  switch (theme) {
    case "default":
      classes.push("bg-tertiary text-tertiary-inverse");
      break;
    case "primary":
      classes.push("bg-primary text-primary-inverse");
      break;
    case "secondary":
      classes.push("bg-secondary text-secondary-inverse");
      break;
    case "info":
      classes.push("bg-teal text-white");
      break;
    case "success":
      classes.push("bg-green text-white");
      break;
    case "danger":
      classes.push("bg-red text-white");
      break;
    case "warning":
      classes.push("bg-orange text-white");
      break;
  }
  return classes;
};
