import { useContext } from "react";
import { RootContext, RootProvider } from "utils";
import {
  Route,
  Redirect,
  Switch,
  RouteProps,
  HashRouter,
} from "react-router-dom";
import { AppContainer } from "containers/AppContainer";
import { ForgotPasswordMail, Login, Register } from "routes/auth";
import { ReactQueryDevtools } from "react-query-devtools";
import { ForgotPassword } from "routes/auth/ForgotPassword";
import { QueryCache, QueryClientProvider, QueryClient } from "react-query";

// react libraries css
import "simplebar/dist/simplebar.min.css";
import "rc-tooltip/assets/bootstrap.css";
import "rc-notification/assets/index.css";
import "rc-pagination/assets/index.css";

// =========> leaflet
import "leaflet/dist/leaflet.css";
import L from "leaflet";
import iconUrl from "leaflet/dist/images/marker-icon.png";
import shadowUrl from "leaflet/dist/images/marker-shadow.png";
import iconRetinaUrl from "leaflet/dist/images/marker-icon-2x.png";

// @ts-ignore
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
});

//

const PrivateRoute = (props: RouteProps) => {
  const { user } = useContext(RootContext);
  if (!user) {
    // logout user
    return <Redirect to="/login" />;
  } else {
    return <Route {...props} />;
  }
};

// =========> react query

const queryCache = new QueryCache();

const client = new QueryClient({
  queryCache,
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: false,
    },
  },
});

function App() {
  return (
    <QueryClientProvider client={client}>
      <RootProvider>
        <HashRouter>
          <Switch>
            <Route
              path="/"
              exact
              render={() => <Redirect to="/app/dashboard" />}
            />
            <Route
              path="/app"
              exact
              render={() => <Redirect to="/app/dashboard" />}
            />

            <PrivateRoute path="/app" component={AppContainer} />

            {/* //////////////////// */}
            <Route path="/register" exact render={() => <Register />} />
            <Route path="/login" exact render={() => <Login />} />
            <Route
              path="/send-forgot-mail"
              exact
              render={() => <ForgotPasswordMail />}
            />
            <Route
              path="/forgot-password/:token"
              exact
              render={() => <ForgotPassword />}
            />
            <Route path="/error" exact render={() => <h1>Error page</h1>} />
            <Route render={() => <h1>Error page</h1>} />
            {/* <Redirect from="*" to="/app/dashboard" /> */}
          </Switch>
        </HashRouter>
      </RootProvider>
      {/* <ReactQueryDevtools /> */}
    </QueryClientProvider>
  );
}

export default App;
