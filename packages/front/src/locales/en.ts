const common = {
  test: "test",
  search: "Search",
  cancel: "Cancel",
};

const routes = {
  dashboard: "Dashboard",
  app: "App",
  users: "Users",
  list: "List",
};

const forms = {
  select: "Select",
  selectDate: "Select date",
  typeHere: "Type here...",
  dragFilesHere: "Drag 'n' drop some files here, or click to select files",
};

const crud = {
  filter: "Filter",
  rows: "Rows",
  goToPage: "Go to page: ",
  currentPage: "Page <1>{{pageIndex}} of {{pageCount}}</1>",
  //create
  created: "Entity created successfully",
  //pagination
  itemsPerPage: "/ page",
  jumpTo: "Go to",
  jumpToConfirm: "confirm",
  prevPage: "Previous Page",
  nextPage: "Next Page",
  prev5: "Previous 5 Pages",
  next5: "Next 5 Pages",
  prev3: "Previous 3 Pages",
  next3: "Next 3 Pages",
};

export default {
  common,
  routes,
  forms,
  crud,
};
