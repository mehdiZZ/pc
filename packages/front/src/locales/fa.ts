const common = {
  test: "تست",
  search: "جستجو",
  cancel: "Cancel",
};

const routes = {
  dashboard: "داشبورد",
  app: "خانه",
  users: "کاربران",
  list: "لیست",
};

const forms = {
  select: "انتخاب کنید",
  selectDate: "انتخاب تاریخ",
  typeHere: "اینجا تایپ کنید...",
  dragFilesHere: "فایلها را اینجا رها کنید یا بر روی باکس کلیک کنید",
};

const crud = {
  filter: "فیلتر کنید",
  rows: "ردیف",
  goToPage: "برو به صفحه: ",
  currentPage: "صفحه <1>{{pageIndex}} از {{pageCount}}</1>",
  //create
  created: "با موفقیت ساخته شد",
  //pagination
  itemsPerPage: "/ صفحه",
  jumpTo: "برو به",
  jumpToConfirm: "تایید",
  prevPage: "صفحه قبل",
  nextPage: "صفحه بعد",
  prev5: "5 صفحه قبل",
  next5: "5 صفحه بعد",
  prev3: "3 صفحه قبل",
  next3: "3 صفحه بعد",
};

export default {
  common,
  routes,
  forms,
  crud,
};
