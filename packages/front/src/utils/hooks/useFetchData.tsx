import { useCallback, useRef, useState } from "react";

export const useFetchData = () => {
  const [state, setState] = useState({
    limit: 10,
    page: 1,
    filter: [],
    sort: [],
  });

  const fetchIdRef = useRef(0);

  const fetchData = useCallback(({ limit = 10, page = 1, sort, filter }) => {
    //
    const fetchId = ++fetchIdRef.current;
    //
    if (fetchId === fetchIdRef.current) {
      setState({
        sort: sort?.length ? sort : [],
        filter: filter?.length ? filter : [],
        page,
        limit,
      });
    }
  }, []);

  return { state, setState, fetchData };
};
