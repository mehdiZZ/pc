import { useContext } from "react";
import { RootContext } from "utils/context";
import { LanguageType } from "utils/types";

export function useLanguage(): { lang: LanguageType["lang"]; isRtl: boolean } {
  const { lang } = useContext(RootContext);
  return { lang: lang.lang, isRtl: lang.dir === "rtl" };
}
