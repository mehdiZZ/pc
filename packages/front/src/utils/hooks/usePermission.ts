import { IPermissionSlug } from "@backend/core/seed/interfaces";
import { useContext } from "react";
import { RootContext } from "utils/context";

export const usePermission = () => {
  const {
    user: { flatPermissions = [] },
  } = useContext(RootContext);

  const canUser = (permission: IPermissionSlug) => {
    return permission && flatPermissions.includes(permission);
  };

  const canUserReg = (permissionReg: string) => {
    return (
      permissionReg && flatPermissions.some((str) => str.match(permissionReg))
    );
  };

  return { canUser, canUserReg };
};
