import { useContext } from "react";
import { RootContext, ThemeType } from "utils";

export function useTheme(): ThemeType {
  return useContext(RootContext).theme["dark"];
}
