import Notification from "rc-notification";

let notification = null;
Notification.newInstance(
  {
    maxCount: 3,
    style: { top: 5, left: "50%", transform: "translateX(-50%)" },
  },
  (n) => (notification = n)
);

type NoticeType = {
  textContent?: string;
  htmlContent?: React.ReactNode;
  onClose?: () => void;
  duration?: number;
  status?: "success" | "error" | "info";
};

const notice = ({
  textContent = "",
  htmlContent = null,
  onClose = () => {},
  duration = 2,
  status = "success",
}: NoticeType) => {
  notification.notice({
    content: htmlContent ? htmlContent : textContent,
    onClose,
    duration,
    className: `status-${status}`,
    closable: true,
  });
};

export const useNotification = () => {
  return { notice };
};
