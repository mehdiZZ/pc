import { General } from "utils/types";
import { useTheme } from "./useTheme";

export function useRoundedClass(rounded?: General["rounded"]): string {
  const defaultRound = useTheme()?.rounded || "rounded";

  if (typeof rounded === "string") return rounded;

  if (typeof rounded === "boolean") return rounded ? defaultRound : "";

  return defaultRound;
}
