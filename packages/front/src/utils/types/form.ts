import {
  Control,
  FieldError,
  FieldName,
  ValidationRule,
} from "react-hook-form";

export type MyControllerProps<T> = {
  rules?: ValidationRule;
  defaultValue?: T;
  name: FieldName<Record<string, any>>;
  error?: FieldError;
  control?: Control<Record<string, any>>;
};
