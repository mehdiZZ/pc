import { TableOptions } from "react-table";

export type General = {
  theme:
    | "default"
    | "primary"
    | "secondary"
    | "danger"
    | "warning"
    | "success"
    | "info";

  rounded?:
    | boolean
    | "rounded"
    | "rounded-md"
    | "rounded-lg"
    | "rounded-xl"
    | "rounded-full";
};

// crud
export type ReactTableColumns = Pick<
  TableOptions<object>,
  "columns"
>["columns"];
