import type { GetManyRequestParams } from "@backend/common/types/front";

export type PostRequest = {
  data?: any;
  url: string;
  isPublic?: boolean;
  method?: "POST" | "PUT" | "PATCH" | "DELETE";
};

export type GetRequest = {
  url: string | string[];
  isPublic?: boolean;
};

export type GetManyResponse<T> = {
  // data: {
  data?: T[];
  count: number;
  total: number;
  page: number;
  pageCount: number;
  // };
};

export type GetManyQueryFunctionContext = {
  queryKey: [_key: string, props?: Partial<GetManyRequestParams>];
  pageParam?: any;
};

export type GetSingleQueryFunctionContext = {
  queryKey: [_key: string, props?: { [name: string]: any }];
  pageParam?: any;
};

export type { GetManyRequestParams };
