import {
  getThemeFromStorage,
  getLangFromStorage,
  getUserFromStorage,
} from "../settings";

export type ThemeType = {
  primary: string;
  primaryInverse: string;
  secondary: string;
  secondaryInverse: string;
  tertiary: string;
  tertiaryInverse: string;
  mainGradient: string;
  inverseHigher: string;
  inverseHigh: string;
  inverseMedium: string;
  rounded: string;
  content: string;
};

export type DetailedThemeType = {
  name: string;
  label: string;
  mode: "dark" | "light";
  dark: ThemeType;
  light: ThemeType;
};

export type ThemesType = DetailedThemeType[];

export type LanguageType = {
  lang: "en" | "fa" | "ar";
  dir: "rtl" | "ltr";
  icon: React.FunctionComponent<
    React.SVGProps<SVGSVGElement> & {
      title?: string | undefined;
    }
  >;
  label: string;
};

export const SET_THEME = "SET_THEME";
export const SET_USER = "SET_USER";
export const SET_SIDEBAR = "SET_SIDEBAR";
export const SET_LANG = "SET_LANG";

//
export type UserType = {
  id: string;
  username: string;
  tk: string;
  rtk: string;
  email: string;
  flatPermissions: string[];
};

export type RootStateType = {
  lang: LanguageType;
  theme: DetailedThemeType;
  user: UserType | null;
  isSidebarOpen: boolean;
};

export type RootContextType = RootStateType & {
  setTheme: (value: DetailedThemeType) => void;
  setUser: (value: UserType) => void;
  setSidebar: (value: boolean) => void;
  setLang: (value: LanguageType) => void;
};

export type ActionType =
  | { type: "SET_THEME"; value: DetailedThemeType }
  | { type: "SET_USER"; value: UserType }
  | { type: "SET_SIDEBAR"; value: boolean }
  | { type: "SET_LANG"; value: LanguageType };

export const defaults = {
  theme: getThemeFromStorage(),
  user: getUserFromStorage(),
  lang: getLangFromStorage(),
  isSidebarOpen: true,
  setUser: () => {},
  setTheme: () => {},
  setSidebar: () => {},
  setLang: () => {},
};
