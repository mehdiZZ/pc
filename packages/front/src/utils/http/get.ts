import { AxiosRequestConfig } from "axios";
import { V1_API_PREFIX } from "utils/constants";
import { getUserFromStorage } from "utils/settings";
import { GetManyRequestParams, GetRequest, UserType } from "utils";
import axiosInstance from "./axios";

export const getRequest = async <T>({ url, isPublic = false }: GetRequest) => {
  const reqConfig: AxiosRequestConfig = {};

  if (!isPublic) {
    // so we need token
    const user: UserType = getUserFromStorage();
    if (user && user.tk) {
      reqConfig["headers"] = { Authorization: `Bearer ${user.tk}` };
    }
  }

  try {
    const { data } = await axiosInstance.get<T>(`${url}`, reqConfig);
    return data;
  } catch (err) {
    throw err;
  }
};

export const getUrlWithQueryStrings = (
  key,
  { page = 0, limit = 10, filter, sort }: Partial<GetManyRequestParams>
) => {
  let url = `${key}?page=${page}&limit=${limit}`;

  //sorts
  if (sort?.length) {
    sort.forEach((s: any) => {
      url += `&sort=${s.id},${s.desc ? "DESC" : "ASC"}`;
    });
  }

  //filters
  if (filter?.length) {
    filter.forEach((f: any) => {
      url += `&filter=${f.id}||${f.value.type}||${f.value.is}`;
    });
  }

  return url;
};
