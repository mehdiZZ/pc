import axios from "axios";
import { V1_API_PREFIX } from "utils/constants";
import { getUserFromStorage, setStorageSetting } from "utils/settings";
import { UserType } from "utils/types";
//
const axiosInstance = axios.create({
  baseURL: V1_API_PREFIX,
  responseType: "json",
});

let refreshPromise = null;

async function refreshToken(user: UserType) {
  //
  let response = null;

  //
  refreshPromise =
    refreshPromise ||
    axios.post(V1_API_PREFIX + "/auth/refresh-token", {
      refreshToken: user.rtk,
    });

  try {
    response = await refreshPromise;
  } finally {
    refreshPromise = null;
  }

  return response.data;
}

//
axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const { config: originalRequest, response } = error;

    if (
      !originalRequest.url.includes("refresh-token") &&
      !originalRequest.isRetryAttempt &&
      response?.status === 401
    ) {
      try {
        const user: UserType = getUserFromStorage();

        const tokens = await refreshToken(user);

        setStorageSetting("user", {
          ...user,
          tk: tokens.accessToken,
          rtk: tokens.refreshToken,
        });

        originalRequest.headers[
          "Authorization"
        ] = `Bearer ${tokens.accessToken}`;

        return axiosInstance.request(originalRequest);
      } catch (err) {
        localStorage.removeItem("user");
        // reload sends user to login page user objet is empty in localStorage
        window.location.reload();
        throw err;
      }
    } else {
      throw error;
    }
  }
);

export default axiosInstance;
