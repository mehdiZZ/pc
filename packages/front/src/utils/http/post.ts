import { AxiosRequestConfig } from "axios";
import { getUserFromStorage } from "utils/settings";
import { PostRequest, UserType } from "utils/types";
import axiosInstance from "./axios";

export const postRequest = async <T>({
  data: requestData,
  url,
  isPublic = false,
  method = "POST",
}: PostRequest) => {
  const req: AxiosRequestConfig = {
    url: `${url}`,
    method,
    data: requestData || undefined,
  };

  if (!isPublic) {
    // we need token
    const user: UserType = getUserFromStorage();
    if (user && user.tk) {
      req["headers"] = { Authorization: `Bearer ${user.tk}` };
    }
  }

  try {
    const { data } = await axiosInstance(req);
    return data as T;
  } catch (err) {
    throw err;
  }
};

//
export const uploadRequest = async ({
  files,
  url = "/media/upload",
  isPublic = false,
  setPercentCompleted = null,
}) => {
  const formData = new FormData();
  files.forEach((file) => {
    formData.append("files", file);
  });

  const req: AxiosRequestConfig = {
    url: `${url}`,
    method: "POST",
    data: formData,
    headers: { "Content-Type": "multipart/form-data" },
    onUploadProgress: (progressEvent) => {
      setPercentCompleted &&
        setPercentCompleted(
          Math.round((progressEvent.loaded * 100) / progressEvent.total)
        );
    },
  };

  if (!isPublic) {
    // we need token
    const user: UserType = getUserFromStorage();
    if (user && user.tk) {
      req["headers"]["Authorization"] = `Bearer ${user.tk}`;
    }
  }

  try {
    const { data } = await axiosInstance(req);
    return data;
  } catch (err) {
    throw err;
  }
};
