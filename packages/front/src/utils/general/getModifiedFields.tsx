export function getModifiedFields({ fields, dirties }) {
  const newFields = {};
  for (const property in fields) {
    if (typeof dirties[property] !== "undefined") {
      newFields[property] = fields[property];
    }
  }

  return newFields;
}
