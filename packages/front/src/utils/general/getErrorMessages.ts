export const getErrorMessages = (err) =>
  err?.response?.data?.messages || ["Internal server error"];
