export const getSelectOptions = ({
  data,
  name = "name",
  id = "id",
  isMulti = false,
}) => {
  if (data?.length) {
    return data.map((row) => {
      return {
        name: row[name],
        id: row[id],
      };
    });
  }
  return isMulti ? [] : null;
};
