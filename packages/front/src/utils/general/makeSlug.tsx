import slugify from "slugify";

type Slugify = {
  string: string;
  options?: {
    replacement?: string;
    remove?: RegExp;
    lower?: boolean;
    strict?: boolean;
    locale?: string;
  };
};

export const makeSlug = (props: Slugify): string => {
  return slugify(props.string || "", {
    ...props.options,
    lower: true,
    remove: props.options?.remove || /[$*_+~.()'"!\-:@]+/g,
  });
};
