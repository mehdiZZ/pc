import { render, RenderOptions } from "@testing-library/react";
import { RootProvider } from "utils";
import { FC, ReactElement } from "react";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { en, fa } from "locales";

const AppProviders: FC = ({ children }) => {
  const lang = "en";
  const dir = "ltr";
  i18n.use(initReactI18next).init(
    {
      resources: {
        en,
        fa,
      },
      fallbackLng: "fa",
      debug: false,
      lng: lang,
      ns: ["common", "routes", "forms", "crud"],
      defaultNS: "common",
      saveMissing: false,
      keySeparator: false,
      interpolation: {
        escapeValue: false,
      },
    },
    function () {
      document.documentElement.setAttribute("lang", lang);
      document.documentElement.setAttribute("dir", dir);
      document.body.classList.add(dir);
    }
  );

  const history = createMemoryHistory();

  return (
    <RootProvider>
      <Router history={history}>{children}</Router>
    </RootProvider>
  );
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, "queries">
) => {
  return render(ui, { wrapper: AppProviders, ...options });
};

export * from "@testing-library/react";

export { customRender as render };
