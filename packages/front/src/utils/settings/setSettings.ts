import { DetailedThemeType, ThemeType } from "utils";

export const setStorageSetting = (key: string, value: any): void => {
  if (typeof value !== "string") {
    value = JSON.stringify(value);
  }

  localStorage.setItem(key, value);
};

export const setCssVariables = (theme: DetailedThemeType) => {
  const currentThemeMode: ThemeType = theme[theme.mode];

  const root = document.documentElement;
  root.style.setProperty("--color-primary", currentThemeMode.primary);
  root.style.setProperty(
    "--color-primary-inverse",
    currentThemeMode.primaryInverse
  );
  root.style.setProperty("--color-secondary", currentThemeMode.secondary);
  root.style.setProperty(
    "--color-secondary-inverse",
    currentThemeMode.secondaryInverse
  );
  root.style.setProperty("--color-tertiary", currentThemeMode.tertiary);
  root.style.setProperty(
    "--color-tertiary-inverse",
    currentThemeMode.tertiaryInverse
  );
  root.style.setProperty("--color-content", currentThemeMode.content);
  // colors based on dark/light mode
  root.style.setProperty(
    "--color-inverse-higher",
    currentThemeMode.inverseHigher
  );
  root.style.setProperty("--color-inverse-high", currentThemeMode.inverseHigh);
  root.style.setProperty(
    "--color-inverse-medium",
    currentThemeMode.inverseMedium
  );

  root.style.setProperty("--gradient-primary", currentThemeMode.mainGradient);
  root.style.setProperty("--rounded", currentThemeMode.rounded); // default border radius className
};
