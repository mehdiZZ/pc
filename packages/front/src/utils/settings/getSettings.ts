import { themes, langs } from "config";
import { DetailedThemeType, LanguageType } from "utils";
import { UserType } from "utils/types";
export const getStorageSetting = (key: string) => {
  let value = localStorage.getItem(key);
  if (!value) return null;

  try {
    const parsed = JSON.parse(value);
    return parsed;
  } catch {
    return value;
  }
};

export const getThemeFromStorage = (): DetailedThemeType => {
  const theme = getStorageSetting("theme");
  if (!theme) {
    return themes[0];
  }
  return theme;
};

export const getLangFromStorage = (): LanguageType => {
  const lang = getStorageSetting("lang");
  if (!lang) {
    return langs[0];
  }
  return lang;
};

export const getUserFromStorage = (): UserType => {
  return getStorageSetting("user");
};
