import { rootReducer } from "./reducers";

import { setCssVariables, setStorageSetting } from "utils/settings";
import {
  DetailedThemeType,
  LanguageType,
  RootContextType,
  SET_THEME,
  SET_USER,
  UserType,
  defaults,
  SET_SIDEBAR,
} from "utils";
import { createContext, FC, useReducer } from "react";

export const RootContext = createContext<RootContextType>(defaults);

export const RootProvider: FC = ({ children }) => {
  const [state, dispatch] = useReducer(rootReducer, defaults);

  const setTheme = (value: DetailedThemeType) => {
    dispatch({ type: SET_THEME, value });
    setStorageSetting("theme", value);
    setCssVariables(value);
  };

  const setUser = (value: UserType) => {
    dispatch({ type: SET_USER, value });
    setStorageSetting("user", value);
  };

  const setSidebar = (value: boolean) => {
    dispatch({ type: SET_SIDEBAR, value });
  };

  const setLang = (value: LanguageType) => {
    setStorageSetting("lang", value);
    window.location.reload();
  };

  return (
    <RootContext.Provider
      value={{
        ...state,
        setTheme,
        setUser,
        setSidebar,
        setLang,
      }}
    >
      <div className="relative h-screen">{children}</div>
    </RootContext.Provider>
  );
};
