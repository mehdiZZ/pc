import {
  DetailedThemeType,
  LanguageType,
  RootStateType,
  SET_THEME,
  SET_USER,
  SET_SIDEBAR,
  ActionType,
  UserType,
  SET_LANG,
} from "utils";

const setTheme = (value: DetailedThemeType, state: RootStateType) => {
  return {
    ...state,
    theme: value,
  };
};

const setUser = (value: UserType, state: RootStateType) => {
  return {
    ...state,
    user: value,
  };
};

const setSidebar = (value: boolean, state: RootStateType) => {
  return {
    ...state,
    isSidebarOpen: value,
  };
};

const setLang = (value: LanguageType, state: RootStateType) => {
  return {
    ...state,
    lang: value,
  };
};

export const rootReducer = (state: RootStateType, action: ActionType) => {
  switch (action.type) {
    case SET_THEME:
      return setTheme(action.value, state);
    case SET_USER:
      return setUser(action.value, state);
    case SET_SIDEBAR:
      return setSidebar(action.value, state);
    case SET_LANG:
      return setLang(action.value, state);
    default:
      return state;
  }
};
