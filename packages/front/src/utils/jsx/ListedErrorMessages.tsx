import { Alert, Box, List, ListItem } from "components";

export const ListedErrorMessages = ({
  errorMessages = [],
}: {
  errorMessages: string[];
}) => {
  return errorMessages?.length ? (
    <Box sm={"w-full"}>
      <Alert className="w-full" theme="danger" hidCloseButton>
        <List listDisc className="w-full flex-col">
          {errorMessages.map((e) => (
            <ListItem key={e}>{e}</ListItem>
          ))}
        </List>
      </Alert>
    </Box>
  ) : null;
};
