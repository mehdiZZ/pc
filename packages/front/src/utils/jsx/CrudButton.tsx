import {
  LeftOutlined,
  PlusCircleOutlined,
  RightOutlined,
} from "@ant-design/icons";
import { Button, Typography } from "components";
import { CrudButtonType } from "./types";
import { useTranslation } from "react-i18next";
import { useLanguage } from "utils/hooks";

export const CrudButton = ({ type, route, customText }: CrudButtonType) => {
  const { t } = useTranslation();

  const { isRtl } = useLanguage();

  switch (type) {
    case "create":
      return (
        <Button
          linkTo={route}
          className="absolute z-10"
          theme="primary"
          size="small"
        >
          <PlusCircleOutlined />
          <Typography tag="small" className="mx-1">
            {customText ? t(customText) : t("crud:create")}
          </Typography>
        </Button>
      );
    case "backToList":
      return (
        <Button linkTo={route} theme="primary" className="mb-2" size="small">
          {isRtl ? (
            <RightOutlined className="text-xxs" />
          ) : (
            <LeftOutlined className="text-xxs" />
          )}
          <Typography tag="small" className="mx-1">
            {customText ? t(customText) : t("crud:backToList")}
          </Typography>
        </Button>
      );
    default:
      return <>-</>;
  }
};
