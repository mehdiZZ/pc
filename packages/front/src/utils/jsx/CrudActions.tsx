import {
  DeleteOutlined,
  EditOutlined,
  Loading3QuartersOutlined,
} from "@ant-design/icons";
import { Button, ButtonGroup, Typography } from "components";
import Tooltip from "rc-tooltip";
import { getErrorMessages } from "utils";
import { Link } from "react-router-dom";
import { useNotification, usePermission } from "utils/hooks";
import { useTranslation } from "react-i18next";
import { useState } from "react";
import { CrudActionsType } from "./types";

export const CrudActions = ({
  data,
  onDelete,
  refetch,
  editColumn = "",
  pathname = "/",
  permissionPrefix,
}: CrudActionsType) => {
  const { notice } = useNotification();
  const { t } = useTranslation();

  const { canUser } = usePermission();

  const [loading, setLoading] = useState(false);
  const [tooltipVisible, setTooltip] = useState(false);

  const onDeleteClicked = async () => {
    setTooltip(false);
    setLoading(true);
    try {
      const res = await onDelete(data.id);

      if (res) {
        notice({
          textContent: t(`crud:deleted`),
        });
        refetch();
      }
    } catch (err) {
      notice({
        textContent: t(`crud:${getErrorMessages(err)[0]}`),
        status: "error",
      });
    }
    setLoading(false);
  };

  const overLay = () => (
    <div>
      <Typography tag="h6" marginBottom>
        Are you sure?
      </Typography>
      <div className="text-center">
        <ButtonGroup>
          <Button size="small" theme="danger" onClick={onDeleteClicked}>
            yes
          </Button>
          <Button onClick={() => setTooltip(false)} size="small">
            No
          </Button>
        </ButtonGroup>
      </div>
    </div>
  );

  return (
    <div>
      {editColumn &&
        (!permissionPrefix ||
          canUser((permissionPrefix + ".item-update") as any)) && (
          <Link to={`${pathname}/${data[editColumn]}`}>
            <EditOutlined className="text-teal" />
          </Link>
        )}
      {(!permissionPrefix ||
        canUser((permissionPrefix + ".item-delete") as any)) && (
        <Tooltip
          placement="top"
          overlay={overLay}
          visible={tooltipVisible}
          animation="zoom"
          trigger={"click"}
          onVisibleChange={(v) => setTooltip(v)}
        >
          <Button
            theme="transparent"
            onClick={() => setTooltip(!tooltipVisible)}
          >
            {loading ? (
              <Loading3QuartersOutlined spin />
            ) : (
              <DeleteOutlined className="text-red" />
            )}
          </Button>
        </Tooltip>
      )}
    </div>
  );
};
