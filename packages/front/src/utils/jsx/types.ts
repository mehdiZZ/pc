export type CrudButtonType = {
  type: "create" | "backToList";
  route: string;
  customText?: string;
};

export type CrudActionsType = {
  data: any;
  onDelete: (id: string) => any;
  refetch: () => void;
  editColumn?: string;
  pathname?: string;
  permissionPrefix?: string;
};
