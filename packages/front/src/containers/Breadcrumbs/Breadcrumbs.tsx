import { Route, RouteChildrenProps } from "react-router";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Breadcrumb = (props: RouteChildrenProps) => {
  const { t } = useTranslation();
  const url = props.match?.url;

  if (!url) return null;

  const linkStack: string[] = [];

  let urlArr = url.split("/").splice(1);

  const links = urlArr.map((link, idx) => {
    const url =
      linkStack.length > 0
        ? "/" + linkStack.join("/") + "/" + link
        : "/" + link;

    linkStack.push(link);

    return (
      <span className="capitalize text-inverse-medium" key={link}>
        {urlArr.length > idx + 1 ? (
          <>
            <Link
              to={url}
              className="hover:text-inverse-medium text-blue-link transition duration-200"
            >
              {t(`routes:${link}`)}
            </Link>
            <span className="inline-block mx-1">&#62;</span>
          </>
        ) : (
          <span>{t(`routes:${link}`)}</span>
        )}
      </span>
    );
  });

  return <>{links}</>;
};

export const Breadcrumbs = () => {
  return (
    <div className="mb-6 px-4">
      <Route
        path="/*"
        render={(props: RouteChildrenProps) => <Breadcrumb {...props} />}
      />
    </div>
  );
};
