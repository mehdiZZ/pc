import { FC } from "react";

export const Main: FC = ({ children }) => {
  return <div className="pt-2 xl:pt-8 px-4 xl:px-12 pb-16">{children}</div>;
};
