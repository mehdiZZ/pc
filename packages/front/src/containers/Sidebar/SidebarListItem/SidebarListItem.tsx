import { useContext, useEffect } from "react";
import { SidebarListItemType } from "./types";
import { SidebarListItemDetails } from "./SidebarListItemDetails";
import { useLocation } from "react-router";
import { motion } from "framer-motion";
import { SideBarContext } from "../Sidebar";
import { usePermission } from "utils";

/**
 *
 * @param title - {string} list item name
 * @param link - {string} page url
 * @param activeItems - {string[]} clicked items;
 * @param label - {string} goes after title inside a <sup> tag
 * @param badge - {string} for count or something like that
 * @param icon -  svg icon
 * @param childItems - {SidebarListItemType} sub menus;
 * @param isHeader - {boolean} is a top level menu
 * @param exact - {boolean}
 *
 * @example
   <SidebarListItem
    title="dashboard"
    link="/dashboard"
    icon={CheckOutlined}
    badge="8"
    label="label"
    activeItems={activeItems}
    sidebarItemsChange={sidebarItemsChange}
    childItems={[
      {
        link: "/dashboard/charts",
        title: "charts",
        activeItems,
      },
    ]}
   />
 */
export const SidebarListItem = (props: SidebarListItemType) => {
  let { link, deep = 0 } = props;
  const { activeItems, sidebarItemsChange } = useContext(SideBarContext);
  const { pathname } = useLocation();

  // set active items after refreshing the page
  useEffect(() => {
    if (
      props.childItems &&
      props.childItems.length &&
      !activeItems.length &&
      pathname &&
      pathname !== "/" &&
      pathname.indexOf(link) > -1
    ) {
      sidebarItemsChange(link);
    }
  }, []);

  if (!props.childItems || !props.childItems.length) {
    return <SidebarListItemDetails {...props} deep={deep} />;
  } else {
    let isOpen = activeItems.includes(link);

    return (
      <>
        <SidebarListItemDetails
          {...props}
          isHeader
          isOpen={isOpen}
          deep={deep}
        />
        <motion.div
          className="overflow-hidden"
          key={props.link}
          initial="collapsed"
          animate={isOpen ? "open" : "collapsed"}
          variants={{
            open: { opacity: 1, height: "auto" },
            collapsed: { opacity: 0, height: 0 },
          }}
          transition={{ duration: 0.3, ease: "easeInOut" }}
        >
          {props.childItems.map((item) => (
            <SidebarListItem {...item} key={item.link} deep={deep + 1} />
          ))}
        </motion.div>
      </>
    );
  }
};
