import { IPermissionSlug } from "@backend/core/seed/interfaces";

export type SidebarListItemType = {
  title: string;
  link: string;
  label?: string;
  badge?: string;
  icon?: any;
  childItems?: SidebarListItemType[];
  isHeader?: boolean;
  exact?: boolean;
  isOpen?: boolean;
  deep?: number;
  permission?: IPermissionSlug;
  permissionRegex?: string;
};
