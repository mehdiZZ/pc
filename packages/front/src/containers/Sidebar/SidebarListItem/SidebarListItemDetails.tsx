import { SidebarListItemType } from "./types";
import { NavLink } from "react-router-dom";
import clsx from "clsx";
import { Badge } from "components";
import { motion } from "framer-motion";
import { useLanguage, usePermission } from "utils";
import { useTranslation } from "react-i18next";
import { SideBarContext } from "../Sidebar";
import { useContext } from "react";

export const SidebarListItemDetails = ({
  title = "",
  link = "",
  label = "",
  badge = "",
  icon,
  isHeader = false,
  exact = true,
  isOpen = false,
  deep = 0,
  permission,
  permissionRegex,
}: SidebarListItemType) => {
  const { isRtl } = useLanguage();
  const { t } = useTranslation();
  const { sidebarItemsChange } = useContext(SideBarContext);
  //
  const { canUserReg, canUser } = usePermission();
  if (permission && !canUser(permission)) return null;
  if (permissionRegex && !canUserReg(permissionRegex)) return null;
  //

  const linkClasses = [
    "flex",
    "px-3",
    "align-center",
    "text-inverse-higher",
    "hover:bg-white-transparent",
    "rounded",
    "flex",
    "items-center",
  ];

  if (deep > 0) {
    linkClasses.push("py-2");
  } else {
    linkClasses.push("py-3");
  }

  const nestedListPadding: "paddingRight" | "paddingLeft" = isRtl
    ? "paddingRight"
    : "paddingLeft";

  const Icon = icon;
  const rest = (
    <>
      {Icon && <Icon className={clsx(isRtl ? "ml-2" : "mr-2")} />}
      {title && (
        <span className={clsx(isOpen && "active-sidebar-link")}>
          {t(`routes:${title}`)}
        </span>
      )}
      {label && <sup className="text-red inline-flex mx-1">{t(label)}</sup>}
      {badge && (
        <span className="flex flex-grow-max justify-end">
          <Badge theme="secondary">{badge}</Badge>
        </span>
      )}
    </>
  );
  if (isHeader) {
    return (
      <li>
        <a
          href={link}
          onClick={(e) => {
            e.preventDefault();
            sidebarItemsChange(link);
          }}
          className={clsx(linkClasses)}
          style={{ paddingLeft: ` ${0.75 + 1 * deep}rem` }}
        >
          {rest}
          <span
            className={clsx(
              "flex flex-grow justify-end text-xxs",
              isRtl ? "mr-3" : "ml-3"
            )}
          >
            <motion.i
              key={link}
              initial="init"
              animate={isOpen ? "rotate" : "init"}
              variants={{
                rotate: { rotate: -90 },
                init: { rotate: 0 },
              }}
              transition={{ duration: 0.2, ease: "easeInOut" }}
              className="icon icon-chevron-left"
            />
          </span>
        </a>
      </li>
    );
  } else {
    return (
      <li>
        <NavLink
          to={"/app" + link}
          activeClassName={"active-sidebar-link"}
          exact={exact}
          style={{
            [nestedListPadding]: ` ${0.75 + 1 * deep}rem`,
          }}
          className={clsx(linkClasses)}
        >
          {rest}
        </NavLink>
      </li>
    );
  }
};
