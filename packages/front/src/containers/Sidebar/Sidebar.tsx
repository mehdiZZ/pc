import { useEffect, useState, useContext, createContext } from "react";
import SimpleBar from "simplebar-react";
import { Link } from "react-router-dom";
import { SidebarListItem } from "./SidebarListItem";
import { motion } from "framer-motion";
import { RootContext, useLanguage, usePermission } from "utils";
import clsx from "clsx";
import {
  ApiOutlined,
  CommentOutlined,
  FileJpgOutlined,
  FileSearchOutlined,
  FormOutlined,
  HomeOutlined,
  MenuOutlined,
  OrderedListOutlined,
  PlusOutlined,
  ProfileOutlined,
  SafetyCertificateOutlined,
  SettingOutlined,
  SnippetsOutlined,
  TagsOutlined,
  UnlockOutlined,
  UserAddOutlined,
  UserOutlined,
} from "@ant-design/icons";

const Path = (props: any) => (
  <motion.path
    fill="transparent"
    strokeWidth="3"
    stroke="var(--color-inverse-high)"
    strokeLinecap="round"
    {...props}
  />
);

export const SideBarContext = createContext({
  activeItems: [],
  sidebarItemsChange: (link: string) => {},
});

export const Sidebar = () => {
  const [activeItems, setActiveMenu] = useState<string[]>([]);
  const [smallScreen, setScreen] = useState(false);

  const { isSidebarOpen, setSidebar } = useContext(RootContext);
  const { isRtl } = useLanguage();

  const checkSize = () => {
    setScreen(window.innerWidth < 1024);
  };

  window.addEventListener("resize", () => {
    checkSize();
  });

  useEffect(() => {
    checkSize();
    // set default status for sidebar
    setSidebar(smallScreen ? false : true);
  }, [smallScreen]);

  const sidebarItemsChange = (link: string) => {
    if (activeItems.includes(link)) {
      setActiveMenu(activeItems.filter((item) => item !== link));
    } else {
      setActiveMenu((prevItems) => [...prevItems, link]);
    }
  };

  return (
    <motion.div
      animate={!isSidebarOpen ? "closed" : "open"}
      initial={"open"}
      variants={{
        closed: { x: isRtl ? "100%" : "-100%" },
        open: { x: 0 },
      }}
      className={clsx(
        "sidebar h-screen fixed inset-y-0 lg:bg-transparent bg-content z-40",
        isRtl ? "right-0 pr-4" : "left-0 pl-4"
      )}
    >
      <button
        onClick={() => {
          setSidebar(!isSidebarOpen);
        }}
        className={clsx(
          "absolute top-1 z-10 mt-1 focus:outline-none",
          isRtl ? "left-0 -ml-8" : "right-0 -mr-8"
        )}
      >
        <svg width="23" height="23" viewBox="0 0 23 23">
          <Path
            animate={!isSidebarOpen ? "closed" : "open"}
            initial={"open"}
            variants={{
              closed: { d: "M 2 2.5 L 20 2.5" },
              open: { d: "M 3 16.5 L 17 2.5" },
            }}
          />
          <Path
            d="M 2 9.423 L 20 9.423"
            animate={!isSidebarOpen ? "closed" : "open"}
            initial={"open"}
            variants={{
              closed: { opacity: 1 },
              open: { opacity: 0 },
            }}
            transition={{ duration: 0.1 }}
          />
          <Path
            animate={!isSidebarOpen ? "closed" : "open"}
            initial={"open"}
            variants={{
              closed: { d: "M 2 16.346 L 20 16.346" },
              open: { d: "M 3 2.5 L 17 16.346" },
            }}
          />
        </svg>
      </button>
      <SimpleBar className="max-h-screen">
        <header className="mt-8 mb-12 text-center">
          <Link to="/">
            <h1 className="text-xl">
              <span className="text-orange">Puzzle</span> Cms
            </h1>
          </Link>
        </header>
        <SideBarContext.Provider value={{ activeItems, sidebarItemsChange }}>
          <nav className="h-full">
            <ul>
              <SidebarListItem
                title="Dashboard"
                link="/dashboard"
                badge={"99"}
                label="label"
                icon={HomeOutlined}
              />
              <SidebarListItem
                title="Users"
                link="/users"
                icon={UserOutlined}
                // permissionRegex={"user."}
                childItems={[
                  {
                    link: "/users/profile",
                    title: "Profile",
                    icon: ProfileOutlined,
                  },
                  {
                    link: "/users",
                    title: "List",
                    permission: "user.list-read",
                    icon: OrderedListOutlined,
                  },
                  {
                    link: "/users/create",
                    title: "Create",
                    permission: "user.item-create",
                    icon: UserAddOutlined,
                  },
                  {
                    link: "/users/permissions",
                    title: "Permissions",
                    permission: "permission.list-read",
                    icon: SafetyCertificateOutlined,
                  },
                  {
                    link: "/users/roles",
                    title: "Roles",
                    permission: "role.list-read",
                    icon: UnlockOutlined,
                  },
                ]}
              />
              <SidebarListItem
                title="Media"
                link="/media"
                icon={FileJpgOutlined}
                permission={"media.list-read"}
                childItems={[
                  {
                    link: "/media",
                    title: "List",
                    icon: FileSearchOutlined,
                  },
                ]}
              />
              <SidebarListItem
                title="Pages"
                link="/page"
                icon={SnippetsOutlined}
                permissionRegex={"page."}
                childItems={[
                  {
                    link: "/pages",
                    title: "List",
                    permission: "page.list-read",
                    icon: OrderedListOutlined,
                  },
                  {
                    link: "/pages/create",
                    title: "Create",
                    permission: "page.item-create",
                    icon: PlusOutlined,
                  },
                ]}
              />
              <SidebarListItem
                title="Posts"
                link="/post"
                icon={FormOutlined}
                permissionRegex={"post."}
                childItems={[
                  {
                    link: "/posts",
                    title: "List",
                    permission: "post.list-read",
                    icon: OrderedListOutlined,
                  },
                  {
                    link: "/posts/create",
                    title: "Create",
                    permission: "post.item-create",
                    icon: PlusOutlined,
                  },
                  {
                    link: "/posts/tags",
                    title: "Tags",
                    permission: "tag.list-read",
                    icon: TagsOutlined,
                  },
                  {
                    link: "/posts/categories",
                    title: "Categories",
                    permission: "category.list-read",
                    icon: ApiOutlined,
                  },
                  {
                    link: "/posts/comments",
                    title: "Comments",
                    permission: "comment.list-read",
                    icon: CommentOutlined,
                  },
                ]}
              />
              <SidebarListItem
                title="Settings"
                link="/settings"
                icon={SettingOutlined}
                permissionRegex={"setting."}
                childItems={[
                  {
                    link: "/settings/general",
                    title: "General",
                    permission: "setting.item-read",
                    icon: OrderedListOutlined,
                  },
                  {
                    link: "/settings/menu",
                    title: "Menu",
                    permission: "setting.item-read",
                    icon: MenuOutlined,
                  },
                ]}
              />
            </ul>
          </nav>
        </SideBarContext.Provider>
      </SimpleBar>
    </motion.div>
  );
};
