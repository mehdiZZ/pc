import { useContext, useState } from "react";
import {
  List,
  ListItem,
  Img,
  Typography,
  InputGroup,
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownContent,
  Card,
  CardBody,
  Switch,
  // Switch,
} from "components";
import { themes, langs } from "config";
import profile from "../../assets/images/user.png";
import sun from "../../assets/images/sun.png";
import night from "../../assets/images/night.png";
import { RootContext, useLanguage } from "utils";
import clsx from "clsx";
import { useTranslation } from "react-i18next";
import { postLogout } from "./utils";
import {
  GlobalOutlined,
  LogoutOutlined,
  NotificationOutlined,
  SearchOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { useMutation } from "react-query";

export const Header = () => {
  const { mutateAsync } = useMutation(postLogout);

  const { t } = useTranslation();
  const { isRtl } = useLanguage();
  const { theme, lang, setTheme, setLang, setUser, user } = useContext(
    RootContext
  );
  const [isModeDark, setMode] = useState<boolean>(theme.mode === "light");
  return (
    <nav className="xl:h-16 py-2 xl:py-4 px-4 xl:px-12 flex justify-end items-center">
      <List
        inline
        className="items-center flex-wrap flex-row-reverse text-inverse-higher"
      >
        {/* USER */}
        <ListItem className="flex items-center">
          <Dropdown>
            <DropdownToggle size="small">
              <Typography tag="small" className={clsx(isRtl ? "ml-2" : "mr-2")}>
                {user.username || user.email}
              </Typography>
              <Img
                src={profile}
                width="35"
                height="35"
                className="rounded-full shadow-sm"
              />
            </DropdownToggle>
            <DropdownContent>
              <Card>
                <CardBody className="text-center">
                  <Button
                    onClick={() => {
                      mutateAsync({ id: user.id });
                      setUser(null);
                    }}
                  >
                    <LogoutOutlined rotate={-90} />
                  </Button>
                </CardBody>
              </Card>
            </DropdownContent>
          </Dropdown>
        </ListItem>
        <ListItem className="mx-6">|</ListItem>
        {/* NOTIFICATIONS */}
        <ListItem className="flex items-center">
          <Dropdown>
            <DropdownToggle>
              <NotificationOutlined />
            </DropdownToggle>
            <DropdownContent>
              <Card>
                <CardBody>Hello</CardBody>
              </Card>
            </DropdownContent>
          </Dropdown>
        </ListItem>
        {/* LANG */}
        <ListItem className="flex items-center ">
          <Dropdown>
            <DropdownToggle>
              <GlobalOutlined />
            </DropdownToggle>
            <DropdownContent>
              <Card>
                <CardBody>
                  {langs.map((lng) => {
                    return (
                      <Button
                        className={clsx(lang.lang !== lng.lang && "opacity-25")}
                        onClick={() => setLang(lng)}
                        theme="transparent"
                        key={lng.lang}
                      >
                        {<lng.icon title={lng.label} width="20px" />}
                      </Button>
                    );
                  })}
                </CardBody>
              </Card>
            </DropdownContent>
          </Dropdown>
        </ListItem>
        {/* SETTINGS */}
        <ListItem className="flex items-center">
          <Dropdown>
            <DropdownToggle>
              <SettingOutlined />
            </DropdownToggle>
            <DropdownContent>
              <Card className="w-40">
                <CardBody className="flex items-center flex-col">
                  <Switch
                    onBg={`url(${sun}) 95% center/40% no-repeat var(--color-primary)`}
                    offBg={`url(${night}) 5% center/40% no-repeat var(--color-tertiary)`}
                    onChange={() => {
                      setTheme({
                        ...theme,
                        mode: isModeDark ? "dark" : "light",
                      });
                      setMode(!isModeDark);
                    }}
                    defaultChecked={isModeDark}
                  />
                  <div className="w-full bg-gray-300 mt-8 rounded shadow p-2 flex justify-center">
                    {themes.map((thm) => {
                      return (
                        <div
                          onClick={() => {
                            setTheme({ ...thm, mode: theme.mode }); // making sure to not broke the theme mode
                          }}
                          key={thm.name}
                          style={{ background: thm["label"] }}
                          className={clsx(
                            "w-6 h-6 rounded cursor-pointer shadow-sm mx-1 border-2",
                            thm.name === theme.name && "border-red"
                          )}
                          title={thm.name}
                        ></div>
                      );
                    })}
                  </div>
                </CardBody>
              </Card>
            </DropdownContent>
          </Dropdown>
        </ListItem>
        <ListItem className="mx-6">|</ListItem>
        {/* SEARCH */}
        <ListItem
          className={clsx(
            "flex items-center cursor-pointer",
            isRtl ? "ml-2" : "mr-2"
          )}
        >
          <InputGroup>
            <Button theme="primary">
              <SearchOutlined />
            </Button>
            <Input name="name" inputSize="small" placeholder={t("search")} />
          </InputGroup>
        </ListItem>
      </List>
    </nav>
  );
};
