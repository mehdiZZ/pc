import axios from "axios";
import { V1_API_PREFIX } from "utils";
import { AuthLogoutUserReq } from "@backend/core/user/_auth/dto";

export const postLogout = async (data: AuthLogoutUserReq) => {
  return await axios({
    url: `${V1_API_PREFIX}/auth/logout`,
    method: "POST",
    data,
  });
};
