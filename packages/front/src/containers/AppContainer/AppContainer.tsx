import { useEffect, useContext, useState, FC } from "react";
import { Switch, Route } from "react-router-dom";
import { Dashboard, UserRoutes } from "routes";
import { Sidebar, Main, Header } from "containers";
import { Breadcrumbs } from "containers/Breadcrumbs";
import { motion } from "framer-motion";
import { RootContext, setStorageSetting, useLanguage } from "utils";
import { MediaRoutes } from "routes/media";
import { PageRoutes } from "routes/content/Page";
import { PostRoutes } from "routes/content/Post";
import { SettingsRoutes } from "routes/settings";
import { useQuery } from "react-query";
import { getUser } from "routes/user/utils";

export const AppContainer: FC = () => {
  const { isSidebarOpen, user, setUser } = useContext(RootContext);
  const { isRtl } = useLanguage();
  const [smallScreen, setScreen] = useState(false);

  const checkSize = () => {
    setScreen(window.innerWidth < 1024);
  };

  useEffect(() => {
    checkSize();
  }, []);

  window.addEventListener("resize", () => {
    checkSize();
  });

  const { error, data } = useQuery([`users/`, { id: user.id }], getUser);
  // refresh user data after reload page
  useEffect(() => {
    if (!error && data) {
      setUser({
        ...user,
        flatPermissions: data.flatPermissions,
        username: data.username,
        email: data.email,
      });
    }
  }, [data]);

  return (
    <motion.div
      animate={!isSidebarOpen || smallScreen ? "closed" : "open"}
      initial={"closed"}
      variants={{
        open: { [isRtl ? "marginRight" : "marginLeft"]: "240px" },
        close: { [isRtl ? "marginRight" : "marginLeft"]: 0 },
      }}
      className="app-container"
    >
      <Header />
      <Sidebar />
      <Main>
        <Breadcrumbs />
        <Switch>
          <Route path="/app/dashboard" component={Dashboard} />
          <Route path="/app/user(s?)" component={UserRoutes} />
          <Route path="/app/media" component={MediaRoutes} />
          <Route path="/app/page(s?)" component={PageRoutes} />
          <Route path="/app/post(s?)" component={PostRoutes} />
          <Route path="/app/settings" component={SettingsRoutes} />
        </Switch>
      </Main>
    </motion.div>
  );
};
