import {
  User,
  UserCreateReq,
  UserUpdateReq,
  UserProfileUpdateReq,
} from "./types";
import * as yup from "yup";
import { Cell, FilterProps } from "react-table";
import {
  CrudActions,
  GetManyResponse,
  getRequest,
  getUrlWithQueryStrings,
  GetManyQueryFunctionContext,
  postRequest,
  ReactTableColumns,
  GetSingleQueryFunctionContext,
} from "utils";
import { BooleanFilterColumn } from "components/ReactTable/components";

/////////////////-------- Validation
export const createUserSchema = yup.object().shape({
  username: yup.string().required(),
  email: yup.string().email().required(),
  roles: yup
    .array()
    .of(yup.object().shape({ id: yup.string().required() }))
    .min(1),
  password: yup
    .string()
    .required()
    .min(8)
    .matches(
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      "password must contain alpha and numeric characters like: MyPassword123456"
    ),
  confirmPassword: yup
    .string()
    .required()
    .test("confirmPassword", "please confirm your password", function (val) {
      return this.parent.password === val;
    }),
  picture: yup.array().of(
    yup
      .object()
      .shape({
        id: yup.string(),
      })
      .nullable()
  ),
});

export const updateUserSchema = yup.object().shape({
  username: yup.string().required(),
  email: yup.string().email().required(),
  roles: yup
    .array()
    .of(yup.object().shape({ id: yup.string().required() }))
    .min(1),
  password: yup
    .string()
    .transform((value) => (!value ? undefined : value))
    .notRequired()
    .min(8)
    .matches(
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      "password must contain alpha and numeric characters like: MyPassword123456"
    ),

  confirmPassword: yup
    .string()
    .test("confirmPassword", "please confirm your password", function (val) {
      return this.parent.password ? this.parent.password === val : true;
    }),
  picture: yup.array().of(
    yup
      .object()
      .shape({
        id: yup.string(),
      })
      .nullable()
  ),
});

export const updateProfileSchema = yup.object().shape({
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup
    .string()
    .transform((value) => (!value ? undefined : value))
    .notRequired()
    .min(8)
    .matches(
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      "password must contain alpha and numeric characters like: MyPassword123456"
    ),

  confirmPassword: yup
    .string()
    .test("confirmPassword", "please confirm your password", function (val) {
      return this.parent.password ? this.parent.password === val : true;
    }),
  picture: yup.array().of(
    yup
      .object()
      .shape({
        id: yup.string(),
      })
      .nullable()
  ),
});

/////////////////-------- HTTP
export const getUsers = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<User>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getUser = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { username, id }] = queryKey;
  return getRequest<User>({
    url: _key + (username ? username : id),
  });
};

export const createUser = async (data: UserCreateReq) => {
  return await postRequest({ data, url: "/users" });
};

export const updateUser = async (data: UserUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/users/${id}`,
    method: "PATCH",
  });
};

export const updateProfile = async (data: UserProfileUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/users/profile/${id}`,
    method: "PATCH",
  });
};

export const deleteUser = async (id: string) => {
  return await postRequest({ url: `/users/${id}`, method: "DELETE" });
};

/////////////////-------- table columns
export const tableColumns: ({ onDelete, refetch }) => ReactTableColumns = ({
  onDelete,
  refetch,
}) => [
  {
    Header: "email",
    accessor: "username",
  },
  {
    Header: "active",
    accessor: "isActive",
    Cell: (props) => {
      return props.value ? "✔" : "❌";
    },
    Filter: (props: FilterProps<object>) => <BooleanFilterColumn {...props} />,
    filter: "equals",
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: Cell) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        editColumn={"username"}
        pathname={"/app/user"}
        permissionPrefix={"user"}
      />
    ),
  },
];
