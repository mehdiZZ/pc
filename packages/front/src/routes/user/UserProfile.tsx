import { yupResolver } from "@hookform/resolvers/yup";
import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactSelectController,
  SwitchController,
} from "components";
import { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getModifiedFields,
  getErrorMessages,
  RootContext,
  usePermission,
} from "utils";
import { getUser, updateProfile, updateProfileSchema } from "./utils";
import { MediaController } from "../media";

export const UserProfile = () => {
  const {
    user: { id },
  } = useContext(RootContext);
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();
  //
  const { mutateAsync, isLoading: updateLoading } = useMutation(updateProfile);

  //
  const { isLoading, error, data } = useQuery([`users/`, { id }], getUser);

  //
  const { errors, handleSubmit, register, control, formState, reset } = useForm(
    {
      resolver: yupResolver(updateProfileSchema),
    }
  );

  const { dirtyFields } = formState;

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (userFields) => {
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      const updatedData = await mutateAsync({
        ...getModifiedFields({
          fields: userFields,
          dirties: dirtyFields,
        }),
        id: data.id,
      });

      if (updatedData) {
        setErrors([]);
        reset(updatedData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isLoading || updateLoading}>
      <CardBody>
        {canUser("user.list-read") && (
          <div className="flex justify-end">
            <CrudButton type="backToList" route={"../users"} />
          </div>
        )}
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            {data && (
              <Form
                onSubmit={handleSubmit(onSubmit, (invalid) => {
                  console.log(invalid);
                })}
              >
                <Flex>
                  {!isLoading && (
                    <ListedErrorMessages errorMessages={errorMessages} />
                  )}
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="email">Email</Label>
                    <Input
                      id="email"
                      name="email"
                      type="email"
                      placeholder="email@domain.com"
                      ref={register}
                      defaultValue={data?.email}
                      error={errors.email?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="username">Username</Label>
                    <Input
                      id="username"
                      name="username"
                      placeholder="test-username"
                      ref={register}
                      defaultValue={data?.username}
                      error={errors.username?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="password">Password</Label>
                    <Input
                      id="password"
                      name="password"
                      type="password"
                      placeholder="Test@1234"
                      ref={register}
                      error={errors.password?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="confirmPassword">Confirm password</Label>
                    <Input
                      id="confirmPassword"
                      name="confirmPassword"
                      type="password"
                      placeholder="Test@1234"
                      ref={register}
                      error={errors.confirmPassword?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for={"picture"}>Picture</Label>
                    <MediaController
                      controllerProps={{
                        name: "picture",
                        control,
                        defaultValue: data?.picture,
                      }}
                      fileProps={{
                        justOneFile: true,
                      }}
                    />
                  </Box>
                  <Box
                    md="w-3/6"
                    lg="w-1/6"
                    className="flex items-end justify-end"
                  ></Box>
                </Flex>
                <Button theme="success">Submit</Button>
              </Form>
            )}
          </>
        )}
      </CardBody>
    </Card>
  );
};
