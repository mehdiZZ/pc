import { useMemo } from "react";
import { Card, CardBody, ReactTable } from "components";
import { useQuery } from "react-query";
import { getUsers, tableColumns, deleteUser } from "./utils";
import { CrudButton, useFetchData, usePermission } from "utils";

export const UserList = () => {
  const { canUser } = usePermission();
  const { state, fetchData } = useFetchData();

  //
  const { data, isFetching, refetch } = useQuery(
    [
      "users",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getUsers
  );

  const columns = useMemo(
    () => tableColumns({ refetch, onDelete: deleteUser }),
    []
  );

  return (
    <Card>
      <CardBody>
        {canUser("user.item-create") && (
          <div className="flex justify-end">
            <CrudButton type="create" route={"./users/create"} />
          </div>
        )}
        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
        />
      </CardBody>
    </Card>
  );
};
