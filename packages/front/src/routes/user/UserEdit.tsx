import { yupResolver } from "@hookform/resolvers/yup";
import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactSelectController,
  SwitchController,
} from "components";
import { useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getModifiedFields,
  getErrorMessages,
  usePermission,
} from "utils";
import { getRoles } from "./Role/utils";
import { updateUserSchema, updateUser, getUser } from "./utils";
import { MediaController } from "../media";
import { useState } from "react";

export const UserEdit = () => {
  const { canUser } = usePermission();
  const { username }: any = useParams();
  const { notice } = useNotification();
  const { t } = useTranslation();

  //
  const { data: roles, isLoading: getRolesLoading } = useQuery(
    ["roles", { limit: 200 }],
    getRoles
  );

  //
  const { mutateAsync, isLoading: updateLoading } = useMutation(updateUser);

  //
  const { isLoading, error, data } = useQuery(
    [`users/username/`, { username }],
    getUser
  );

  //
  const { errors, handleSubmit, register, control, formState, reset } = useForm(
    {
      resolver: yupResolver(updateUserSchema),
    }
  );

  const { dirtyFields } = formState;

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (userFields) => {
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      const updatedData = await mutateAsync({
        ...getModifiedFields({
          fields: userFields,
          dirties: dirtyFields,
        }),
        id: data.id,
      });

      if (updatedData) {
        setErrors([]);
        reset(updatedData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isLoading || updateLoading}>
      <CardBody>
        <div className="flex justify-end">
          <CrudButton type="backToList" route={"../users"} />
        </div>
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            {data && (
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Flex>
                  {!isLoading && (
                    <ListedErrorMessages errorMessages={errorMessages} />
                  )}
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="username">Username</Label>
                    <Input
                      id="username"
                      name="username"
                      placeholder="test-username"
                      ref={register}
                      defaultValue={data?.username}
                      error={errors.username?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="email">Email</Label>
                    <Input
                      id="email"
                      name="email"
                      type="email"
                      placeholder="email@domain.com"
                      ref={register}
                      defaultValue={data?.email}
                      error={errors.email?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="password">Password</Label>
                    <Input
                      id="password"
                      name="password"
                      type="password"
                      placeholder="Test@1234"
                      ref={register}
                      error={errors.password?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="confirmPassword">Confirm password</Label>
                    <Input
                      id="confirmPassword"
                      name="confirmPassword"
                      type="password"
                      placeholder="Test@1234"
                      ref={register}
                      error={errors.confirmPassword?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="roles">Roles</Label>
                    <ReactSelectController
                      controllerProps={{
                        control,
                        name: "roles",
                        defaultValue: data?.roles,
                      }}
                      selectProps={{
                        error: errors.roles?.message,
                        options: roles?.data,
                        isMulti: true,
                        isLoading: getRolesLoading,
                      }}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for={"picture"}>Picture</Label>
                    <MediaController
                      controllerProps={{
                        name: "picture",
                        control,
                        defaultValue: data?.picture,
                      }}
                      fileProps={{
                        justOneFile: true,
                      }}
                    />
                  </Box>
                  <Box md="w-3/6" lg="w-1/6">
                    <Label className="w-full" for="isActive">
                      Active
                    </Label>
                    <SwitchController
                      controllerProps={{
                        name: "isActive",
                        control,
                        defaultValue: data?.isActive,
                      }}
                      switchProps={{ error: errors.isActive?.message }}
                    />
                  </Box>
                  <Box
                    md="w-3/6"
                    lg="w-1/6"
                    className="flex items-end justify-end"
                  ></Box>
                </Flex>
                <Button theme="success" disabled={!canUser("user.item-update")}>
                  Submit
                </Button>
              </Form>
            )}
          </>
        )}
      </CardBody>
    </Card>
  );
};
