import { Redirect, Route, Switch } from "react-router-dom";
import { PermissionCreate, PermissionList, PermissionEdit } from "./Permission";
import { UserProfile } from "./UserProfile";
import { RoleList, RoleCreate, RoleEdit } from "./Role";
import { UserCreate } from "./UserCreate";
import { UserEdit } from "./UserEdit";
import { UserList } from "./UserList";

export const UserRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={"/app/user"}
        render={() => <Redirect to="/app/users" />}
      />
      {/* UserProfile */}
      <Route exact path={"/app/users/profile"} component={UserProfile} />
      {/* User */}
      <Route exact path={"/app/users"} component={UserList} />
      <Route exact path={"/app/users/create"} component={UserCreate} />
      <Route exact path={"/app/user/:username"} component={UserEdit} />
      {/* permissions */}
      <Route
        exact
        path={"/app/users/permission"}
        render={() => <Redirect to="/app/users/permissions" />}
      />
      <Route exact path={"/app/users/permissions"} component={PermissionList} />
      <Route
        exact
        path={"/app/users/permission/:slug"}
        component={PermissionEdit}
      />
      <Route
        exact
        path={"/app/users/permissions/create"}
        component={PermissionCreate}
      />
      {/* roles */}
      <Route
        exact
        path={"/app/users/role"}
        render={() => <Redirect to="/app/users/roles" />}
      />
      <Route exact path={"/app/users/roles"} component={RoleList} />
      <Route
        exact
        path={"/app/users/role/:slug"}
        component={() => <RoleEdit />}
      />
      <Route exact path={"/app/users/roles/create"} component={RoleCreate} />

      <Route path={"*"} render={() => <>Not found</>} />
    </Switch>
  );
};
