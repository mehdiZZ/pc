import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactSelectController,
  SwitchController,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import { MediaController } from "routes/media";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getErrorMessages,
  usePermission,
} from "utils";
import { getRoles } from "./Role/utils";
import { createUserSchema, createUser } from "./utils";
import { UserCreateReq } from "./types";

export const UserCreate = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();

  //
  const { data, isLoading: getRolesLoading } = useQuery(
    ["roles", { limit: 200 }],
    getRoles
  );

  //
  const { errors, handleSubmit, register, control } = useForm({
    resolver: yupResolver(createUserSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(createUser);

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (userFields: UserCreateReq) => {
    try {
      const data = await mutateAsync(userFields);
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isLoading}>
      <CardBody>
        {canUser("user.list-read") && (
          <div className="flex justify-end">
            <CrudButton type="backToList" route={"../users"} />
          </div>
        )}
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Flex>
            {!isLoading && (
              <ListedErrorMessages errorMessages={errorMessages} />
            )}
            <Box md="w-4/6" lg="w-2/6">
              <Label for="username">Username</Label>
              <Input
                id="username"
                name="username"
                placeholder="test-username"
                ref={register}
                error={errors.username?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="email">Email</Label>
              <Input
                id="email"
                name="email"
                type="email"
                placeholder="email@domain.com"
                ref={register}
                error={errors.email?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="password">Password</Label>
              <Input
                id="password"
                name="password"
                type="password"
                placeholder="Test@1234"
                ref={register}
                error={errors.password?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="password">Confirm password</Label>
              <Input
                name="confirmPassword"
                type="password"
                placeholder="Test@1234"
                ref={register}
                error={errors.confirmPassword?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="roles">Roles</Label>

              <ReactSelectController
                controllerProps={{
                  control,
                  name: "roles",
                  defaultValue: [],
                }}
                selectProps={{
                  error: errors.roles?.message,
                  options: data?.data,
                  isMulti: true,
                  isLoading: getRolesLoading,
                }}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for={"picture"}>Picture</Label>
              <MediaController
                controllerProps={{
                  name: "picture",
                  control,
                }}
                fileProps={{
                  justOneFile: true,
                }}
              />
            </Box>
            <Box md="w-3/6" lg="w-1/6">
              <Label className="w-full" for="isActive">
                Active
              </Label>

              <SwitchController
                controllerProps={{
                  name: "isActive",
                  control,
                  defaultValue: false,
                }}
                switchProps={{ error: errors.isActive?.message }}
              />
            </Box>
          </Flex>
          <Button theme="success">Submit</Button>
        </Form>
      </CardBody>
    </Card>
  );
};
