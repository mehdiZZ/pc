import * as yup from "yup";

import {
  CrudActions,
  GetManyQueryFunctionContext,
  GetManyResponse,
  getRequest,
  GetSingleQueryFunctionContext,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
} from "utils";

import { Permission, PermissionCreateReq, PermissionUpdateReq } from "./types";

/////////////////-------- Validation
export const createPermissionSchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
});

export const updatePermissionSchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
});

/////////////////-------- HTTP
export const getPermissions = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<Permission>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getPermission = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { slug }] = queryKey;
  return getRequest<Permission>({
    url: _key + slug,
  });
};

export const createPermission = async (data: PermissionCreateReq) => {
  return await postRequest({ data, url: "/permissions" });
};

export const updatePermission = async (data: PermissionUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/permissions/${id}`,
    method: "PATCH",
  });
};

export const deletePermission = async (id: string) => {
  return await postRequest({ url: `/permissions/${id}`, method: "DELETE" });
};

/////////////////-------- table columns
export const tableColumns: ({
  onDelete,
  refetch,
}: {
  onDelete: (id: string) => any;
  refetch: () => void;
}) => ReactTableColumns = ({ onDelete, refetch }) => [
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Slug",
    accessor: "slug",
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        editColumn={"slug"}
        pathname={"/app/users/permission"}
        permissionPrefix={"permission"}
      />
    ),
  },
];
