import { useMemo } from "react";
import { useQuery } from "react-query";
import { Card, CardBody, ReactTable } from "components";
import { deletePermission, getPermissions, tableColumns } from "./utils";
import { CrudButton, useFetchData, usePermission } from "utils";

export const PermissionList = () => {
  const { state, fetchData } = useFetchData();
  const { canUser } = usePermission();

  //
  const { data, isFetching, refetch } = useQuery(
    [
      "permissions",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getPermissions
  );

  const columns = useMemo(
    () => tableColumns({ onDelete: deletePermission, refetch }),
    []
  );

  return (
    <Card>
      <CardBody>
        {canUser("permission.item-create") && (
          <div className="flex justify-end">
            <CrudButton type="create" route={"./permissions/create"} />
          </div>
        )}

        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
        />
      </CardBody>
    </Card>
  );
};
