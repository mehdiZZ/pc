import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
} from "components";
import { useMutation, useQuery } from "react-query";
import { useParams } from "react-router-dom";
import {
  getPermission,
  updatePermissionSchema,
  updatePermission,
} from "./utils";
import {
  CrudButton,
  getErrorMessages,
  getModifiedFields,
  ListedErrorMessages,
  useNotification,
  usePermission,
} from "utils";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";
import { useState } from "react";

export const PermissionEdit = () => {
  const { slug }: any = useParams();
  const { notice } = useNotification();
  const { canUser } = usePermission();
  const { t } = useTranslation();

  const { isLoading, error, data } = useQuery(
    [`permissions/slug/`, { slug }],
    getPermission
  );

  //
  const { mutateAsync, isLoading: updateLoading } = useMutation(
    updatePermission
  );

  //
  const { errors, handleSubmit, register, formState, reset } = useForm({
    resolver: yupResolver(updatePermissionSchema),
  });

  const { dirtyFields } = formState;

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (permissionFields) => {
    //
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      //
      const updatedData = await mutateAsync({
        ...getModifiedFields({
          fields: permissionFields,
          dirties: dirtyFields,
        }),
        id: data.id,
      });

      //
      if (updatedData) {
        setErrors([]);
        reset(updatedData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isLoading || updateLoading}>
      <CardBody>
        <div className="flex justify-end">
          <CrudButton type="backToList" route={"../permissions"} />
        </div>
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            {data && (
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Flex col>
                  {!updateLoading && (
                    <ListedErrorMessages errorMessages={errorMessages} />
                  )}
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="name">Name</Label>
                    <Input
                      id="name"
                      name="name"
                      placeholder="Category Item Create"
                      defaultValue={data?.name}
                      ref={register}
                      error={errors.name?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="slug">Slug</Label>
                    <Input
                      id="slug"
                      name="slug"
                      placeholder="category.item-create"
                      defaultValue={data?.slug}
                      ref={register}
                      error={errors.slug?.message}
                    />
                  </Box>
                </Flex>
                <Button
                  theme="success"
                  disabled={!canUser("permission.item-update")}
                >
                  Submit
                </Button>
              </Form>
            )}
          </>
        )}
      </CardBody>
    </Card>
  );
};
