import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation } from "react-query";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getErrorMessages,
  usePermission,
} from "utils";
import { createPermission, createPermissionSchema } from "./utils";

export const PermissionCreate = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();
  //
  const { errors, handleSubmit, register } = useForm({
    resolver: yupResolver(createPermissionSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(createPermission);

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (permissionFields) => {
    try {
      const data = await mutateAsync(permissionFields);
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isLoading}>
      <CardBody>
        {canUser("permission.list-read") && (
          <div className="flex justify-end">
            <CrudButton type="backToList" route={"../permissions"} />
          </div>
        )}
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Flex col>
            {!isLoading && (
              <ListedErrorMessages errorMessages={errorMessages} />
            )}
            <Box md="w-4/6" lg="w-2/6">
              <Label for="name">Name</Label>
              <Input
                id="name"
                name="name"
                placeholder="Category Item Create"
                ref={register}
                error={errors.name?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="slug">Slug</Label>
              <Input
                id="slug"
                name="slug"
                placeholder="category.item-create"
                ref={register}
                error={errors.slug?.message}
              />
            </Box>
          </Flex>
          <Button theme="success">Submit</Button>
        </Form>
      </CardBody>
    </Card>
  );
};
