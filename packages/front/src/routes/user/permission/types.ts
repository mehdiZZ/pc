export type {
  PermissionUpdateReq,
  PermissionCreateReq,
} from "@backend/core/user/_permission/dto";
export type { Permission } from "@backend/core/user/_permission/entities";
