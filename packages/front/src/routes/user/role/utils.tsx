import * as yup from "yup";
import {
  CrudActions,
  GetManyQueryFunctionContext,
  GetManyResponse,
  getRequest,
  GetSingleQueryFunctionContext,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
} from "utils";
import { Role, RoleCreateReq, RoleUpdateReq } from "./types";

/////////////////-------- Validation
export const createRoleSchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
  permissions: yup
    .array()
    .of(yup.object().shape({ id: yup.string().required() }))
    .min(1),
});

export const updateRoleSchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
  permissions: yup
    .array()
    .of(yup.object().shape({ id: yup.string().required() }))
    .min(1),
});

/////////////////-------- HTTP
export const getRoles = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<Role>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getRole = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { slug }] = queryKey;
  return getRequest<Role>({
    url: _key + slug,
  });
};

export const deleteRole = async (id: string) => {
  return await postRequest({ url: `/roles/${id}`, method: "DELETE" });
};

export const createRole = async (data: RoleCreateReq) => {
  return await postRequest({ data, url: "/roles" });
};

export const updateRole = async (data: RoleUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/roles/${id}`,
    method: "PATCH",
  });
};

/////////////////-------- table columns
export const tableColumns: ({
  onDelete,
  refetch,
}: {
  onDelete: (id: string) => any;
  refetch: () => void;
}) => ReactTableColumns = ({ onDelete, refetch }) => [
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Slug",
    accessor: "slug",
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        editColumn={"slug"}
        pathname="/app/users/role"
        permissionPrefix={"role"}
      />
    ),
  },
];
