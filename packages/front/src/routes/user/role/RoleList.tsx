import { Card, CardBody, ReactTable } from "components";
import { useMemo } from "react";
import { useQuery } from "react-query";
import { CrudButton, useFetchData, usePermission } from "utils";
import { tableColumns, getRoles, deleteRole } from "./utils";

export const RoleList = () => {
  const { state, fetchData } = useFetchData();
  const { canUser } = usePermission();

  const { data, isFetching, refetch } = useQuery(
    [
      "roles",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getRoles
  );

  const columns = useMemo(
    () => tableColumns({ onDelete: deleteRole, refetch }),
    []
  );

  return (
    <Card>
      <CardBody>
        {canUser("role.item-create") && (
          <div className="flex justify-end">
            <CrudButton type="create" route={"./roles/create"} />
          </div>
        )}

        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
        />
      </CardBody>
    </Card>
  );
};
