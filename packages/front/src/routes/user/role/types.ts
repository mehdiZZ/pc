export type { Role } from "@backend/core/user/_role/entities";

export type {
  RoleCreateReq,
  RoleUpdateReq,
} from "@backend/core/user/_role/dto";
