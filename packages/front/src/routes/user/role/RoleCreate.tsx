import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactSelectController,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getErrorMessages,
  usePermission,
} from "utils";
import { getPermissions } from "../Permission/utils";
import { createRole, createRoleSchema } from "./utils";

export const RoleCreate = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();
  //
  const { data, isLoading: getPermissionsLoading } = useQuery(
    ["permissions", { limit: 200 }],
    getPermissions
  );

  //
  const { errors, handleSubmit, register, control } = useForm({
    resolver: yupResolver(createRoleSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(createRole);

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (roleFields) => {
    try {
      const data = await mutateAsync(roleFields);
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isLoading}>
      <CardBody>
        {canUser("role.list-read") && (
          <div className="flex justify-end">
            <CrudButton type="backToList" route={"../roles"} />
          </div>
        )}
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Flex col>
            {!isLoading && (
              <ListedErrorMessages errorMessages={errorMessages} />
            )}
            <Box md="w-4/6" lg="w-2/6">
              <Label for="name">Name</Label>
              <Input
                id="name"
                name="name"
                placeholder="Category Item Create"
                ref={register}
                error={errors.name?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="slug">Slug</Label>
              <Input
                id="slug"
                name="slug"
                placeholder="category.item-create"
                ref={register}
                error={errors.slug?.message}
              />
            </Box>
            <Box md="w-full">
              <Label for="permissions">Permissions</Label>
              <ReactSelectController
                controllerProps={{
                  control,
                  name: "permissions",
                  defaultValue: [],
                }}
                selectProps={{
                  error: errors.permissions?.message,
                  options: data?.data,
                  isMulti: true,
                  isLoading: getPermissionsLoading,
                }}
              />
            </Box>
          </Flex>
          <Button theme="success">Submit</Button>
        </Form>
      </CardBody>
    </Card>
  );
};
