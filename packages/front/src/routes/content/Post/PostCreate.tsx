import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactEditorController,
  ReactSelectController,
  Select,
} from "components";
import { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import { MediaController } from "routes/media";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getErrorMessages,
  RootContext,
  makeSlug,
  usePermission,
} from "utils";
import { getCategories } from "./Category";
import { getTags } from "./Tag";
import { createPost, createPostSchema } from "./utils";

export const PostCreate = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { user } = useContext(RootContext);
  const { canUser } = usePermission();
  //
  const { data: categories, isLoading: getCategoriesLoading } = useQuery(
    [
      "categories",
      {
        limit: 500,
      },
    ],
    getCategories
  );
  const { data: tags, isLoading: getTagsLoading } = useQuery(
    [
      "tags",
      {
        limit: 500,
      },
    ],
    getTags
  );

  //
  const {
    errors,
    handleSubmit,
    register,
    control,
    setValue,
    getValues,
  } = useForm({ resolver: yupResolver(createPostSchema) });

  //
  const { mutateAsync, isLoading } = useMutation(createPost);

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (postFields) => {
    try {
      const data = await mutateAsync({ ...postFields, user });
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  const generateSlug = () => {
    setValue("slug", makeSlug({ string: getValues("title") }), {
      shouldDirty: true,
    });
  };

  return (
    <Card loading={isLoading}>
      <CardBody>
        {canUser("post.list-read") && (
          <div className="flex justify-end">
            <CrudButton type="backToList" route={"../posts"} />
          </div>
        )}
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Flex>
            {!isLoading && (
              <ListedErrorMessages errorMessages={errorMessages} />
            )}
            <Box md="w-4/6" lg="w-2/6">
              <Label for="title">Title</Label>
              <Input
                id="title"
                name="title"
                ref={register}
                error={errors.title?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="slug">
                Slug
                <Button
                  onClick={generateSlug}
                  className="ml-3"
                  size="tiny"
                  theme="primary"
                >
                  Generate slug
                </Button>
              </Label>
              <Input
                id="slug"
                name="slug"
                ref={register}
                error={errors.slug?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for={"thumbnail"}>Thumbnail</Label>
              <MediaController
                controllerProps={{
                  name: "thumbnail",
                  control,
                }}
                fileProps={{
                  justOneFile: true,
                }}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="categories">Categories</Label>
              <ReactSelectController
                controllerProps={{
                  control,
                  name: "categories",
                  defaultValue: null,
                }}
                selectProps={{
                  error: errors.categories?.message,
                  options: categories?.data,
                  isMulti: true,
                  isLoading: getCategoriesLoading,
                }}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="tags">Tags</Label>
              <ReactSelectController
                controllerProps={{
                  control,
                  name: "tags",
                  defaultValue: null,
                }}
                selectProps={{
                  error: errors.tags?.message,
                  options: tags?.data,
                  isMulti: true,
                  isLoading: getTagsLoading,
                }}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="status">Status</Label>
              <Select
                name={"status"}
                ref={register}
                error={errors.status?.message}
              >
                <option value={1}>Pending</option>
                <option value={2}>Publish</option>
                <option value={3}>Disabled</option>
              </Select>
            </Box>
            <Box lg="w-full">
              <Label>Content</Label>
              <ReactEditorController
                controllerProps={{
                  control,
                  name: "content",
                }}
                editorProps={{
                  error: errors.content?.message,
                }}
              />
            </Box>
          </Flex>
          <Button theme="success">Submit</Button>
        </Form>
      </CardBody>
    </Card>
  );
};
