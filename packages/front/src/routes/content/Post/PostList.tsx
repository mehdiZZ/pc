import { useMemo, useState } from "react";
import { useQuery } from "react-query";
import { Card, CardBody, ReactTable } from "components";
import { getPosts, tableColumns, deletePost } from "./utils";
import { CrudButton, useFetchData, usePermission } from "utils";

export const PostList = () => {
  const { state, fetchData } = useFetchData();
  const { canUser } = usePermission();

  const { data, isFetching, refetch } = useQuery(
    [
      "posts",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getPosts
  );

  const columns = useMemo(
    () =>
      tableColumns({
        onDelete: deletePost,
        refetch,
      }),
    []
  );

  return (
    <Card>
      <CardBody>
        {canUser("post.item-create") && (
          <div className="flex justify-end">
            <CrudButton type="create" route={"./posts/create"} />
          </div>
        )}
        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
        />
      </CardBody>
    </Card>
  );
};
