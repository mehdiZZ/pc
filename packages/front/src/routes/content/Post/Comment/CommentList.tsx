import { yupResolver } from "@hookform/resolvers/yup";
import {
  Button,
  Card,
  CardBody,
  Form,
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ReactTable,
  TextArea,
  Typography,
} from "components";
import { useContext, useMemo, useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import {
  getErrorMessages,
  useNotification,
  RootContext,
  useFetchData,
  usePermission,
} from "utils";
import {
  tableColumns,
  getPostComments,
  deletePostComment,
  updatePostComment,
  createPostComment,
  createPostCommentSchema,
} from "./utils";

export const CommentList = () => {
  const { state, fetchData } = useFetchData();
  const { canUser } = usePermission();
  //
  const { user } = useContext(RootContext);

  //
  const { notice } = useNotification();
  const { t } = useTranslation();

  //
  const [modal, setModal] = useState<{
    isOpen: boolean;
    commentId?: string;
    postId?: string;
  }>({
    isOpen: false,
    commentId: null,
    postId: null,
  });

  //
  const { mutateAsync, isLoading: createLoading } = useMutation(
    createPostComment
  );

  //
  const { handleSubmit, errors, register } = useForm({
    resolver: yupResolver(createPostCommentSchema),
  });

  const { data, isFetching, refetch } = useQuery(
    [
      "comments",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getPostComments
  );

  const columns = useMemo(
    () =>
      tableColumns({
        onDelete: deletePostComment,
        refetch,
        updatePostComment,
        setModal,
        canUser,
      }),
    []
  );

  //
  const onSubmit = async (commentFields) => {
    try {
      const data = await mutateAsync({
        ...commentFields,
        parent: { id: modal.commentId },
        post: { id: modal.postId },
        user: { id: user.id },
        status: 2,
      });
      if (data) {
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      notice({
        textContent: getErrorMessages(err),
      });
    }
  };

  return (
    <>
      <Modal
        onClose={() => setModal({ isOpen: false })}
        isOpen={modal.isOpen}
        size="small"
      >
        <ModalHeader>
          <Typography tag="h3" className="text-xl">
            Reply to comment
          </Typography>
        </ModalHeader>
        <ModalContent>
          <Form id="reply-to-comment" onSubmit={handleSubmit(onSubmit)}>
            <TextArea
              name={"content"}
              ref={register}
              placeholder={"Type here..."}
            ></TextArea>
          </Form>
        </ModalContent>
        <ModalFooter>
          <div className="flex justify-between">
            <Button
              form="reply-to-comment"
              type="submit"
              theme="success"
              loading={createLoading}
            >
              Submit
            </Button>
            <Button
              type="button"
              theme="danger"
              onClick={() => setModal({ isOpen: false })}
            >
              Close
            </Button>
          </div>
        </ModalFooter>
      </Modal>

      <Card>
        <CardBody>
          <ReactTable
            filters={state.filter}
            columns={columns}
            data={data?.data || []}
            fetchData={fetchData}
            pageCount={data?.pageCount}
            loading={isFetching}
          />
        </CardBody>
      </Card>
    </>
  );
};
