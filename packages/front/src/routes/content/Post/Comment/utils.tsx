import * as yup from "yup";
import {
  CrudActions,
  GetManyQueryFunctionContext,
  GetManyResponse,
  getRequest,
  GetSingleQueryFunctionContext,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
} from "utils";
import { Comment, PostCommentCreateReq, PostCommentUpdateReq } from "./types";
import { Button, Select } from "components";
import { FilterProps } from "react-table";
import { CommentOutlined } from "@ant-design/icons";
import { SelectFilterColumn } from "components/ReactTable/components";
import { IPermissionSlug } from "@backend/core/seed/interfaces";

/////////////////-------- Validation
export const createPostCommentSchema = yup.object().shape({
  status: yup.number(),
  content: yup.string().required(),
  user: yup.object().shape({ id: yup.string() }),
  post: yup.object().shape({ id: yup.string() }),
  parent: yup.object().shape({ id: yup.string() }),
});

export const updatePostCommentSchema = yup.object().shape({
  status: yup.number().required(),
  content: yup.string().required(),
});

/////////////////-------- HTTP
export const getPostComments = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<Comment>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getPostComment = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { id }] = queryKey;
  return getRequest<Comment>({
    url: _key + id,
  });
};

export const deletePostComment = async (id: string) => {
  return await postRequest({ url: `/comments/${id}`, method: "DELETE" });
};

export const createPostComment = async (data: PostCommentCreateReq) => {
  return await postRequest({ data, url: "/comments" });
};

export const updatePostComment = async (data: PostCommentUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/comments/${id}`,
    method: "PATCH",
  });
};

/////////////////-------- table columns
export const tableColumns: ({
  onDelete,
  refetch,
  updatePostComment,
  setModal,
  canUser,
}: {
  onDelete: (id: string) => any;
  refetch: () => void;
  updatePostComment: (data: PostCommentUpdateReq) => any;
  setModal: ({ isOpen, commentId, postId }) => void;
  canUser: (permission: IPermissionSlug) => boolean;
}) => ReactTableColumns = ({ onDelete, refetch, setModal, canUser }) => [
  {
    Header: "Content",
    accessor: "content",
    minWidth: 230,
  },
  {
    Header: "Status",
    accessor: "status",
    Filter: (props: FilterProps<object>) => (
      <SelectFilterColumn
        props={props}
        options={[
          { value: "", label: "Nothing" },
          { value: 1, label: "Pending" },
          { value: 2, label: "Approved" },
          { value: 3, label: "Rejected" },
        ]}
      />
    ),
    // filter: "equals",
    Cell: ({ row: { original }, value }: any) => (
      <div>
        <Select
          value={value}
          onChange={async (e) => {
            if (e.target.value) {
              await updatePostComment({
                id: original.id,
                status: +e.target.value,
              });
              refetch();
            }
          }}
        >
          <option value={1}>Pending</option>
          <option value={2}>Approve</option>
          <option value={3}>Reject</option>
        </Select>
      </div>
    ),
  },
  {
    Header: "Post",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => <div>{original?.post?.title}</div>,
  },
  {
    Header: "User",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => <div>{original?.user?.username}</div>,
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <>
        {canUser("comment.item-update")}
        <div>
          <Button
            onClick={() =>
              setModal({
                isOpen: true,
                commentId: original.id,
                postId: original?.post?.id,
              })
            }
            title="Reply"
            theme="transparent"
          >
            <CommentOutlined />
          </Button>
        </div>
        <CrudActions
          editColumn={"id"}
          data={original}
          onDelete={onDelete}
          refetch={refetch}
          pathname={"/app/posts/comment"}
          permissionPrefix={"comment"}
        />
      </>
    ),
  },
];
