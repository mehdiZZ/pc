import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  Select,
  TextArea,
  Typography,
} from "components";
import { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";
import { useParams, Link } from "react-router-dom";
import {
  getPostComment,
  updatePostCommentSchema,
  updatePostComment,
} from "./utils";

import {
  CrudButton,
  getErrorMessages,
  getModifiedFields,
  ListedErrorMessages,
  useNotification,
  usePermission,
} from "utils";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";

export const CommentEdit = () => {
  const { id }: any = useParams();
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();

  //
  const { isLoading, error, data, refetch } = useQuery(
    [`comments/`, { id }],
    getPostComment
  );

  useEffect(() => {
    refetch();
  }, [id]);
  //
  const { mutateAsync, isLoading: updateLoading } = useMutation(
    updatePostComment
  );

  //
  const { errors, handleSubmit, register, formState, reset } = useForm({
    resolver: yupResolver(updatePostCommentSchema),
  });

  const { dirtyFields } = formState;

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (commentFields) => {
    //
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      //
      const updatedData = await mutateAsync({
        ...getModifiedFields({
          fields: commentFields,
          dirties: dirtyFields,
        }),
        id: data.id,
      });
      if (updatedData) {
        setErrors([]);
        reset(updatedData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isLoading || updateLoading}>
      <CardBody>
        <div className="flex justify-end">
          <CrudButton type="backToList" route={"../comments"} />
        </div>
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            {data && (
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Flex>
                  {!updateLoading && (
                    <ListedErrorMessages errorMessages={errorMessages} />
                  )}
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="user">User</Label>
                    <Input
                      id="user"
                      name="user"
                      disabled
                      defaultValue={data?.user?.username}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="post">Post</Label>
                    <Input
                      id="post"
                      name="post"
                      disabled
                      defaultValue={data?.post?.title}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-1/6">
                    <Label for="status">Set status</Label>
                    <Select
                      id={"status"}
                      defaultValue={data?.status}
                      ref={register}
                      name={"status"}
                    >
                      <option value={"1"}>Pending</option>
                      <option value={"2"}>Approve</option>
                      <option value={"3"}>Reject</option>
                    </Select>
                  </Box>
                  <Box md="w-4/6" lg="w-3/6">
                    <Label for="content">Comment</Label>
                    <TextArea
                      rows={8}
                      id="content"
                      name="content"
                      defaultValue={data?.content}
                      ref={register}
                      error={errors.content?.message}
                    ></TextArea>
                  </Box>
                  {data?.parent && (
                    <Box md="w-4/6" lg="w-3/6">
                      <Label for="parent">
                        Replied to (
                        <Link
                          className="text-blue"
                          to={`./${data?.parent?.id}`}
                        >
                          Link
                        </Link>
                        )
                      </Label>
                      <Typography>{data?.parent?.content}</Typography>
                    </Box>
                  )}
                </Flex>
                <Button
                  theme="success"
                  disabled={!canUser("comment.item-update")}
                >
                  Submit
                </Button>
              </Form>
            )}
          </>
        )}
      </CardBody>
    </Card>
  );
};
