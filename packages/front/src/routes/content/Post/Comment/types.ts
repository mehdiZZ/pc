export type {
  Comment,
  CommentStatus,
} from "@backend/modules/content/_comment/entities";

export type {
  PostCommentCreateReq,
  PostCommentUpdateReq,
} from "@backend/modules/content/_comment/dto";
