import * as yup from "yup";

import {
  CrudActions,
  GetManyQueryFunctionContext,
  GetManyResponse,
  getRequest,
  GetSingleQueryFunctionContext,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
} from "utils";

import { Post, PostCreateReq, PostUpdateReq } from "./types";

/////////////////-------- Validation
export const createPostSchema = yup.object().shape({
  title: yup.string().required(),
  slug: yup.string().required(),
  content: yup.string().optional(),
  status: yup.number().oneOf([1, 2, 3]).optional(),
  thumbnail: yup.array().of(
    yup
      .object()
      .shape({
        id: yup.string(),
      })
      .nullable()
  ),
});

export const updatePostSchema = yup.object().shape({
  title: yup.string().required(),
  slug: yup.string().required(),
  content: yup.string().optional(),
  status: yup.number().oneOf([1, 2, 3]).optional(),
  thumbnail: yup.array().of(
    yup
      .object()
      .shape({
        id: yup.string(),
      })
      .nullable()
  ),
});

/////////////////-------- HTTP
export const getPosts = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<Post>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getPost = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { slug }] = queryKey;
  return getRequest<Post>({
    url: _key + slug,
  });
};

export const createPost = async (data: PostCreateReq) => {
  return await postRequest({ data, url: "/posts" });
};

export const updatePost = async (data: PostUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/posts/${id}`,
    method: "PATCH",
  });
};

export const deletePost = async (id: string) => {
  return await postRequest({ url: `/posts/${id}`, method: "DELETE" });
};

/////////////////-------- table columns
export const tableColumns: ({
  onDelete,
  refetch,
}: {
  onDelete: (id: string) => any;
  refetch: () => void;
}) => ReactTableColumns = ({ onDelete, refetch }) => [
  {
    Header: "Title",
    accessor: "title",
  },
  {
    Header: "User",
    accessor: "user",
    Cell: ({ row: { original } }: any) => <>{original.user?.username}</>,
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        editColumn={"slug"}
        pathname={"/app/post"}
        permissionPrefix={"post"}
      />
    ),
  },
];
