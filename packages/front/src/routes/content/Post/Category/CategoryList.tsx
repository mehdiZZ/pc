import { Card, CardBody, ReactTable } from "components";
import { useMemo } from "react";
import { useQuery } from "react-query";
import { CrudButton, useFetchData, usePermission } from "utils";
import { tableColumns, getCategories, deleteCategory } from "./utils";

export const CategoryList = () => {
  const { state, fetchData } = useFetchData();
  const { canUser } = usePermission();
  const { data, isFetching, refetch } = useQuery(
    [
      "categories",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getCategories
  );

  const columns = useMemo(
    () =>
      tableColumns({
        onDelete: deleteCategory,
        refetch,
      }),
    []
  );

  return (
    <Card>
      <CardBody>
        {canUser("category.item-create") && (
          <div className="flex justify-end">
            <CrudButton type="create" route={"./categories/create"} />
          </div>
        )}
        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
        />
      </CardBody>
    </Card>
  );
};
