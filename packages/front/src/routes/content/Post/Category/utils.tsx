import * as yup from "yup";
import {
  CrudActions,
  GetManyQueryFunctionContext,
  GetManyResponse,
  getRequest,
  GetSingleQueryFunctionContext,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
} from "utils";
import { Category, TagCreateReq, TagUpdateReq } from "./types";

/////////////////-------- Validation
export const createCategorySchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
  description: yup.string().optional(),
  children: yup
    .array()
    .of(yup.object().shape({ id: yup.string().required() }))
    .nullable(),
  parent: yup.object().shape({ id: yup.string().required() }).nullable(),
});

export const updateCategorySchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
  description: yup.string().optional(),
  children: yup
    .array()
    .of(yup.object().shape({ id: yup.string().required() }))
    .nullable(),
  parent: yup.object().shape({ id: yup.string().required() }).nullable(),
});

/////////////////-------- HTTP
export const getCategories = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<Category>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getCategory = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { slug }] = queryKey;
  return getRequest<Category>({
    url: _key + slug,
  });
};

export const deleteCategory = async (id: string) => {
  return await postRequest({ url: `/categories/${id}`, method: "DELETE" });
};

export const createCategory = async (data: TagCreateReq) => {
  return await postRequest({ data, url: "/categories" });
};

export const updateCategory = async (data: TagUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/categories/${id}`,
    method: "PATCH",
  });
};

/////////////////-------- table columns
export const tableColumns: ({
  onDelete,
  refetch,
}: {
  onDelete: (id: string) => any;
  refetch: () => void;
}) => ReactTableColumns = ({ onDelete, refetch }) => [
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Slug",
    accessor: "slug",
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        editColumn={"slug"}
        pathname={"/app/posts/category"}
        permissionPrefix={"category"}
      />
    ),
  },
];
