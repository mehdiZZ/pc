import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactSelectController,
  TextArea,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getErrorMessages,
  makeSlug,
  usePermission,
} from "utils";
import { createCategory, createCategorySchema, getCategories } from "./utils";

export const CategoryCreate = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();

  //
  const { data, isLoading: getCategoriesLoading } = useQuery(
    ["categories", { limit: 500 }],
    getCategories
  );

  //
  const {
    errors,
    handleSubmit,
    register,
    control,
    setValue,
    getValues,
  } = useForm({
    resolver: yupResolver(createCategorySchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(createCategory);

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (categoryFields) => {
    try {
      const data = await mutateAsync(categoryFields);
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  //
  const generateSlug = () => {
    setValue("slug", makeSlug({ string: getValues("name") }));
  };

  return (
    <Card loading={isLoading}>
      <CardBody>
        {canUser("category.list-read") && (
          <div className="flex justify-end">
            <CrudButton type="backToList" route={"../categories"} />
          </div>
        )}
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Flex col>
            {!isLoading && (
              <ListedErrorMessages errorMessages={errorMessages} />
            )}
            <Box md="w-4/6" lg="w-2/6">
              <Label for="name">Name</Label>
              <Input
                id="name"
                name="name"
                ref={register}
                error={errors.name?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="slug">
                Slug{" "}
                <Button
                  onClick={generateSlug}
                  className="ml-3"
                  size="tiny"
                  theme="primary"
                >
                  Generate slug
                </Button>
              </Label>
              <Input
                id="slug"
                name="slug"
                ref={register}
                error={errors.slug?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="slug">Description</Label>
              <TextArea
                id="description"
                name="description"
                ref={register}
                error={errors.description?.message}
              ></TextArea>
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="parent">Parent</Label>
              <ReactSelectController
                controllerProps={{
                  control,
                  name: "parent",
                  defaultValue: null,
                }}
                selectProps={{
                  error: errors.parent?.message,
                  options: data?.data,
                  isMulti: false,
                  isLoading: getCategoriesLoading,
                }}
              />
            </Box>
          </Flex>
          <Button theme="success">Submit</Button>
        </Form>
      </CardBody>
    </Card>
  );
};
