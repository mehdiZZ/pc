import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactEditorController,
  ReactSelectController,
  Select,
} from "components";
import { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { getPost, updatePostSchema, updatePost } from "./utils";
import {
  CrudButton,
  getErrorMessages,
  getModifiedFields,
  ListedErrorMessages,
  makeSlug,
  useNotification,
  usePermission,
} from "utils";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";
import { MediaController } from "routes/media";
import { getCategories } from "./Category";
import { getTags } from "./Tag";

export const PostEdit = () => {
  const { slug }: any = useParams();
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();
  //
  const { data: categories, isLoading: getCategoriesLoading } = useQuery(
    [
      "categories",
      {
        limit: 500,
      },
    ],
    getCategories
  );

  const { data: tags, isLoading: getTagsLoading } = useQuery(
    [
      "tags",
      {
        limit: 500,
      },
    ],
    getTags
  );

  //
  const { isLoading, error, data } = useQuery(
    [`posts/slug/`, { slug }],
    getPost
  );

  const {
    errors,
    handleSubmit,
    register,
    formState,
    control,
    setValue,
    getValues,
    reset,
  } = useForm({
    resolver: yupResolver(updatePostSchema),
  });

  //
  const { mutateAsync, isLoading: updateLoading } = useMutation(updatePost);
  const { dirtyFields } = formState;

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (postFields) => {
    //
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      //
      const updatedData = await mutateAsync({
        ...getModifiedFields({
          fields: postFields,
          dirties: dirtyFields,
        }),
        id: data.id,
      });

      //
      if (updatedData) {
        setErrors([]);
        reset(updatedData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  const generateSlug = () => {
    setValue("slug", makeSlug({ string: getValues("title") }), {
      shouldDirty: true,
    });
  };

  return (
    <Card loading={isLoading || updateLoading}>
      <CardBody>
        <div className="flex justify-end">
          <CrudButton type="backToList" route={"../posts"} />
        </div>
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            {data && (
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Flex>
                  {!updateLoading && (
                    <ListedErrorMessages errorMessages={errorMessages} />
                  )}
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="title">Title</Label>
                    <Input
                      id="title"
                      name="title"
                      ref={register}
                      defaultValue={data?.title}
                      error={errors.title?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="slug">
                      Slug{" "}
                      <Button
                        onClick={generateSlug}
                        className="ml-3"
                        size="tiny"
                        theme="primary"
                      >
                        Generate slug
                      </Button>
                    </Label>
                    <Input
                      id="slug"
                      name="slug"
                      ref={register}
                      defaultValue={data?.slug}
                      error={errors.slug?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for={"thumbnail"}>Thumbnail</Label>
                    <MediaController
                      controllerProps={{
                        name: "thumbnail",
                        control,
                        defaultValue: data?.thumbnail,
                      }}
                      fileProps={{
                        justOneFile: true,
                      }}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="categories">Categories</Label>
                    <ReactSelectController
                      controllerProps={{
                        control,
                        name: "categories",
                        defaultValue: data?.categories,
                      }}
                      selectProps={{
                        error: errors.categories?.message,
                        options: categories?.data,
                        isMulti: true,
                        isLoading: getCategoriesLoading,
                      }}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="tags">Tags</Label>
                    <ReactSelectController
                      controllerProps={{
                        control,
                        name: "tags",
                        defaultValue: data?.tags,
                      }}
                      selectProps={{
                        error: errors.tags?.message,
                        options: tags?.data,
                        isMulti: true,
                        isLoading: getTagsLoading,
                      }}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="status">Status</Label>
                    <Select
                      name={"status"}
                      ref={register}
                      defaultValue={data?.status}
                      error={errors.status?.message}
                    >
                      <option value={1}>Pending</option>
                      <option value={2}>Publish</option>
                      <option value={3}>Disabled</option>
                    </Select>
                  </Box>
                  <Box lg="w-full">
                    <Label>Content</Label>
                    <ReactEditorController
                      controllerProps={{
                        control,
                        name: "content",
                        defaultValue: data?.content,
                      }}
                      editorProps={{
                        error: errors.content?.message,
                      }}
                    />
                  </Box>
                </Flex>
                <Button theme="success" disabled={!canUser("post.item-update")}>
                  Submit
                </Button>
              </Form>
            )}
          </>
        )}
      </CardBody>
    </Card>
  );
};
