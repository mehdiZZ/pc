import { Redirect, Route, Switch } from "react-router-dom";
import { CategoryList, CategoryEdit } from "./Category";
import { CategoryCreate } from "./Category/CategoryCreate";
import { CommentEdit, CommentList } from "./Comment";
import { PostCreate } from "./PostCreate";
import { PostEdit } from "./PostEdit";
import { PostList } from "./PostList";
import { TagCreate, TagList } from "./Tag";
import { TagEdit } from "./Tag/TagEdit";

export const PostRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={"/app/post"}
        render={() => <Redirect to="/app/posts" />}
      />
      <Route exact path={"/app/posts"} component={PostList} />
      <Route exact path={"/app/posts/create"} component={PostCreate} />
      <Route exact path={"/app/post/:slug"} component={PostEdit} />
      {/* Tags */}
      <Route
        exact
        path={"/app/posts/tag"}
        render={() => <Redirect to="/app/posts/tags" />}
      />
      <Route exact path={"/app/posts/tags"} component={TagList} />
      <Route exact path={"/app/posts/tags/create"} component={TagCreate} />
      <Route exact path={"/app/posts/tag/:slug"} component={TagEdit} />
      {/* Categories */}
      <Route exact path={"/app/posts/categories"} component={CategoryList} />
      <Route
        exact
        path={"/app/posts/categories/create"}
        component={CategoryCreate}
      />
      <Route
        exact
        path={"/app/posts/category/:slug"}
        component={CategoryEdit}
      />
      {/* Comments */}
      <Route exact path={"/app/posts/comments"} component={CommentList} />
      <Route exact path={"/app/posts/comment/:id"} component={CommentEdit} />
    </Switch>
  );
};
