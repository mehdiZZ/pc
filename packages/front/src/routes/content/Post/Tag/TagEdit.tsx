import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  TextArea,
} from "components";
import { useMutation, useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { getTag, updateTagSchema, updateTag } from "./utils";
import {
  CrudButton,
  getErrorMessages,
  getModifiedFields,
  ListedErrorMessages,
  makeSlug,
  useNotification,
  usePermission,
} from "utils";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";
import { useState } from "react";

export const TagEdit = () => {
  const { slug }: any = useParams();
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();

  const { isLoading, error, data } = useQuery([`tags/slug/`, { slug }], getTag);

  //
  const { mutateAsync, isLoading: updateLoading } = useMutation(updateTag);

  //
  const {
    errors,
    handleSubmit,
    register,
    formState,
    setValue,
    getValues,
    reset,
  } = useForm({
    resolver: yupResolver(updateTagSchema),
  });

  const { dirtyFields } = formState;

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (tagFields) => {
    //
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      //
      const updatedData = await mutateAsync({
        ...getModifiedFields({
          fields: tagFields,
          dirties: dirtyFields,
        }),
        id: data.id,
      });

      //
      if (updatedData) {
        setErrors([]);
        reset(updatedData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  const generateSlug = () => {
    setValue("slug", makeSlug({ string: getValues("name") }), {
      shouldDirty: true,
    });
  };

  return (
    <Card loading={isLoading || updateLoading}>
      <CardBody>
        <div className="flex justify-end">
          <CrudButton type="backToList" route={"../tags"} />
        </div>
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            {data && (
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Flex col>
                  {!updateLoading && (
                    <ListedErrorMessages errorMessages={errorMessages} />
                  )}
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="name">Name</Label>
                    <Input
                      id="name"
                      name="name"
                      placeholder="Category Item Create"
                      defaultValue={data?.name}
                      ref={register}
                      error={errors.name?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="slug">
                      Slug{" "}
                      <Button
                        onClick={generateSlug}
                        className="ml-3"
                        size="tiny"
                        theme="primary"
                      >
                        Generate slug
                      </Button>
                    </Label>
                    <Input
                      id="slug"
                      name="slug"
                      placeholder="category.item-create"
                      defaultValue={data?.slug}
                      ref={register}
                      error={errors.slug?.message}
                    />
                  </Box>
                  <Box md="w-4/6" lg="w-2/6">
                    <Label for="description">Description</Label>
                    <TextArea
                      id="description"
                      name="description"
                      defaultValue={data?.description}
                      ref={register}
                      error={errors.description?.message}
                    ></TextArea>
                  </Box>
                </Flex>
                <Button theme="success" disabled={!canUser("tag.item-update")}>
                  Submit
                </Button>
              </Form>
            )}
          </>
        )}
      </CardBody>
    </Card>
  );
};
