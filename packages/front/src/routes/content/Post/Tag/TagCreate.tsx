import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  TextArea,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation } from "react-query";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getErrorMessages,
  makeSlug,
  usePermission,
} from "utils";
import { createTag, createTagSchema } from "./utils";

export const TagCreate = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();
  //
  const { errors, handleSubmit, register, getValues, setValue } = useForm({
    resolver: yupResolver(createTagSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(createTag);

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (tagFields) => {
    try {
      const data = await mutateAsync(tagFields);
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  const generateSlug = () => {
    setValue("slug", makeSlug({ string: getValues("name") }));
  };

  return (
    <Card loading={isLoading}>
      <CardBody>
        {canUser("tag.list-read") && (
          <div className="flex justify-end">
            <CrudButton type="backToList" route={"../tags"} />
          </div>
        )}
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Flex col>
            {!isLoading && (
              <ListedErrorMessages errorMessages={errorMessages} />
            )}
            <Box md="w-4/6" lg="w-2/6">
              <Label for="name">Name</Label>
              <Input
                id="name"
                name="name"
                ref={register}
                error={errors.name?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="slug">
                Slug{" "}
                <Button
                  onClick={generateSlug}
                  className="ml-3"
                  size="tiny"
                  theme="primary"
                >
                  Generate slug
                </Button>
              </Label>
              <Input
                id="slug"
                name="slug"
                ref={register}
                error={errors.slug?.message}
              />
            </Box>
            <Box md="w-4/6" lg="w-2/6">
              <Label for="description">Description</Label>
              <TextArea
                id="description"
                name="description"
                ref={register}
                error={errors.description?.message}
              ></TextArea>
            </Box>
          </Flex>
          <Button theme="success">Submit</Button>
        </Form>
      </CardBody>
    </Card>
  );
};
