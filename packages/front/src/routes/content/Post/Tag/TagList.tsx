import { useMemo } from "react";
import { useQuery } from "react-query";
import { Card, CardBody, ReactTable } from "components";
import { deleteTag, getTags, tableColumns } from "./utils";
import { CrudButton, useFetchData, usePermission } from "utils";

export const TagList = () => {
  const { state, fetchData } = useFetchData();
  const { canUser } = usePermission();

  const { data, isFetching, refetch } = useQuery(
    [
      "tags",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getTags
  );

  const columns = useMemo(
    () => tableColumns({ onDelete: deleteTag, refetch }),
    []
  );

  return (
    <Card>
      <CardBody>
        {canUser("tag.item-create") && (
          <div className="flex justify-end">
            <CrudButton type="create" route={"./tags/create"} />
          </div>
        )}
        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
        />
      </CardBody>
    </Card>
  );
};
