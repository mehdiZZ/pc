export type { Tag } from "@backend/modules/content/_taxonomy/entities";
export type {
  TagCreateReq,
  TagUpdateReq,
} from "@backend/modules/content/_taxonomy/dto";
