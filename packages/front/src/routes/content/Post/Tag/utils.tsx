import * as yup from "yup";

import {
  CrudActions,
  GetManyQueryFunctionContext,
  GetManyResponse,
  getRequest,
  GetSingleQueryFunctionContext,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
} from "utils";

import { Tag, TagUpdateReq, TagCreateReq } from "./types";

/////////////////-------- Validation
export const createTagSchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
  description: yup.string().optional(),
});

export const updateTagSchema = yup.object().shape({
  name: yup.string().required(),
  slug: yup.string().required(),
  description: yup.string().optional(),
});

/////////////////-------- HTTP
export const getTags = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<Tag>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getTag = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { slug }] = queryKey;
  return getRequest<Tag>({
    url: _key + slug,
  });
};

export const createTag = async (data: TagCreateReq) => {
  return await postRequest({ data, url: "/tags" });
};

export const updateTag = async (data: TagUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/tags/${id}`,
    method: "PATCH",
  });
};

export const deleteTag = async (id: string) => {
  return await postRequest({ url: `/tags/${id}`, method: "DELETE" });
};

/////////////////-------- table columns
export const tableColumns: ({
  onDelete,
  refetch,
}: {
  onDelete: (id: string) => any;
  refetch: () => void;
}) => ReactTableColumns = ({ onDelete, refetch }) => [
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Slug",
    accessor: "slug",
  },
  {
    Header: "Description",
    accessor: "description",
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        editColumn={"slug"}
        pathname={"/app/posts/tag"}
        permissionPrefix={"tag"}
      />
    ),
  },
];
