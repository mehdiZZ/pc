import { CloseCircleOutlined, PlusCircleOutlined } from "@ant-design/icons";
import { Box, Button, Flex, Hr, Input, Label } from "components";
import { useEffect, useRef } from "react";
import { useFieldArray, useFormContext } from "react-hook-form";
import { Page } from "../types";
import L from "leaflet";
const center = { lat: 35.69981, lng: 51.33809 };

const latName = "metaData.location.lat";
const lngName = "metaData.location.lng";

export const Contact = ({
  metaData,
  mode,
}: Partial<Page> & { mode: string }) => {
  const { control, register, errors, watch, setValue } = useFormContext();

  const { fields, append, remove } = useFieldArray({
    control,
    name: "metaData.phonNumbers",
  });

  const setPosition = ({ lat, lng }: { lat: number; lng: number }) => {
    setValue(latName, lat, { shouldDirty: true });
    setValue(lngName, lng, { shouldDirty: true });
  };

  //
  const dragEnd = (e) => {
    const { lat, lng } = e?.target?.getLatLng();
    setPosition({ lat, lng });
  };

  const map = useRef(null);
  const marker = useRef(null);

  useEffect(() => {
    const latLng = { lat: center.lat, lng: center.lng };
    if (mode === "edit") {
      latLng.lat = metaData?.location?.lat;
      latLng.lng = metaData?.location?.lng;
    }

    map.current = L.map("addressMap", {
      layers: [
        new L.TileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
          attribution:
            'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        }),
      ],
    }).setView([latLng.lat, latLng.lng], 13);

    marker.current = L.marker([latLng.lat, latLng.lng], {
      draggable: true,
      autoPan: true,
    })
      .addTo(map.current)
      .on("dragend", dragEnd);
  }, [metaData?.location?.lat, metaData?.location?.lng]);

  return (
    <>
      <Box md="w-4/6" lg="w-2/6">
        <Label for="companyName">Company Name</Label>
        <Input
          id="companyName"
          name="metaData.companyName"
          ref={register}
          error={errors.metaData?.companyName?.message}
          defaultValue={metaData?.companyName}
        />
      </Box>
      <Box md="w-4/6" lg="w-4/6">
        <Label for="address">Address</Label>
        <Input
          id="address"
          name="metaData.address"
          ref={register}
          error={errors.metaData?.address?.message}
          defaultValue={metaData?.address}
        />
      </Box>

      <Box md="w-4/6" lg="w-2/6">
        <Label>Socials</Label>
        <Hr verticalMargin={"mb-4"} />
        <Flex>
          <Box md="w-2/6" className="flex">
            <Label for="instagram">Instagram</Label>
          </Box>
          <Box md="w-4/6">
            <Input
              id="instagram"
              name="metaData.socials.instagram"
              placeholder="Instagram ID"
              ref={register}
              error={errors.metaData?.socials?.instagram?.message}
              defaultValue={metaData?.socials?.instagram}
            />
          </Box>
        </Flex>
        <Flex>
          <Box md="w-2/6" className="flex">
            <Label for="telegram">Telegram</Label>
          </Box>
          <Box md="w-4/6">
            <Input
              id="telegram"
              name="metaData.socials.telegram"
              placeholder="Telegram ID"
              ref={register}
              error={errors.metaData?.socials?.telegram?.message}
              defaultValue={metaData?.socials?.telegram}
            />
          </Box>
        </Flex>
        <Flex>
          <Box md="w-2/6" className="flex">
            <Label for="whatsapp">Whatsapp</Label>
          </Box>
          <Box md="w-4/6">
            <Input
              id="whatsapp"
              name="metaData.socials.whatsapp"
              placeholder="Phone Number: 989123456789"
              ref={register}
              error={errors.metaData?.socials?.whatsapp?.message}
              defaultValue={metaData?.socials?.whatsapp}
            />
          </Box>
        </Flex>
      </Box>
      <Box md="w-4/6" lg="w-4/6">
        <Label>
          Phone Numbers{" "}
          <Button
            size="small"
            theme={"transparent"}
            onClick={() => append({ title: "Title", value: "989123456783" })}
          >
            <PlusCircleOutlined />
          </Button>
        </Label>
        <Hr verticalMargin={"mb-4"} />
        {fields.map((item, index) => {
          return (
            <Flex key={item.id}>
              <Box md="w-5/12">
                <Label for={`metaData.phonNumbers[${index}].title`}>
                  Title
                </Label>
                <Input
                  id={`metaData.phonNumbers[${index}].title`}
                  name={`metaData.phonNumbers[${index}].title`}
                  ref={register()}
                  error={errors.metaData?.phonNumbers?.[index]?.title?.message}
                  defaultValue={item.title}
                />
              </Box>
              <Box md="w-5/12">
                <Label for={`metaData.phonNumbers[${index}].value`}>
                  Value
                </Label>
                <Input
                  id={`metaData.phonNumbers[${index}].value`}
                  name={`metaData.phonNumbers[${index}].value`}
                  ref={register()}
                  type="number"
                  error={errors.metaData?.phonNumbers?.[index]?.value?.message}
                  defaultValue={item.value}
                />
              </Box>
              <Box md="w-2/12" className="flex">
                <Button onClick={() => remove(index)} theme={"transparent"}>
                  <CloseCircleOutlined />
                </Button>
              </Box>
            </Flex>
          );
        })}
      </Box>
      {(mode === "create" || metaData) && (
        <Box sm="w-full">
          <Label>Location</Label>
          <div id="addressMap" className="h-96"></div>
          <Input
            type="text"
            className="hidden"
            name={latName}
            ref={register}
            defaultValue={metaData?.location?.lat || center.lat}
          />

          <Input
            type="text"
            className="hidden"
            name={lngName}
            ref={register}
            defaultValue={metaData?.location?.lng || center.lng}
          />
        </Box>
      )}
    </>
  );
};
