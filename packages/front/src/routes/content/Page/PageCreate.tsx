import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactEditorController,
  Select,
} from "components";
import { useContext, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation } from "react-query";
import { MediaController } from "routes/media";
import {
  useNotification,
  ListedErrorMessages,
  CrudButton,
  getErrorMessages,
  RootContext,
  makeSlug,
} from "utils";

import { createPage, createPageSchema, getTemplate } from "./utils";

export const PageCreate = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { user } = useContext(RootContext);

  const formMethods = useForm({ resolver: yupResolver(createPageSchema) });

  //
  const {
    errors,
    handleSubmit,
    register,
    control,
    setValue,
    getValues,
    watch,
  } = formMethods;
  const template = watch("template");

  //
  const { mutateAsync, isLoading } = useMutation(createPage);

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (pageFields) => {
    try {
      const data = await mutateAsync({ ...pageFields, user });
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:created"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  const generateSlug = () => {
    setValue("slug", makeSlug({ string: getValues("title") }), {
      shouldDirty: true,
    });
  };

  return (
    <Card loading={isLoading}>
      <CardBody>
        <div className="flex justify-end">
          <CrudButton type="backToList" route={"../pages"} />
        </div>
        <FormProvider {...formMethods}>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Flex>
              {!isLoading && (
                <ListedErrorMessages errorMessages={errorMessages} />
              )}
              <Box md="w-4/6" lg="w-2/6">
                <Label for="title">Title</Label>
                <Input
                  id="title"
                  name="title"
                  ref={register}
                  error={errors.title?.message}
                />
              </Box>
              <Box md="w-4/6" lg="w-2/6">
                <Label for="slug">
                  Slug
                  <Button
                    onClick={generateSlug}
                    className="ml-3"
                    size="tiny"
                    theme="primary"
                  >
                    Generate slug
                  </Button>
                </Label>
                <Input
                  id="slug"
                  name="slug"
                  ref={register}
                  error={errors.slug?.message}
                />
              </Box>
              <Box md="w-4/6" lg="w-2/6">
                <Label for={"thumbnail"}>Thumbnail</Label>
                <MediaController
                  controllerProps={{
                    name: "thumbnail",
                    control,
                  }}
                  fileProps={{
                    justOneFile: true,
                  }}
                />
              </Box>
              <Box md="w-4/6" lg="w-2/6">
                <Label for="template">Template</Label>
                <Select
                  name="template"
                  ref={register}
                  id="template"
                  error={errors.template?.message}
                >
                  <option value="">Select</option>
                  <option value={"Contact"}>Contact</option>
                </Select>
              </Box>
              <Box lg="w-full">
                <Label>Content</Label>
                <ReactEditorController
                  controllerProps={{
                    control,
                    name: "content",
                  }}
                  editorProps={{
                    error: errors.content?.message,
                  }}
                />
              </Box>
              {getTemplate({ template })}
            </Flex>
            <Button theme="success">Submit</Button>
          </Form>
        </FormProvider>
      </CardBody>
    </Card>
  );
};
