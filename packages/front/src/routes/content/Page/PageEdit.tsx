import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
  ReactEditorController,
  Select,
} from "components";
import { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { getPage, updatePageSchema, updatePage, getTemplate } from "./utils";
import {
  CrudButton,
  getErrorMessages,
  getModifiedFields,
  ListedErrorMessages,
  makeSlug,
  useNotification,
  usePermission,
} from "utils";
import { FormProvider, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";
import { MediaController } from "routes/media";

export const PageEdit = () => {
  const { slug }: any = useParams();
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();

  const { isLoading, error, data } = useQuery(
    [`pages/slug/`, { slug }],
    getPage
  );

  const formMethods = useForm({
    resolver: yupResolver(updatePageSchema),
  });

  //
  const {
    errors,
    handleSubmit,
    register,
    formState,
    control,
    setValue,
    getValues,
    reset,
    watch,
  } = formMethods;

  //
  const template = watch("template");

  useEffect(() => {
    reset(data);
  }, [data]);

  //
  const { mutateAsync, isLoading: updateLoading } = useMutation(updatePage);

  const { dirtyFields } = formState;

  //
  const [errorMessages, setErrors] = useState([]);

  //
  const onSubmit = async (pageFields) => {
    //
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      //
      const updatedData = await mutateAsync({
        ...getModifiedFields({
          fields: pageFields,
          dirties: dirtyFields,
        }),
        id: data.id,
      });

      //
      if (updatedData) {
        setErrors([]);
        reset(updatedData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  const generateSlug = () => {
    setValue("slug", makeSlug({ string: getValues("title") }), {
      shouldDirty: true,
    });
  };

  return (
    <Card loading={isLoading || updateLoading}>
      <CardBody>
        <div className="flex justify-end">
          <CrudButton type="backToList" route={"../pages"} />
        </div>
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            {data && (
              <FormProvider {...formMethods}>
                <Form onSubmit={handleSubmit(onSubmit)}>
                  <Flex>
                    {!updateLoading && (
                      <ListedErrorMessages errorMessages={errorMessages} />
                    )}
                    <Box md="w-4/6" lg="w-2/6">
                      <Label for="title">Title</Label>
                      <Input
                        id="title"
                        name="title"
                        ref={register}
                        defaultValue={data?.title}
                        error={errors.title?.message}
                      />
                    </Box>
                    <Box md="w-4/6" lg="w-2/6">
                      <Label for="slug">
                        Slug{" "}
                        <Button
                          onClick={generateSlug}
                          className="ml-3"
                          size="tiny"
                          theme="primary"
                        >
                          Generate slug
                        </Button>
                      </Label>
                      <Input
                        id="slug"
                        name="slug"
                        ref={register}
                        defaultValue={data?.slug}
                        error={errors.slug?.message}
                      />
                    </Box>
                    <Box md="w-4/6" lg="w-2/6">
                      <Label for={"thumbnail"}>Thumbnail</Label>
                      <MediaController
                        controllerProps={{
                          name: "thumbnail",
                          control,
                          defaultValue: data?.thumbnail,
                        }}
                        fileProps={{
                          justOneFile: true,
                        }}
                      />
                    </Box>
                    <Box md="w-4/6" lg="w-2/6">
                      <Label for="template">Template</Label>
                      <Select
                        name="template"
                        ref={register}
                        id="template"
                        defaultValue={data?.template}
                        error={errors.template?.message}
                        disabled
                      >
                        <option value={"Contact"}>Contact</option>
                      </Select>
                    </Box>
                    <Box lg="w-full">
                      <Label>Content</Label>
                      <ReactEditorController
                        controllerProps={{
                          control,
                          name: "content",
                          defaultValue: data?.content,
                        }}
                        editorProps={{
                          error: errors.content?.message,
                        }}
                      />
                    </Box>
                    {getTemplate({ template, data, mode: "edit" })}
                  </Flex>
                  <Button
                    theme="success"
                    disabled={!canUser("page.item-update")}
                  >
                    Submit
                  </Button>
                </Form>
              </FormProvider>
            )}
          </>
        )}
      </CardBody>
    </Card>
  );
};
