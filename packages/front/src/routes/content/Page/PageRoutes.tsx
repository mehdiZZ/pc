import { Redirect, Route, Switch } from "react-router-dom";
import { PageCreate } from "./PageCreate";
import { PageEdit } from "./PageEdit";
import { PageList } from "./PageList";

export const PageRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={"/app/page"}
        render={() => <Redirect to="/app/pages" />}
      />
      <Route exact path={"/app/pages"} component={PageList} />
      <Route exact path={"/app/pages/create"} component={PageCreate} />
      <Route exact path={"/app/page/:slug"} component={PageEdit} />
    </Switch>
  );
};
