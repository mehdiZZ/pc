import { useMemo, useState } from "react";
import { useQuery } from "react-query";
import { Card, CardBody, ReactTable } from "components";
import { getPages, tableColumns, deletePage } from "./utils";
import { CrudButton, useFetchData, usePermission } from "utils";

export const PageList = () => {
  const { state, fetchData } = useFetchData();
  const { canUser } = usePermission();
  const { data, isFetching, refetch } = useQuery(
    [
      "pages",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getPages,
    {
      keepPreviousData: true,
    }
  );

  const columns = useMemo(
    () =>
      tableColumns({
        onDelete: deletePage,
        refetch,
      }),
    []
  );

  return (
    <Card>
      <CardBody>
        {canUser("page.item-create") && (
          <div className="flex justify-end">
            <CrudButton type="create" route={"./pages/create"} />
          </div>
        )}
        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
        />
      </CardBody>
    </Card>
  );
};
