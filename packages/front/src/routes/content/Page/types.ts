export type { Page, ContactPage } from "@backend/modules/content/entities";
export type {
  PageUpdateReq,
  PageCreateReq,
} from "@backend/modules/content/dto";
