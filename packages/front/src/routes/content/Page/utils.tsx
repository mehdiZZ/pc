import * as yup from "yup";

import {
  CrudActions,
  GetManyQueryFunctionContext,
  GetManyResponse,
  getRequest,
  GetSingleQueryFunctionContext,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
} from "utils";

import { PageCreateReq, PageUpdateReq, Page } from "./types";
import { Contact } from "./templates";

const contactSchema = {
  companyName: yup.string().optional(),
  address: yup.string().optional(),
  socials: yup.object().shape({
    instagram: yup.string().optional(),
    telegram: yup.string().optional(),
    whatsapp: yup.string().optional(),
  }),
  phonNumbers: yup.array().of(
    yup.object().shape({
      title: yup.string().optional(),
      value: yup.string().required(),
    })
  ),
  location: yup.object().shape({
    lat: yup.number().optional(),
    lng: yup.number().optional(),
  }),
};

/////////////////-------- Validation
export const createPageSchema = yup.object().shape({
  title: yup.string().required(),
  slug: yup.string().required(),
  content: yup.string().optional(),
  thumbnail: yup.array().of(
    yup
      .object()
      .shape({
        id: yup.string(),
      })
      .nullable()
  ),
  template: yup.string().optional(),
  metaData: yup.object().when("template", (template, schema) => {
    switch (template) {
      case "Contact":
        return schema.shape(contactSchema);
      default:
        return schema.optional();
    }
  }),
});

export const updatePageSchema = yup.object().shape({
  title: yup.string().required(),
  slug: yup.string().required(),
  content: yup.string().optional(),
  thumbnail: yup.array().of(
    yup
      .object()
      .shape({
        id: yup.string(),
      })
      .nullable()
  ),
  template: yup.string().optional(),
  metaData: yup.object().when("template", (template, schema) => {
    switch (template) {
      case "Contact":
        return schema.shape(contactSchema);
      default:
        return schema.optional();
    }
  }),
});

/////////////////-------- HTTP
export const getPages = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;
  return getRequest<GetManyResponse<Page>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const getPage = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, { slug }] = queryKey;
  return getRequest<Page>({
    url: _key + slug,
  });
};

export const createPage = async (data: PageCreateReq) => {
  return await postRequest({ data, url: "/pages" });
};

export const updatePage = async (data: PageUpdateReq) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/pages/${id}`,
    method: "PATCH",
  });
};

export const deletePage = async (id: string) => {
  return await postRequest({ url: `/pages/${id}`, method: "DELETE" });
};

/////////////////-------- table columns
export const tableColumns: ({
  onDelete,
  refetch,
}: {
  onDelete: (id: string) => any;
  refetch: () => void;
}) => ReactTableColumns = ({ onDelete, refetch }) => [
  {
    Header: "Title",
    accessor: "title",
  },
  {
    Header: "User",
    accessor: "user",
    Cell: ({ row: { original } }: any) => <>{original.user?.username}</>,
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        editColumn={"slug"}
        pathname={"/app/page"}
        permissionPrefix={"page"}
      />
    ),
  },
];
/////////////////-------- others
export const getTemplate = ({
  template,
  data = undefined,
  mode = "create",
}: {
  template: string;
  data?: Page;
  mode?: string;
}) => {
  switch (template) {
    case "Contact":
      return <Contact {...data} mode={mode} />;
    default:
      return "";
  }
};

// export const generateContactValues: () => ContactPage = () => ({
//   companyName: "",
//   address: "",
//   socials: {
//     instagram: "",
//     telegram: "",
//     whatsapp: "",
//   },
//   phonNumbers: [{ title: "", value: "" }],
//   location: { lat: 35.69981, lng: 51.33809 },
// });
