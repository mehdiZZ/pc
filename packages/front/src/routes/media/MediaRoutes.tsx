import { Route, Switch } from "react-router-dom";
import { MediaList } from "./MediaList";

export const MediaRoutes = () => {
  return (
    <Switch>
      <Route exact path={"/app/media"} component={MediaList} />
    </Switch>
  );
};
