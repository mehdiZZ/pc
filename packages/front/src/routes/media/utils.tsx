import { ShowSingleFile } from "components/Upload";
import {
  CrudActions,
  GetManyResponse,
  getRequest,
  getUrlWithQueryStrings,
  postRequest,
  ReactTableColumns,
  SERVER_URL,
  uploadRequest,
  GetManyQueryFunctionContext,
} from "utils";
import { Media } from "./types";

/////////////////-------- HTTP
export const getMedia = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, props] = queryKey;

  return getRequest<GetManyResponse<Media>>({
    url: getUrlWithQueryStrings(_key, props),
  });
};

export const uploadFilesReq = async ({ files, setPercentCompleted }) => {
  return await uploadRequest({ files, setPercentCompleted });
};

export const deleteMedia = async (id: string | string[]) => {
  return await postRequest({
    url: `/media/deleteFiles`,
    method: "DELETE",
    data: { ids: typeof id === "string" ? [id] : [...id] },
  });
};

/////////////////-------- funcs
export const getThumbnail = ({ cropped, path }) => {
  if (cropped?.length) {
    const thumbnail = cropped.find(
      (croppedFile) => croppedFile.sizeLabel === "thumbnail"
    );
    if (thumbnail) return thumbnail.path;
  }

  return path;
};

/////////////////-------- table columns

export const tableColumns: ({ onDelete, refetch }) => ReactTableColumns = ({
  onDelete,
  refetch,
}) => [
  {
    Header: "FileName",
    accessor: "fileName",
    Cell: ({ row: { original } }: any) => (
      <a
        title={original.filename}
        href={SERVER_URL + "/" + getThumbnail(original)}
        rel="noopener noreferrer"
        target={"_blank"}
      >
        <ShowSingleFile
          {...original}
          type={original.mimeType}
          filename={original.fileName}
        />
      </a>
    ),
  },
  {
    Header: "Type",
    accessor: "mimeType",
  },
  {
    Header: "Sizes",
    accessor: "cropped",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ value, row: { original } }: any) => (
      <small>
        {original.width}
        {value?.length ? ` & [${value.map((v) => `${v.sizeLabel}`)}]` : null}
      </small>
    ),
  },
  {
    Header: "Action",
    disableFilters: true,
    disableSortBy: true,
    Cell: ({ row: { original } }: any) => (
      <CrudActions
        data={original}
        onDelete={onDelete}
        refetch={refetch}
        permissionPrefix={"media"}
      />
    ),
  },
];
