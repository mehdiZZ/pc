import { CloudUploadOutlined } from "@ant-design/icons";
import { Button, List } from "components";
import SimpleBar from "simplebar-react";
import { Controller } from "react-hook-form";
import { MediaModal } from "./MediaModal";
import { useState } from "react";
import { MediaRows } from "./MediaRows";
import { MediaControllerType } from "./types";

export const MediaController = ({
  fileProps,
  controllerProps,
}: MediaControllerType) => {
  const [isOpen, setModal] = useState(false);

  return (
    <Controller
      {...controllerProps}
      name={controllerProps.name}
      control={controllerProps.control}
      defaultValue={controllerProps.defaultValue || []}
      render={({ onChange, value }) => {
        return (
          <>
            <SimpleBar style={{ maxHeight: 200 }}>
              {/* show selected files */}
              <List className="flex flex-wrap">
                <MediaRows rows={value} onChange={onChange} />
              </List>
            </SimpleBar>
            <div className="my-2 mx-1">
              {/* select/upload new files */}
              <Button theme={"info"} onClick={() => setModal(!isOpen)}>
                <CloudUploadOutlined />
              </Button>
            </div>
            <MediaModal
              isModalOpen={isOpen}
              onModalChange={(status) => setModal(status)}
              selectedFiles={value}
              justOneFile={fileProps?.justOneFile}
              setSelectedFiles={(rows) => onChange(rows)}
            />
          </>
        );
      }}
    />
  );
};
