import {
  Button,
  Flex,
  Input,
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Upload,
} from "components";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import { useFetchData, useNotification } from "utils";
import { MediaSelect } from "./MediaSelect";
import { getMedia, uploadFilesReq } from "../utils";

export const MediaModal = ({
  isModalOpen = false,
  onModalChange,
  setSelectedFiles,
  selectedFiles,
  justOneFile,
}) => {
  const { t } = useTranslation();
  const [isOpen, setModalStatus] = useState(false);
  const { notice } = useNotification();
  const { state, fetchData } = useFetchData();

  const { data, refetch } = useQuery(
    [
      "media",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getMedia
  );

  useEffect(() => {
    setModalStatus(isModalOpen);
  }, [isModalOpen]);

  //
  const { mutateAsync, isLoading } = useMutation(uploadFilesReq);

  const [clearFiles, setClearFiles] = useState<boolean>(false);

  const [percentCompleted, setPercentCompleted] = useState(0);

  const uploadFiles = async (files: File[]) => {
    try {
      const data = await mutateAsync({ files, setPercentCompleted });
      if (data) {
        notice({
          textContent: t("crud:uploaded"),
        });
      }

      setClearFiles(true);
      setPercentCompleted(0);
      refetch();
    } catch (err) {
      setClearFiles(true);
    }
  };

  return (
    <Modal onClose={() => onModalChange(false)} isOpen={isOpen} size="large">
      <ModalHeader>
        <Flex>
          <Input
            placeholder="File name..."
            inputSize="small"
            type="text"
            className="max-w-md mx-auto"
            onChange={(e) => {
              if (e.target.value) {
                fetchData({
                  page: state.page,
                  limit: state.limit,
                  filter: [
                    {
                      id: "fileName",
                      value: { is: e.target.value, type: "$cont" },
                    },
                  ],
                });
              } else {
                fetchData({ page: state.page, limit: state.limit });
              }
            }}
          />
        </Flex>
      </ModalHeader>
      <ModalContent>
        <MediaSelect
          data={data}
          fetchData={fetchData}
          selectedFiles={selectedFiles}
          justOneFile={justOneFile}
          setSelectedFiles={setSelectedFiles}
        />
        <div className="mt-4">
          <Upload
            loading={isLoading}
            clearFiles={clearFiles}
            uploadFiles={uploadFiles}
            uploadingProgress={percentCompleted}
          />
        </div>
      </ModalContent>
      <ModalFooter>
        <Button
          theme="success"
          className="mx-4"
          onClick={() => onModalChange(false)}
        >
          {t("ok")}
        </Button>
        <Button
          theme="danger"
          className="mx-4"
          onClick={() => {
            setSelectedFiles([]);
            onModalChange(false);
          }}
        >
          {t("cancel")}
        </Button>
      </ModalFooter>
    </Modal>
  );
};
