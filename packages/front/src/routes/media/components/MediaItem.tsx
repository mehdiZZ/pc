import { CloseOutlined } from "@ant-design/icons";
import { Button, ListItem, ShowSingleFile } from "components";

export const MediaItem = ({ onChange, media, arrayFiles = null }) => {
  return (
    <ListItem className="mx-1 relative w-16 h-16 rounded">
      <ShowSingleFile
        {...media}
        type={media.mimeType}
        filename={media.fileName}
      />

      <Button
        className={"absolute top-0 z-10 opacity-90"}
        size={"tiny"}
        theme={"danger"}
        title={"Delete"}
        onClick={() => {
          if (arrayFiles) {
            onChange(arrayFiles.filter((file) => file.id !== media.id));
          } else {
            onChange([]);
          }
        }}
      >
        <CloseOutlined />
      </Button>
    </ListItem>
  );
};
