import {
  Box,
  Card,
  CardBody,
  Flex,
  Input,
  Label,
  ReactPagination,
  ShowSingleFile,
} from "components";
import { Media } from "../types";

export const MediaSelect = ({
  setSelectedFiles,
  selectedFiles = [],
  justOneFile,
  data,
  fetchData,
}) => {
  const isFileActive = (selectedFiles, media) => {
    return !!selectedFiles?.find((state) => state.id === media.id);
  };

  return (
    <div className="media-select">
      <Flex>
        {data?.data?.map((media) => {
          return (
            <Box
              className="max-w-xs mx-auto lg:mx-0"
              md={"w-1/3"}
              lg={"w-1/4"}
              xl={"w-1/5"}
              key={media.id}
            >
              <Label for={`media-${media.id}`} block>
                <Card
                  className={`overflow-hidden w-full cursor-pointer ${
                    isFileActive(selectedFiles, media) ? "file-active" : ""
                  }`}
                >
                  <ShowSingleFile
                    {...media}
                    type={media.mimeType}
                    filename={media.fileName}
                  />
                  <CardBody>
                    <Flex>
                      <div>
                        <Input
                          onChange={(e) => {
                            let newFiles: Media[] | null = selectedFiles
                              ? [...selectedFiles]
                              : [];

                            if (e.target.checked) {
                              if (justOneFile) {
                                newFiles = [media];
                              } else {
                                newFiles.push(media);
                              }
                            } else {
                              if (justOneFile) {
                                newFiles = null;
                              } else {
                                newFiles = newFiles.filter(
                                  (state) => state.id !== media.id
                                );
                              }
                            }
                            setSelectedFiles(newFiles);
                          }}
                          checked={isFileActive(selectedFiles, media)}
                          id={`media-${media.id}`}
                          type="checkbox"
                        />
                      </div>
                      <div className="text-xs mx-1 inline-flex items-center overflow-hidden no-wrap overflow-ellipsis whitespace-nowrap">
                        {media.fileName}
                      </div>
                    </Flex>
                  </CardBody>
                </Card>
              </Label>
            </Box>
          );
        })}
      </Flex>
      {data?.total > 0 && (
        <Flex className="justify-center">
          <ReactPagination
            onChange={(page) => fetchData({ page })}
            current={data.page}
            pageSize={10}
            total={data.total}
          />
        </Flex>
      )}
    </div>
  );
};
