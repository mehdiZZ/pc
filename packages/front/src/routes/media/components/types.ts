import { MyControllerProps } from "utils";
import { Media } from "../types";

export type MediaControllerType = {
  fileProps?: {
    justOneFile?: boolean;
  };
  controllerProps: MyControllerProps<Media[]>;
};
