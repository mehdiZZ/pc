import { MediaItem } from "./MediaItem";

export const MediaRows = ({ rows, onChange }) => {
  let ret = null;
  if (rows) {
    if (Array.isArray(rows)) {
      ret = rows.map((media) => {
        return (
          <MediaItem
            key={media.id}
            onChange={onChange}
            media={media}
            arrayFiles={rows}
          />
        );
      });
    } else {
      ret = <MediaItem onChange={onChange} media={rows} />;
    }
  }
  return ret;
};
