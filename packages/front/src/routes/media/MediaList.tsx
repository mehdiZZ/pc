import { CloseSquareOutlined } from "@ant-design/icons";
import { Button, Card, CardBody, ReactTable, Upload } from "components";
import { useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import {
  getErrorMessages,
  ListedErrorMessages,
  useFetchData,
  useNotification,
  usePermission,
} from "utils";
import { deleteMedia, getMedia, tableColumns, uploadFilesReq } from "./utils";

export const MediaList = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();
  const { state, fetchData } = useFetchData();

  const { data, isFetching, refetch } = useQuery(
    [
      "media",
      {
        page: state.page,
        limit: state.limit,
        filter: state.filter,
        sort: state.sort,
      },
    ],
    getMedia
  );

  const columns = useMemo(
    () => tableColumns({ refetch, onDelete: deleteMedia }),
    []
  );
  //
  const [percentCompleted, setPercentCompleted] = useState(0);

  //
  const { mutateAsync, isLoading } = useMutation(uploadFilesReq);

  const [clearFiles, setClearFiles] = useState<boolean>(false);

  //
  const [errorMessages, setErrors] = useState([]);

  const uploadFiles = async (files: File[]) => {
    try {
      const data = await mutateAsync({ files, setPercentCompleted });
      if (data) {
        setErrors([]);
        notice({
          textContent: t("crud:uploaded"),
        });
      }

      setClearFiles(true);
      setErrors([]);
      setPercentCompleted(0);
      refetch();
    } catch (err) {
      setErrors(getErrorMessages(err));
      setClearFiles(true);
    }
  };

  const [selectedFiles, setSelectedFiles] = useState([]);

  return (
    <Card className="media-list">
      <CardBody>
        {!isLoading && <ListedErrorMessages errorMessages={errorMessages} />}
        {canUser("media.item-create") && (
          <Upload
            loading={isLoading}
            clearFiles={clearFiles}
            uploadFiles={uploadFiles}
            uploadingProgress={percentCompleted}
          />
        )}
        {!!selectedFiles.length && (
          <Button
            title={"Delete all"}
            size={"tiny"}
            theme={"transparent"}
            className={"focus:outline-none pt-0 mx-1"}
            onClick={async () => {
              try {
                const ids = selectedFiles.map(
                  (file) => file?.original?.id
                ) as any;
                await deleteMedia(ids);
              } catch (err) {
                notice({
                  textContent: getErrorMessages(err)[0],
                  status: "error",
                });
              }
              await refetch();
            }}
          >
            <CloseSquareOutlined className="text-red text-lg" />
          </Button>
        )}
        <ReactTable
          filters={state.filter}
          columns={columns}
          data={data?.data || []}
          fetchData={fetchData}
          pageCount={data?.pageCount}
          loading={isFetching}
          selectable={true}
          setSelectedFiles={setSelectedFiles}
        />
      </CardBody>
    </Card>
  );
};
