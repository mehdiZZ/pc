import {
  GetManyQueryFunctionContext,
  getRequest,
  GetSingleQueryFunctionContext,
  postRequest,
} from "utils";
import { Setting } from "./types";
import * as yup from "yup";

/////////////////-------- Validation
export const saveGeneralSettingsSchema = yup.object().shape({
  title: yup.string().optional(),
  description: yup.string().optional(),
  url: yup.string().url().optional(),
});

/////////////////-------- HTTP
export const getSettings = ({ queryKey }: GetManyQueryFunctionContext) => {
  const [_key, type] = queryKey;
  return getRequest<Setting[]>({
    url: _key + type,
  });
};

export const getSetting = ({ queryKey }: GetSingleQueryFunctionContext) => {
  const [_key, slug] = queryKey;
  return getRequest<Setting>({
    url: _key + slug,
  });
};

export const createSettings = async (
  data: Pick<Setting, "slug" | "type" | "data">
) => {
  return await postRequest({ data, url: "/settings" });
};

export const updateSettings = async (data: Partial<Setting>) => {
  const { id, ...others } = data;
  return await postRequest({
    data: others,
    url: `/settings/${id}`,
    method: "PATCH",
  });
};

export const deleteSettings = async (id: string) => {
  return await postRequest({ url: `/settings/${id}`, method: "DELETE" });
};
