import { yupResolver } from "@hookform/resolvers/yup";
import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Label,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import {
  getErrorMessages,
  ListedErrorMessages,
  useNotification,
  usePermission,
} from "utils";
import {
  getSetting,
  saveGeneralSettingsSchema,
  updateSettings,
} from "../utils";

export const GeneralSettings = () => {
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { canUser } = usePermission();
  const { data, isFetching, error } = useQuery(
    ["settings/", "general"],
    getSetting
  );

  const { errors, handleSubmit, register, formState, reset } = useForm({
    resolver: yupResolver(saveGeneralSettingsSchema),
  });

  const { dirtyFields } = formState;

  //
  const { mutateAsync, isLoading } = useMutation(updateSettings);

  //
  const [errorMessages, setErrors] = useState([]);

  const onSubmit = async (settingFields) => {
    if (!Object.keys(dirtyFields).length) {
      notice({
        textContent: t("crud:nothingChanged"),
        status: "error",
      });
      return;
    }

    try {
      const updateData = await mutateAsync({
        data: {
          ...settingFields,
        },
        id: data.id,
      });

      if (updateData) {
        setErrors([]);
        reset(updateData, {
          dirtyFields: true,
        });
        notice({
          textContent: t("crud:updated"),
        });
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <Card loading={isFetching || isLoading}>
      <CardBody>
        {error ? (
          <Alert hidCloseButton theme={"danger"}>
            {t("crud:notFound")}
          </Alert>
        ) : (
          <>
            <Form onSubmit={handleSubmit(onSubmit)}>
              {!isLoading && (
                <ListedErrorMessages errorMessages={errorMessages} />
              )}
              <Flex>
                <Box lg={"w-1/2"}>
                  <Label>Title</Label>
                  <Input
                    ref={register}
                    name="title"
                    defaultValue={data?.data?.title}
                    error={errors?.title?.message}
                  />
                </Box>
                <Box lg={"w-1/2"}>
                  <Label>Description</Label>
                  <Input
                    ref={register}
                    name="description"
                    defaultValue={data?.data?.description}
                    error={errors?.description?.message}
                  />
                </Box>
                <Box lg={"w-1/2"}>
                  <Label>Site Url</Label>
                  <Input
                    ref={register}
                    name="url"
                    defaultValue={data?.data?.url}
                    error={errors?.url?.message}
                  />
                </Box>
                <Box className="flex" lg={"w-1/2"}>
                  <Button
                    theme="info"
                    className="mt-auto"
                    disabled={!canUser("setting.item-update")}
                  >
                    Save
                  </Button>
                </Box>
              </Flex>
            </Form>
          </>
        )}
      </CardBody>
    </Card>
  );
};
