import {
  CheckOutlined,
  EditOutlined,
  MinusOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  Box,
  Button,
  Flex,
  Form,
  Hr,
  Input,
  InputGroup,
  Label,
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ReactSelect,
  Select,
  Typography,
} from "components";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useMutation, useQuery } from "react-query";
import { useHistory, useParams } from "react-router";
import SortableTree, {
  addNodeUnderParent,
  removeNodeAtPath,
  changeNodeAtPath,
  TreeItem,
} from "react-sortable-tree";
import "react-sortable-tree/style.css";
import {
  getErrorMessages,
  useLanguage,
  useNotification,
  usePermission,
} from "utils";
import {
  createSettings,
  getSetting,
  getSettings,
  updateSettings,
  deleteSettings,
} from "../utils";

type EditModalType = {
  isOpen?: boolean;
  path?: Array<string | number>;
  node?: TreeItem;
};

export const MenuSettings = () => {
  const { isRtl } = useLanguage();
  const { notice } = useNotification();
  const { t } = useTranslation();
  const { push } = useHistory();
  const { canUser } = usePermission();
  const { slug }: any = useParams();

  //
  const { mutateAsync, isLoading } = useMutation(createSettings);
  const {
    mutateAsync: mutateAsyncDelete,
    isLoading: deleteLoading,
  } = useMutation(deleteSettings);

  //
  const {
    mutateAsync: mutateAsyncUpdate,
    isLoading: updateLoading,
  } = useMutation(updateSettings);

  //
  const [modal, setModal] = useState<EditModalType>({
    isOpen: false,
    node: null,
    path: null,
  });

  //
  const { data, refetch, isFetching } = useQuery(
    ["settings/type/", 2],
    getSettings,
    {
      keepPreviousData: true,
    }
  );

  //
  const {
    data: singleMenu,
    isFetching: isFetchingSingle,
    refetch: singleRefetch,
  } = useQuery(["settings/", slug], getSetting, {
    enabled: false,
  });

  //
  const getNodeKey = ({ treeIndex }) => treeIndex;

  const changeSingleNode = (path, newNode) => {
    setState((oldState) =>
      changeNodeAtPath({
        treeData: oldState,
        path,
        getNodeKey,
        newNode,
      })
    );
  };

  const [state, setState] = useState<TreeItem[]>([]);

  useEffect(() => {
    setState(singleMenu?.data || []);
  }, [singleMenu?.data]);

  useEffect(() => {
    if (slug) singleRefetch();
  }, [slug]);

  return (
    <Flex className="px-4 md:pt-4 lg:pt-6">
      <Box md={"w-2/3"}>
        <Flex col className="justify-center mb-4">
          {slug && (
            <Box>
              <div className="flex justify-between">
                <Button
                  size="small"
                  onClick={() =>
                    setState((state) =>
                      state.concat({
                        title: "Change me",
                        link: "",
                        classNames: "",
                      })
                    )
                  }
                >
                  <PlusOutlined /> <span> Create a link</span>
                </Button>
                <div>
                  {canUser("setting.item-update") && (
                    <Button
                      onClick={async () => {
                        await mutateAsyncUpdate({
                          id: singleMenu.id,
                          data: state,
                        });
                        notice({ textContent: "Menu updated" });
                        refetch();
                      }}
                      loading={updateLoading}
                      className="mx-1"
                      size="small"
                      theme="success"
                    >
                      Save
                    </Button>
                  )}
                  {canUser("setting.item-delete") && (
                    <Button
                      onClick={async () => {
                        await mutateAsyncDelete(singleMenu.id);
                        notice({ textContent: "Menu deleted" });
                        push({ pathname: "/app/settings/menu" });
                        refetch();
                      }}
                      loading={deleteLoading}
                      className="mx-1"
                      size="small"
                      theme="danger"
                    >
                      Delete
                    </Button>
                  )}
                </div>
              </div>
            </Box>
          )}
          <Box>
            <div style={{ height: 400 }} className="text-black-transparent">
              <SortableTree
                rowDirection={isRtl ? "rtl" : "ltr"}
                isVirtualized={false}
                treeData={state}
                onChange={(treeData) => setState(treeData)}
                generateNodeProps={({ node, path }) => ({
                  buttons: [
                    <Button
                      onClick={() => {
                        setModal({ isOpen: true, path, node });
                      }}
                      size="small"
                      theme="primary"
                    >
                      <EditOutlined />
                    </Button>,
                    <Button
                      size="small"
                      className="mx-1"
                      theme="success"
                      onClick={() =>
                        setState(
                          (oldState) =>
                            addNodeUnderParent({
                              treeData: oldState,
                              parentKey: path[path.length - 1],
                              expandParent: true,
                              getNodeKey,
                              newNode: {
                                title: `Your title`,
                                link: "",
                                className: "",
                              },
                              addAsFirstChild: false,
                            }).treeData
                        )
                      }
                    >
                      <PlusOutlined />
                    </Button>,
                    <Button
                      size="small"
                      theme="danger"
                      onClick={() =>
                        setState((oldState) =>
                          removeNodeAtPath({
                            treeData: oldState,
                            path,
                            getNodeKey,
                          })
                        )
                      }
                    >
                      <MinusOutlined />
                    </Button>,
                  ],
                })}
              />
            </div>
            <EditModal
              modal={modal}
              setModal={setModal}
              changeSingleNode={changeSingleNode}
            />
          </Box>
        </Flex>
      </Box>
      <Box md={"w-1/3"}>
        <Flex col>
          <Box>
            <Label block for="menuId">
              Select a menu
            </Label>
            <ReactSelect
              value={{
                id: singleMenu?.slug,
                slug: singleMenu?.slug,
              }}
              options={data}
              getOptionLabel={(option: any) => option.slug}
              isLoading={isFetching || isFetchingSingle}
              onChange={(value) => {
                if (value?.slug) {
                  push({
                    pathname: "/app/settings/menu/" + value.slug,
                  });
                } else {
                  push({
                    pathname: "/app/settings/menu",
                  });
                }
              }}
            />
          </Box>
          <Box className="flex items-center">
            <Form
              className="w-full"
              onSubmit={async (e) => {
                e.preventDefault();
                const { menuId }: any = e.currentTarget.elements;
                try {
                  const data = await mutateAsync({
                    slug: menuId.value,
                    type: 2,
                    data: [
                      {
                        title: "Chicken",
                        link: "",
                        classNames: "",
                      },
                    ],
                  });
                  if (data) {
                    notice({
                      textContent: t("crud:created"),
                    });
                    refetch();
                  }
                } catch (err) {
                  notice({
                    status: "error",
                    textContent: t(getErrorMessages(err)),
                  });
                }
              }}
            >
              <Label block for="menuId">
                Create new
              </Label>
              <InputGroup block>
                <Input
                  required
                  minLength={2}
                  name="menuId"
                  id="menuId"
                  defaultValue="main-menu"
                />
                <Button
                  theme="primary"
                  loading={isLoading}
                  disabled={!canUser("setting.item-create")}
                >
                  Create
                </Button>
              </InputGroup>
            </Form>
          </Box>
        </Flex>
      </Box>
    </Flex>
  );
};

const EditModal = ({
  modal,
  setModal,
  changeSingleNode,
}: {
  modal: EditModalType;
  setModal: React.Dispatch<React.SetStateAction<EditModalType>>;
  changeSingleNode: (path: Array<string | number>, node: TreeItem) => void;
}) => {
  if (!modal.isOpen) return null;

  const { node, isOpen, path } = modal;

  const onChange = (e) => {
    changeSingleNode(path, { ...node, [e.target.name]: e.target.value });
  };

  return (
    <Modal
      onClose={() => setModal({ isOpen: false })}
      isOpen={isOpen}
      size="large"
    >
      <ModalHeader>
        <Typography tag="span" className="text-xl">
          Edit menu
        </Typography>
      </ModalHeader>
      <ModalContent>
        <Form className="lg:px-6">
          <Flex col>
            <Box>
              <Label for={"title"}>Title</Label>
              <Input
                id={"title"}
                type={"text"}
                name={"title"}
                onChange={onChange}
                placeholder={"Add title"}
                defaultValue={node.title as string}
              />
            </Box>
            <Box>
              <Label for={"link"}>Link</Label>
              <Input
                id={"link"}
                type={"text"}
                name={"link"}
                onChange={onChange}
                placeholder={"Add Link"}
                defaultValue={node.link}
              />
            </Box>
            <Box>
              <Label for={"classNames"}>ClassNames</Label>
              <Input
                id={"classNames"}
                type={"text"}
                name={"classNames"}
                onChange={onChange}
                placeholder={"Add classNames"}
                defaultValue={node.classNames}
              />
            </Box>
          </Flex>
        </Form>
      </ModalContent>
      <ModalFooter>
        <Button theme="primary" onClick={() => setModal({ isOpen: false })}>
          Close
        </Button>
      </ModalFooter>
    </Modal>
  );
};
