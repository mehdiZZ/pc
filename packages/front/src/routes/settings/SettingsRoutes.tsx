import { Redirect, Route, Switch } from "react-router-dom";
import { GeneralSettings } from "./General";
import { MenuSettings } from "./Menu";

export const SettingsRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={"/app/settings"}
        render={() => <Redirect to="/app/settings/general" />}
      />
      <Route exact path={"/app/settings/general"} component={GeneralSettings} />
      <Route
        exact
        path={"/app/settings/menu/:slug?"}
        component={MenuSettings}
      />
    </Switch>
  );
};
