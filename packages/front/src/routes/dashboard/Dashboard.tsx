import { Card, CardBody, TinyMCEEditor, ReactTable, Wizard } from "components";
import { useState } from "react";

export const Dashboard = () => {
  const [value, setValue] = useState("Default value");

  return (
    <Card>
      <CardBody>
        <TinyMCEEditor value={value} onChange={setValue} />
      </CardBody>
    </Card>
  );
};
