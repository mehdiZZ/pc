import * as yup from "yup";
import {
  AuthPasswordResetNewPasswordReq,
  AuthLoginUserReq,
  AuthPasswordResetMailTokenReq,
} from "./types";
import { postRequest } from "utils";
import { UserRegisterReq } from "@backend/core/user/dto";

/////////////////-------- Validation
export const loginSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup
    .string()
    .required()
    .min(8)
    .matches(
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      "password must contain alpha and numeric characters like: MyPassword123456"
    ),
});

export const registerSchema = yup.object().shape({
  username: yup.string().email().required(),
  password: yup
    .string()
    .required()
    .min(8)
    .matches(
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      "password must contain alpha and numeric characters like: MyPassword123456"
    ),
  confirmPassword: yup
    .string()
    .required()
    .test("confirmPassword", "please confirm your password", function (val) {
      return this.parent.password === val;
    }),
});

export const forgotPasswordSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup
    .string()
    .required()
    .min(8)
    .matches(
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      "password must contain alpha and numeric characters like: MyPassword123456"
    ),
  confirmPassword: yup
    .string()
    .required()
    .test("confirmPassword", "please confirm your password", function (val) {
      return this.parent.password === val;
    }),
});

export const forgotPasswordEmailSchema = yup.object().shape({
  email: yup.string().email().required(),
});

/////////////////-------- HTTP
export const postLogin = async (data: AuthLoginUserReq) => {
  return await postRequest({ data, url: "/auth/login" });
};

export const postRegister = async (data: UserRegisterReq) => {
  return await postRequest({ data, url: "/users/register" });
};

export const postForgotPasswordEmail = async (
  data: AuthPasswordResetMailTokenReq
) => {
  return await postRequest({ data, url: "/auth/mail-reset-token" });
};

export const postForgotPassword = async (
  data: AuthPasswordResetNewPasswordReq
) => {
  return await postRequest({ data, url: "/auth/reset-password" });
};
