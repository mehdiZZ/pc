import {
  Alert,
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Typography,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import { forgotPasswordSchema, postForgotPassword } from "./utils";
import { getErrorMessages, ListedErrorMessages } from "utils";
import { useHistory, useParams } from "react-router-dom";
import { useMutation } from "react-query";
import { AuthPasswordResetNewPasswordReq } from "./types";

export const ForgotPassword = () => {
  const {
    errors,
    handleSubmit,
    register,
  } = useForm<AuthPasswordResetNewPasswordReq>({
    resolver: yupResolver(forgotPasswordSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(postForgotPassword);
  const { replace } = useHistory();

  const { token } = useParams<{ token: string }>();

  //
  const [errorMessages, setErrors] = useState([]);

  const onSubmit = async (forgotFields: AuthPasswordResetNewPasswordReq) => {
    try {
      const data = await mutateAsync({ ...forgotFields, token });
      setErrors([]);
      if (data) replace("/login");
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <div className="flex justify-center items-center h-full">
      <Card className="w-2/3 md:w-1/4" loading={isLoading}>
        <CardBody>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Flex>
              {!isLoading ? (
                <ListedErrorMessages errorMessages={errorMessages} />
              ) : (
                <Box sm="w-full">
                  <Alert theme="info" hidCloseButton>
                    <Typography>Please set your new password</Typography>
                  </Alert>
                </Box>
              )}
              <Box sm="w-full">
                <Input
                  name="email"
                  placeholder="Email"
                  ref={register}
                  error={errors.email?.message}
                />
              </Box>
              <Box sm="w-full">
                <Input
                  name="password"
                  type="password"
                  placeholder="password"
                  ref={register}
                  error={errors.password?.message}
                />
              </Box>
              <Box sm="w-full">
                <Input
                  name="confirmPassword"
                  type="password"
                  placeholder="confirm password"
                  ref={register}
                  error={errors.confirmPassword?.message}
                />
              </Box>
              <Box className="text-center">
                <Button theme="primary">Change password</Button>
                <Link to="/login">
                  <Typography tag="small" className="px-2" color="text-gray">
                    login
                  </Typography>
                </Link>
              </Box>
            </Flex>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
};
