import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Typography,
} from "components";
import { useState } from "react";
import { useForm } from "react-hook-form";

import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import { registerSchema, postRegister } from "./utils";
import { getErrorMessages, ListedErrorMessages } from "utils";
import { useHistory } from "react-router-dom";
import { UserRegisterReq } from "./types";
import { useMutation } from "react-query";

export const Register = () => {
  const { errors, handleSubmit, register } = useForm<UserRegisterReq>({
    resolver: yupResolver(registerSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(postRegister);
  const { push } = useHistory();

  //
  const [errorMessages, setErrors] = useState([]);

  const onSubmit = async (registerFields: UserRegisterReq) => {
    try {
      const data = await mutateAsync(registerFields);
      setErrors([]);
      if (data) push("/login");
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  return (
    <div className="flex justify-center items-center h-full">
      <Card className="w-2/3 md:w-1/4" loading={isLoading}>
        <CardBody>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Flex>
              {!isLoading && (
                <ListedErrorMessages errorMessages={errorMessages} />
              )}
              <Box sm="w-full">
                <Input
                  name="email"
                  placeholder="Email"
                  type="email"
                  ref={register}
                  error={errors.email?.message}
                />
              </Box>
              <Box sm="w-full">
                <Input
                  name="password"
                  type="password"
                  placeholder="Password"
                  ref={register}
                  error={errors.password?.message}
                />
              </Box>
              <Box sm="w-full">
                <Input
                  name="confirmPassword"
                  type="password"
                  placeholder="Confirm Password"
                  ref={register}
                  error={errors.confirmPassword?.message}
                />
              </Box>
              <Box className="text-center">
                <Button theme="primary">Register</Button>
                <Link to="/login">
                  <Typography tag="small" className="px-2" color="text-gray">
                    login
                  </Typography>
                </Link>
              </Box>
            </Flex>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
};
