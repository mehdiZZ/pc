import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Typography,
} from "components";
import { useContext, useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Link, useHistory } from "react-router-dom";
import { getErrorMessages, ListedErrorMessages, RootContext } from "utils";
import { forgotPasswordEmailSchema, postForgotPasswordEmail } from "./utils";
import { AuthPasswordResetMailTokenReq } from "./types";
import { CheckOutlined } from "@ant-design/icons";
import { useMutation } from "react-query";

export const ForgotPasswordMail = () => {
  const { user } = useContext(RootContext);

  //
  const {
    errors,
    handleSubmit,
    register,
  } = useForm<AuthPasswordResetMailTokenReq>({
    resolver: yupResolver(forgotPasswordEmailSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(postForgotPasswordEmail);

  //
  const [errorMessages, setErrors] = useState([]);

  const onSubmit = async (
    forgotPasswordMailFields: AuthPasswordResetMailTokenReq
  ) => {
    try {
      const data = await mutateAsync({ email: forgotPasswordMailFields.email });
      setErrors([]);
      if (data) {
        setModal(true);
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  //
  const [modal, setModal] = useState(false);

  //
  const { replace } = useHistory();
  useEffect(() => {
    if (user) replace("./app");
  }, [replace, user]);

  return (
    <>
      <div className="flex justify-center items-center h-full">
        <Card className="w-2/3 md:w-1/4" loading={isLoading}>
          <CardBody>
            <Form onSubmit={handleSubmit(onSubmit)}>
              <Flex>
                {!isLoading && (
                  <ListedErrorMessages errorMessages={errorMessages} />
                )}
                <Box sm="w-full">
                  <Input
                    name="email"
                    placeholder="Email"
                    type="email"
                    ref={register}
                    error={errors.email?.message}
                  />
                </Box>

                <Box className="text-center">
                  <Button theme="primary">Send email</Button>
                  <Typography tag="small" className="px-2" color="text-gray">
                    <Link to="/login">Login</Link>
                  </Typography>
                </Box>
              </Flex>
            </Form>
          </CardBody>
        </Card>
      </div>
      <Modal onClose={() => setModal(false)} isOpen={modal} size="small">
        <ModalHeader>
          <Typography tag="h1">
            <CheckOutlined className="text-green" />
            we sent you a link
          </Typography>
        </ModalHeader>
        <ModalContent>
          <p className="text-center">
            Please check your email and click the link
          </p>
        </ModalContent>
        <ModalFooter>
          <Button theme="danger" onClick={() => setModal(false)}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};
