import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Form,
  Input,
  Typography,
} from "components";
import { useContext, useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { AuthLoginUserReq } from "./types";
import { useHistory } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import { loginSchema, postLogin } from "./utils";
import { getErrorMessages, ListedErrorMessages, RootContext } from "utils";
import { useMutation } from "react-query";

export const Login = () => {
  const { setUser, user } = useContext(RootContext);

  //
  const { errors, handleSubmit, register } = useForm<AuthLoginUserReq>({
    resolver: yupResolver(loginSchema),
  });

  //
  const { mutateAsync, isLoading } = useMutation(postLogin);
  const { replace } = useHistory();

  //
  const [errorMessages, setErrors] = useState([]);

  const onSubmit = async (loginFields: AuthLoginUserReq) => {
    try {
      const data = await mutateAsync(loginFields);
      if (data) {
        setErrors([]);
        setUser(data as any);
        replace("./app");
      }
    } catch (err) {
      setErrors(getErrorMessages(err));
    }
  };

  //
  useEffect(() => {
    if (user) replace("./app");
  }, [user, replace]);

  return (
    <div className="flex justify-center items-center h-full">
      <Card className="w-2/3 md:w-1/4" loading={isLoading}>
        <CardBody>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Flex>
              {!isLoading && (
                <ListedErrorMessages errorMessages={errorMessages} />
              )}
              <Box sm="w-full">
                <Input
                  name="email"
                  placeholder="Email"
                  type="email"
                  ref={register}
                  error={errors.email?.message}
                />
              </Box>
              <Box sm="w-full">
                <Input
                  name="password"
                  placeholder="password"
                  type="password"
                  ref={register}
                  error={errors.password?.message}
                />
              </Box>
              <Box className="text-center">
                <Button theme="primary">Login</Button>
                <Typography tag="small" className="px-2" color="text-gray">
                  <Link to="/register">Register</Link> /{" "}
                  <Link to="/send-forgot-mail">Forgot password</Link>
                </Typography>
              </Box>
            </Flex>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
};
