import { ThemesType, LanguageType } from "utils";
import { ReactComponent as IranSvg } from "../assets/images/flags/iran.svg";
import { ReactComponent as UkSvg } from "../assets/images/flags/united-kingdom.svg";

export const themes: ThemesType = [
  {
    name: "Blue",
    mode: "dark",
    label:
      "radial-gradient(farthest-side ellipse at 10% 0, #333867 20%, #17193B)",
    dark: {
      primary: "#1870DC",
      primaryInverse: "#f4f4f5",
      secondary: "rgba(244, 244, 244,.9)",
      secondaryInverse: "#333333",
      tertiary: "#474D84",
      tertiaryInverse: "#f4f4f5",
      mainGradient:
        "radial-gradient(farthest-side ellipse at 10% 0, #333867 20%, #17193B)",
      inverseHigher: "rgba(244,244,245,.9)",
      inverseHigh: "rgba(244,244,245,.7)",
      inverseMedium: "rgba(244,244,245,.6)",
      content: "rgba(0,0,0,.24)",
      rounded: "rounded",
    },
    light: {
      primary: "#1870DC",
      primaryInverse: "#f4f4f5",
      secondary: "rgba(244, 244, 244,.9)",
      secondaryInverse: "#333333",
      tertiary: "#474D84",
      tertiaryInverse: "#f4f4f5",
      mainGradient:
        "radial-gradient(farthest-side ellipse at 10% 0, rgb(85 43 86 / 11%) 20%, rgb(52 21 53 / 28%))",
      inverseHigher: "rgba(0,0,0,.9)",
      inverseHigh: "rgba(0,0,0,.7)",
      inverseMedium: "rgba(0,0,0,.6)",
      content: "rgba(255,255,255,.24)",
      rounded: "rounded",
    },
  },
  {
    name: "Purple",
    mode: "dark",
    label:
      "radial-gradient(farthest-side ellipse at 10% 0, #45046a 20%, #27033b)",
    dark: {
      primary: "#45046a",
      primaryInverse: "#f4f4f5",
      secondary: "rgba(244, 244, 244,.9)",
      secondaryInverse: "#333333",
      tertiary: "#474D84",
      tertiaryInverse: "#f4f4f5",
      mainGradient:
        "radial-gradient(farthest-side ellipse at 10% 0, #45046a 20%, #27033b)",
      inverseHigher: "rgba(244,244,245,.9)",
      inverseHigh: "rgba(244,244,245,.7)",
      inverseMedium: "rgba(244,244,245,.6)",
      content: "rgba(0,0,0,.24)",
      rounded: "rounded",
    },
    light: {
      primary: "#45046a",
      primaryInverse: "#f4f4f5",
      secondary: "rgba(244, 244, 244,.9)",
      secondaryInverse: "#333333",
      tertiary: "#474D84",
      tertiaryInverse: "#f4f4f5",
      mainGradient:
        "radial-gradient(farthest-side ellipse at 10% 0, rgb(85 43 86 / 11%) 20%, rgb(52 21 53 / 28%))",
      inverseHigher: "rgba(0,0,0,.9)",
      inverseHigh: "rgba(0,0,0,.7)",
      inverseMedium: "rgba(0,0,0,.6)",
      content: "rgba(255,255,255,.24)",
      rounded: "rounded",
    },
  },
];

export const langs: LanguageType[] = [
  { lang: "en", dir: "ltr", icon: UkSvg, label: "English" },
  { lang: "fa", dir: "rtl", icon: IranSvg, label: "Farsi" },
];
