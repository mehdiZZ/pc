import { JwtGuard } from '@backend/core/user/_auth';
import {
  PermissionGuard,
  PermissionsList,
} from '@backend/core/user/_permission';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiParam } from '@nestjs/swagger';
import { SettingCreateReq, SettingUpdateReq } from './dto';
import { Setting, SettingsType } from './entities';
import { SettingService } from './setting.service';

@ApiTags('/v1/settings')
@Controller('/v1/settings')
export class SettingController {
  constructor(private readonly SettingService: SettingService) {}

  @Post()
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('setting.item-create')
  async create(@Body() input: SettingCreateReq): Promise<Setting[]> {
    try {
      return await this.SettingService.create(input);
    } catch (err) {
      throw new BadRequestException('This item already exists');
    }
  }

  @Get(':slug')
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('setting.item-read')
  async get(@Param('slug') slug: string): Promise<Setting> {
    return await this.SettingService.get(slug);
  }

  @Get('type/:type')
  async getMany(@Param('type') type: SettingsType): Promise<Setting[]> {
    return await this.SettingService.getMany(type);
  }

  @Patch(':id')
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('setting.item-update')
  async update(
    @Param('id') id: string,
    @Body() menu: SettingUpdateReq,
  ): Promise<Setting> {
    return await this.SettingService.update(id, menu);
  }

  @Delete(':id')
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('setting.item-delete')
  @ApiParam({ type: String, name: 'id' })
  async delete(@Param('id') id: string): Promise<Setting[]> {
    return await this.SettingService.delete(id);
  }
}
