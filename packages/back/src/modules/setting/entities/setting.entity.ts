import { Entity, Column, Index } from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';

//
export enum SettingsType {
  General = 1,
  Menu = 2,
}
//
@Entity('site-settings')
export class Setting extends ExtendedBaseEntity {
  @Column({ type: 'varchar', name: 'slug', length: 250, unique: true })
  @Index({ unique: true })
  slug!: string;

  @Column({ type: 'int', default: 1 })
  type: SettingsType;

  @Column({ type: 'jsonb', nullable: true, default: null })
  data?: any;
}
