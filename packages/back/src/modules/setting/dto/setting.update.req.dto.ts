import { PartialType } from '@nestjs/swagger';
import { SettingCreateReq } from './setting.create.req.dto';

export class SettingUpdateReq extends PartialType(SettingCreateReq) {
  id?: string; // for frontend
}
