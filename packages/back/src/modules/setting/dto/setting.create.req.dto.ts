import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { SettingsType } from '../entities';

export class SettingCreateReq {
  @IsNotEmpty()
  @IsString()
  @MaxLength(250)
  slug!: string;

  @IsNotEmpty()
  type: SettingsType;

  @IsOptional()
  data?: any;
}
