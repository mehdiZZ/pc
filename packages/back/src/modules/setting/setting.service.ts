import { Injectable, NotFoundException } from '@nestjs/common';
import { Setting, SettingsType } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RedisService } from '@backend/core/redis/redis.service';
import { SettingCreateReq, SettingUpdateReq } from './dto';

@Injectable()
export class SettingService {
  constructor(
    @InjectRepository(Setting) private settingsRepo: Repository<Setting>,
    private readonly redisService: RedisService, //save data in redis for frontend(website only)
  ) {}

  async getMany(type?: SettingsType): Promise<Setting[]> {
    if (type)
      return await this.settingsRepo.find({
        where: { type },
      });

    return await this.settingsRepo.find();
  }

  async get(slug: string): Promise<Setting> {
    const find = await this.settingsRepo.findOne({ slug });
    if (!find) throw new NotFoundException('Settings not found');
    return find;
  }

  async create(settings: SettingCreateReq): Promise<Setting[]> {
    await this.settingsRepo.create(settings).save();
    const allSettings = await this.getMany();
    this.redisService.setSettings(allSettings);
    return allSettings;
  }

  async update(id: string, settings: SettingUpdateReq): Promise<Setting> {
    const res = await this.settingsRepo.findOne({ id });
    res.data = settings.data;
    await res.save();
    const allSettings = await this.getMany();
    this.redisService.setSettings(allSettings);
    return res;
  }

  async delete(id: string): Promise<Setting[]> {
    await this.settingsRepo.delete({ id });
    const allSettings = await this.getMany();
    this.redisService.setSettings(allSettings);
    return allSettings;
  }
}
