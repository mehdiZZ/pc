import { Tag } from './entities';
import { TagService } from './taxonomy.tag.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { TagCreateReq, TagUpdateReq } from './dto';
import { JwtGuard } from '@backend/core/user/_auth';
import {
  PermissionGuard,
  PermissionsList,
} from '@backend/core/user/_permission';

@Crud({
  model: { type: Tag },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    maxLimit: 100,
    alwaysPaginate: true,
    sort: [{ field: 'createdAt', order: 'DESC' }],
  },
  dto: { create: TagCreateReq, update: TagUpdateReq },
  routes: {
    exclude: ['createManyBase'],
    getManyBase: {
      // decorators: [
      //   UseGuards(JwtGuard, PermissionGuard),
      //   PermissionsList('page.list-read'),
      // ],
    },
    getOneBase: {
      // decorators: [
      //   UseGuards(JwtGuard, PermissionGuard),
      //   PermissionsList('page.list-read'),
      // ],
    },
    createOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('tag.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('tag.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('tag.item-delete'),
      ],
      returnDeleted: true,
    },
  },
})
@ApiTags('/v1/tags')
@Controller('/v1/tags')
export class TagController {
  constructor(public service: TagService) {}
  get base(): CrudController<Tag> {
    return this;
  }

  @Get('slug/:slug')
  // @UseGuards(JwtGuard, PermissionGuard)
  // @PermissionsList('page.item-read')
  async getBySlug(@Param('slug') slug: string) {
    return await this.service.getBySlug(slug);
  }
}
