import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './entities';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CrudRequest } from '@nestjsx/crud';

@Injectable()
export class CategoryService extends TypeOrmCrudService<Category> {
  constructor(
    @InjectRepository(Category)
    public categoryRepo: Repository<Category>,
  ) {
    super(categoryRepo);
  }

  // async getParent(parentId?: string | null): Promise<Category> | null {
  //   if (parentId) return await this.categoryRepo.findOneOrFail(parentId);

  //   return null;
  // }

  // async createOne(req: CrudRequest, dto: Category): Promise<Category> {
  //   const nextDto = {
  //     ...dto,
  //     parent: await this.getParent(dto.parentId),
  //   };

  //   return super.createOne(req, nextDto);
  // }

  // async updateOne(req: CrudRequest, dto: Category): Promise<Category> {
  //   const nextDto = {
  //     ...dto,
  //     parent: await this.getParent(dto.parentId),
  //   };

  //   return super.updateOne(req, nextDto);
  // }

  // async tree(): Promise<Category[]> {
  //   return await this.categoryRepo.findTrees();
  // }

  async getBySlug(slug: string): Promise<Category> {
    const category = await this.categoryRepo.findOne(
      { slug },
      { relations: ['children', 'parent'] },
    );

    if (!category) throw new NotFoundException('Category not found');
    // const test = await this.categoryRepo.findAncestors(category);
    // console.log(test);

    return category;
  }
}
