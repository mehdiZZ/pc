import { Entity, Column, Index } from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';

@Entity('tags')
export class Tag extends ExtendedBaseEntity {
  @Column({ type: 'varchar', length: 60, unique: true })
  @Index({ unique: true })
  name!: string;

  @Column({ type: 'varchar', length: 250, unique: true })
  @Index({ unique: true })
  slug!: string;

  @Column({ type: 'text', nullable: true })
  description?: string;
}
