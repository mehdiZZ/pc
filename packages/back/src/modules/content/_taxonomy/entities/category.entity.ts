import { Entity, Column, Index, ManyToOne, OneToMany } from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';

@Entity('categories')
export class Category extends ExtendedBaseEntity {
  @Column({ type: 'varchar', length: 200, unique: true })
  @Index({ unique: true })
  name!: string;

  @Column({ type: 'varchar', length: 200, unique: true })
  @Index({ unique: true })
  slug!: string;

  @ManyToOne(() => Category, (category) => category.children, {
    onDelete: 'SET NULL',
  })
  parent: Category;

  @OneToMany(() => Category, (category) => category.parent)
  children: Category[];

  @Column({ type: 'text', nullable: true })
  description?: string;
}
