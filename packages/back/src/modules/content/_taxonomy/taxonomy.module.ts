import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category, Tag } from './entities';
import { CategoryService } from './taxonomy.category.service';
import { CategoryController } from './taxonomy.category.controller';
import { TagService } from './taxonomy.tag.service';
import { TagController } from './taxonomy.tag.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Category, Tag])],
  providers: [CategoryService, TagService],
  controllers: [CategoryController, TagController],
  exports: [CategoryService, TagService],
})
export class TaxonomyModule {}
