import { PartialType } from '@nestjs/swagger';
import { TagCreateReq } from './tag.create.req.dto';

export class TagUpdateReq extends PartialType(TagCreateReq) {
  id?: string; // for frontend only
}
