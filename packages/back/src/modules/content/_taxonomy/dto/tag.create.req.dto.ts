import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';

export class TagCreateReq {
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  name!: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(250)
  slug!: string;

  @IsString()
  @IsOptional()
  description?: string;
}
