import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { Category } from '../entities';

export class CategoryCreateReq {
  @IsNotEmpty()
  @IsString()
  @MaxLength(200)
  name!: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(200)
  slug!: string;

  @IsOptional()
  parent: Category;

  @IsOptional()
  children: Category[];

  @IsOptional()
  @IsString()
  description?: string;
}
