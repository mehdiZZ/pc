import { PartialType } from '@nestjs/swagger';
import { CategoryCreateReq } from './category.create.req.dto';

export class CategoryUpdateReq extends PartialType(CategoryCreateReq) {
  id?: string; // for frontend only
}
