import { Injectable, NotFoundException } from '@nestjs/common';
import { Tag } from './entities';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class TagService extends TypeOrmCrudService<Tag> {
  constructor(
    @InjectRepository(Tag)
    public tagRepo: Repository<Tag>,
  ) {
    super(tagRepo);
  }

  async getBySlug(slug: string): Promise<Tag> {
    const tag = await this.tagRepo.findOne({ slug });
    if (!tag) throw new NotFoundException('Tag not found');
    return tag;
  }
}
