import { Category } from './entities';
import { CategoryService } from './taxonomy.category.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { CategoryCreateReq, CategoryUpdateReq } from './dto';
import { JwtGuard } from '@backend/core/user/_auth';
import {
  PermissionGuard,
  PermissionsList,
} from '@backend/core/user/_permission';

@Crud({
  model: { type: Category },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    maxLimit: 500,
    alwaysPaginate: true,
    join: { parent: {}, children: {} },
    sort: [{ field: 'createdAt', order: 'DESC' }],
  },
  dto: {
    create: CategoryCreateReq,
    update: CategoryUpdateReq,
  },
  routes: {
    exclude: ['createManyBase'],
    getManyBase: {
      // decorators: [
      //   UseGuards(JwtGuard, PermissionGuard),
      //   PermissionsList('category.list-read'),
      // ],
    },
    getOneBase: {
      // decorators: [
      //   UseGuards(JwtGuard, PermissionGuard),
      //   PermissionsList('category.item-read'),
      // ],
    },
    createOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('category.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('category.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('category.item-delete'),
      ],
      returnDeleted: true,
    },
  },
})
@ApiTags('/v1/categories')
@Controller('/v1/categories')
export class CategoryController {
  constructor(public service: CategoryService) {}
  get base(): CrudController<Category> {
    return this;
  }

  @Get('slug/:slug')
  // @UseGuards(JwtGuard, PermissionGuard)
  // @PermissionsList('category.item-read')
  async getBySlug(@Param('slug') slug: string) {
    return await this.service.getBySlug(slug);
  }
}
