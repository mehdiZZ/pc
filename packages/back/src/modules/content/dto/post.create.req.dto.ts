import { Media } from '@backend/core/media/entities';
import { User } from '@backend/core/user/entities';
import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { PostStatus } from '../entities';
import { Category, Tag } from '../_taxonomy/entities';

export class PostCreateReq {
  @IsNotEmpty()
  @IsString()
  @MaxLength(250)
  title!: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(250)
  slug!: string;

  @IsString()
  @IsOptional()
  content?: string;

  @IsString()
  @IsOptional()
  excerpt?: string;

  @IsOptional()
  status?: PostStatus;

  user!: User;

  @IsOptional()
  categories?: Category[];

  @IsOptional()
  tags?: Tag[];

  @IsOptional()
  thumbnail?: Media[];
}
