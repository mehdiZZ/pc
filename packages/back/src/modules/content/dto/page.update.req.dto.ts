import { PartialType } from '@nestjs/swagger';
import { PageCreateReq } from './page.create.req.dto';

export class PageUpdateReq extends PartialType(PageCreateReq) {
  id?: string;
}
