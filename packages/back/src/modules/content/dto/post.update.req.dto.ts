import { PartialType } from '@nestjs/swagger';
import { PostCreateReq } from './post.create.req.dto';

export class PostUpdateReq extends PartialType(PostCreateReq) {
  id?: string;
}
