import { Media } from '@backend/core/media/entities';
import { User } from '@backend/core/user/entities';
import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { ContactPage } from '../entities';

export class PageCreateReq {
  @IsNotEmpty()
  @IsString()
  @MaxLength(250)
  title!: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(250)
  slug!: string;

  @IsString()
  @IsOptional()
  content?: string;

  @IsString()
  @IsOptional()
  template?: string;

  user!: User;

  @IsOptional()
  metaData?: ContactPage;

  @IsOptional()
  thumbnail?: Media[];
}
