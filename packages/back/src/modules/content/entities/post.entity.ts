import {
  Entity,
  Index,
  Column,
  ManyToOne,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';
import { User } from '@backend/core/user/entities/user.entity';
import { Category, Tag } from '@backend/modules/content/_taxonomy/entities';
import { Media } from '@backend/core/media/entities';
import { Comment } from '@backend/modules/content/_comment/entities';

export enum PostStatus {
  Pending = 1,
  Published = 2,
  Disabled = 3,
}

@Entity('posts')
export class Post extends ExtendedBaseEntity {
  @Column({ type: 'varchar', length: 250 })
  title!: string;

  @Column({ type: 'varchar', length: 250, unique: true })
  @Index({ unique: true })
  slug!: string;

  @Column({ type: 'text', nullable: true, default: null })
  content?: string;

  @Column({ type: 'text', nullable: true, default: null })
  excerpt?: string;

  @Column({ type: 'int', default: 2 })
  status: PostStatus;

  @ManyToOne(() => User)
  user!: User;

  @OneToMany(() => Post, (post) => post.comments)
  comments: Comment[];

  @ManyToMany(() => Category)
  @JoinTable({
    name: 'posts_categories',
    joinColumn: {
      name: 'post_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'category_id',
      referencedColumnName: 'id',
    },
  })
  categories?: Category[];

  @ManyToMany(() => Tag)
  @JoinTable({
    name: 'posts_tag',
    joinColumn: {
      name: 'post_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'tag_id',
      referencedColumnName: 'id',
    },
  })
  tags?: Tag[];

  @ManyToMany(() => Media, (media) => media.postsThumbnails)
  @JoinTable({
    name: 'posts_thumbnails',
    joinColumn: {
      name: 'post_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'media_id',
      referencedColumnName: 'id',
    },
  })
  thumbnail?: Media[];

  @Column({ type: 'timestamptz', nullable: true })
  releasedAt?: Date; // remember to implement this
}
