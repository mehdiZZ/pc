import {
  Entity,
  Column,
  Index,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';
import { User } from '@backend/core/user/entities/user.entity';
import { Media } from '@backend/core/media/entities';
//

export type ContactPage = {
  companyName?: string;
  address?: string;
  socials?: {
    instagram?: string;
    telegram?: string;
    whatsapp?: string;
  };
  phonNumbers?: { title?: string; value?: string }[];
  location?: { lat?: number; lng?: number };
};

@Entity('pages')
export class Page extends ExtendedBaseEntity {
  @Column({ type: 'varchar', length: 250 })
  title!: string;

  @Column({ type: 'varchar', length: 250, unique: true })
  @Index({ unique: true })
  slug!: string;

  @Column({ type: 'text', nullable: true, default: null })
  content?: string;

  @Column({ type: 'varchar', length: 150, nullable: true })
  template?: string;

  @ManyToOne(() => User)
  user!: User;

  @Column({ type: 'jsonb', name: 'meta-data', nullable: true, default: null })
  metaData?: ContactPage;

  @ManyToMany(() => Media, (media) => media.pagesThumbnails)
  @JoinTable({
    name: 'pages_thumbnails',
    joinColumn: {
      name: 'page_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'media_id',
      referencedColumnName: 'id',
    },
  })
  thumbnail?: Media[];
}
