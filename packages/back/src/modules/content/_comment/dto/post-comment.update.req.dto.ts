import { PartialType } from '@nestjs/swagger';
import { PostCommentCreateReq } from './post-comment.create.req.dto';

export class PostCommentUpdateReq extends PartialType(PostCommentCreateReq) {
  id?: string; // for frontend only
}
