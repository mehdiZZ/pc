import { User } from '@backend/core/user/entities';
import { IsOptional, IsString } from 'class-validator';
import { Post } from '../../entities';
import { CommentStatus, Comment } from '../entities';

export class PostCommentCreateReq {
  @IsString()
  content: string;

  @IsOptional()
  status: CommentStatus;

  @IsOptional()
  user?: User;

  @IsOptional()
  parent?: Comment;

  @IsOptional()
  children?: Comment[];

  post: Post;
}
