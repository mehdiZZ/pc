import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from './entities';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CrudRequest } from '@nestjsx/crud';

@Injectable()
export class PostCommentService extends TypeOrmCrudService<Comment> {
  constructor(
    @InjectRepository(Comment)
    public commentPostRepo: Repository<Comment>,
  ) {
    super(commentPostRepo);
  }

  //   async getBySlug(slug: string): Promise<Category> {
  //     const category = await this.categoryRepo.findOne(
  //       { slug },
  //       { relations: ['children', 'parent'] },
  //     );

  //     if (!category) throw new NotFoundException('Category not found');
  //     // const test = await this.categoryRepo.findAncestors(category);
  //     // console.log(test);

  //     return category;
  //   }
}
