import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostCommentController } from './comment.controller';
import { PostCommentService } from './comment.service';
import { Comment } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([Comment])],
  providers: [PostCommentService],
  controllers: [PostCommentController],
  exports: [PostCommentService],
})
export class CommentModule {}
