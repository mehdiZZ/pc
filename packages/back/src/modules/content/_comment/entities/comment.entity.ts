import { ExtendedBaseEntity } from '@backend/common/base';
import { User } from '@backend/core/user/entities';
import { Post } from '@backend/modules/content/entities';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

//
export enum CommentStatus {
  Verifying = 1,
  VerifySuccess = 2,
  VerifyFail = 3,
}

@Entity({ name: 'comments' })
export class Comment extends ExtendedBaseEntity {
  @Column('varchar', { length: 2000 })
  content: string;

  @Column('int', { default: 1 })
  status: CommentStatus;

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn()
  user?: User;

  @ManyToOne(() => Comment, (comment) => comment.children, {
    onDelete: 'CASCADE',
  })
  parent?: Comment;

  @OneToMany(() => Comment, (comment) => comment.parent)
  children?: Comment[];

  @ManyToOne(() => Post, (post) => post.comments)
  @JoinColumn()
  post: Post;
}
