import { Comment } from './entities';
import { PostCommentService } from './comment.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';
import { Controller, UseGuards } from '@nestjs/common';
import { PostCommentCreateReq, PostCommentUpdateReq } from './dto';
import { JwtGuard } from '@backend/core/user/_auth';
import {
  PermissionGuard,
  PermissionsList,
} from '@backend/core/user/_permission';

@Crud({
  model: { type: Comment },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    maxLimit: 500,
    alwaysPaginate: true,
    join: {
      parent: { eager: true },
      children: { eager: true },
      post: { eager: true },
      user: { eager: true },
    },
    sort: [{ field: 'createdAt', order: 'DESC' }],
  },
  dto: { create: PostCommentCreateReq, update: PostCommentUpdateReq },
  routes: {
    exclude: ['createManyBase'],
    getManyBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('comment.list-read'),
      ],
    },
    getOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('comment.item-read'),
      ],
    },
    createOneBase: {
      decorators: [
        // UseGuards(JwtGuard, PermissionGuard),
        // PermissionsList('comment.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('comment.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('comment.item-delete'),
      ],
      returnDeleted: true,
    },
  },
})
@ApiTags('/v1/comments')
@Controller('/v1/comments')
export class PostCommentController {
  constructor(public service: PostCommentService) {} ////////////////////==============remember to fix status in create=============//////////////////////
  get base(): CrudController<Comment> {
    return this;
  }
}
