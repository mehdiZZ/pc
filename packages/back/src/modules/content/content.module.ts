import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post, Page } from './entities';
import { PostService } from './content.post.service';
import { PostController } from './content.post.controller';
import { TaxonomyModule } from '@backend/modules/content/_taxonomy/taxonomy.module';
import { PageService } from './content.page.service';
import { PageController } from './content.page.controller';
import { CommentModule } from './_comment/comment.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Post, Page]),
    TaxonomyModule,
    CommentModule,
  ],
  providers: [PostService, PageService],
  controllers: [PostController, PageController],
  exports: [PostService, PageService],
})
export class ContentModule {}
