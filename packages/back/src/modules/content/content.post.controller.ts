import { Post } from './entities';
import { PostService } from './content.post.service';

import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';
import { PostCreateReq } from './dto';
import { PostUpdateReq } from './dto/post.update.req.dto';
import { JwtGuard } from '@backend/core/user/_auth';
import {
  PermissionGuard,
  PermissionsList,
} from '@backend/core/user/_permission';

@Crud({
  model: { type: Post },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    maxLimit: 100,
    alwaysPaginate: true,
    sort: [{ field: 'createdAt', order: 'DESC' }],
    join: {
      tags: { eager: true },
      categories: { eager: true },
      user: { eager: true },
      thumbnail: { eager: true },
    },
  },
  dto: { create: PostCreateReq, update: PostUpdateReq },
  routes: {
    exclude: ['createManyBase'],
    getManyBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('post.list-read'),
      ],
    },
    getOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('post.item-read'),
      ],
    },
    createOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('post.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('post.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('post.item-delete'),
      ],
      returnDeleted: true,
    },
  },
})
@Controller('/v1/posts')
@ApiTags('/v1/posts')
export class PostController {
  constructor(public service: PostService) {}
  get base(): CrudController<Post> {
    return this;
  }

  @Get('slug/:slug')
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('post.item-read')
  async getBySlug(@Param('slug') slug: string) {
    return await this.service.getBySlug(slug);
  }
}
