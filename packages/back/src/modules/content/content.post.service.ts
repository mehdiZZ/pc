import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from './entities';
import { Repository, DeepPartial } from 'typeorm';
import { CategoryService } from '@backend/modules/content/_taxonomy/taxonomy.category.service';
import { Category } from '@backend/modules/content/_taxonomy/entities';
import { User } from '@backend/core/user/entities/user.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class PostService extends TypeOrmCrudService<Post> {
  constructor(
    @InjectRepository(Post) private postRepo: Repository<Post>,
    private readonly categoryService: CategoryService,
  ) {
    super(postRepo);
  }

  async getBySlug(slug: string): Promise<Post> {
    const page = await this.postRepo.findOne(
      { slug },
      { relations: ['user', 'thumbnail', 'tags', 'categories'] },
    );
    if (!page) throw new NotFoundException('Post not found');
    return page;
  }

  // async createPost(post: CreatePostInput, user: User): Promise<Post> {
  //   const { categoryIds = [], ...others } = post;

  //   const newPost: DeepPartial<Post> = { ...others, user };

  //   if (categoryIds.length > 0) {
  //     const relations: { categories?: Category[] } = {};
  //     const postCategories: Category[] = await this.categoryService.findByIds(
  //       categoryIds,
  //     );
  //     relations.categories = postCategories;

  //     return await this.create(newPost, relations);
  //   }
  //   return await this.create(newPost);
  // }

  // async updatePost(post: UpdatePostInput, user: User): Promise<Post> {
  //   const { categoryIds = [], id, ...others } = post;

  //   const newPost: DeepPartial<Post> = { ...others, user };

  //   if (categoryIds.length > 0) {
  //     const relations: { categories?: Category[] } = {};
  //     const postCategories: Category[] = await this.categoryService.findByIds(
  //       categoryIds,
  //     );
  //     if (postCategories) {
  //       relations.categories = postCategories;
  //       return await this.update(id, newPost, relations);
  //     } else {
  //       throw new BadRequestException();
  //     }
  //   }
  //   return await this.update(id, newPost);
  // }
}
