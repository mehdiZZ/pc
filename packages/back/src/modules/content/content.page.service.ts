import { Injectable, NotFoundException } from '@nestjs/common';
import { Page } from './entities/page.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class PageService extends TypeOrmCrudService<Page> {
  constructor(@InjectRepository(Page) private pageRepo: Repository<Page>) {
    super(pageRepo);
  }

  async getBySlug(slug: string): Promise<Page> {
    const page = await this.pageRepo.findOne(
      { slug },
      { relations: ['user', 'thumbnail'] },
    );
    if (!page) throw new NotFoundException('Page not found');
    return page;
  }
}
