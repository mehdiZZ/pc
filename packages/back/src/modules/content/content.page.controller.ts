import { Page } from './entities/page.entity';
import { PageService } from './content.page.service';
import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';
import { PageCreateReq, PageUpdateReq } from './dto';
import { JwtGuard } from '@backend/core/user/_auth';
import {
  PermissionsList,
  PermissionGuard,
} from '@backend/core/user/_permission';

@Crud({
  model: { type: Page },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    maxLimit: 100,
    alwaysPaginate: true,
    sort: [{ field: 'createdAt', order: 'DESC' }],
    join: {
      thumbnail: { eager: true },
      user: { eager: true },
    },
  },
  dto: { create: PageCreateReq, update: PageUpdateReq },
  routes: {
    exclude: ['createManyBase'],
    getManyBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('page.list-read'),
      ],
    },
    getOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('page.item-read'),
      ],
    },
    createOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('page.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('page.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('page.item-delete'),
      ],
      returnDeleted: true,
    },
  },
})
@ApiTags('/v1/pages')
@Controller('/v1/pages')
export class PageController {
  constructor(public service: PageService) {}
  get base(): CrudController<Page> {
    return this;
  }

  @Get('slug/:slug')
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('page.item-read')
  async getBySlug(@Param('slug') slug: string) {
    return await this.service.getBySlug(slug);
  }
}
