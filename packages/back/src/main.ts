import { NestFactory, Reflector } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from '@backend/common/filters/';
import i18n from 'i18n';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useStaticAssets(join(__dirname, '..', 'public'), { prefix: '/public/' });

  //
  i18n.configure({
    locales: ['en', 'fa'],
    directory: join(__dirname, 'common/i18n/locales'),
    defaultLocale: 'fa',
    autoReload: true,
    updateFiles: true,
    syncFiles: true,
    // objectNotation: true,
    logWarnFn: function (msg) {
      console.log('i18n warn', msg);
    },
    logErrorFn: function (msg) {
      console.log('i18n error', msg);
    },
    // logDebugFn: function(msg) {
    //   // console.log('debug', msg);
    // },
  });

  //
  app.use(i18n.init);

  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector))); //??

  app.useGlobalFilters(new HttpExceptionFilter());

  ///////////./////////

  // app.disable('x-powered-by');
  // app.enableCors({
  //   origin: '*',
  //   // origin: true,
  //   // credentials: true,
  //   maxAge: 0,
  //   optionsSuccessStatus: 200,
  //   // methods: '',
  //   allowedHeaders: ['Content-Type', 'Authorization', 'Accept-Language', ],
  //   exposedHeaders: ['Authorization'],
  //   // preflightContinue: false,
  // });

  ////////////////
  const configService = app.get(ConfigService);

  //
  if (process.env.NODE_ENV === 'development') {
    //swagger
    const swaggerOptions = new DocumentBuilder()
      .setTitle('🚀 Puzzle CMS 🚀')
      .setDescription('Api descriptions')
      .setVersion('1.0')
      .addBearerAuth() // remember to use this on top of your controllers @ApiBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(app, swaggerOptions);
    SwaggerModule.setup('docs', app, document);
  }
  //
  await app.listen(configService.get<number>('global.port', 15812));
}
bootstrap();
