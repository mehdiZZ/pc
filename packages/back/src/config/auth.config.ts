import { registerAs } from '@nestjs/config';

export default registerAs('auth', () => ({
  jwtTokenSecret: process.env.JWT_TOKEN_SECRET,
  jwtRefreshTokenSecret: process.env.JWT_REFRESH_TOKEN_SECRET,
  jwtExpiresIn: '20m',
  jwtRefreshTokenExpiresIn: '7d',
}));
