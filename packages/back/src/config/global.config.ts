import { registerAs } from '@nestjs/config';

// user and auth stuff
export default registerAs('global', () => ({
  port: parseInt(process.env.PORT, 10),
}));
