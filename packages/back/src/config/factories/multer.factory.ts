import {
  MulterOptionsFactory as IMulterOptionsFactory,
  MulterModuleOptions,
} from '@nestjs/platform-express';
import multer from 'multer';
import mkdirp from 'mkdirp';
import { existsSync } from 'fs';
import { randomBytes } from 'crypto';
import { extname } from 'path';
import { Injectable, BadRequestException } from '@nestjs/common';

const getDirImage = () => {
  const year = new Date().getFullYear();
  const month = new Date().getMonth() + 1;
  const day = new Date().getDate();
  return `./${process.env.UPLOAD_DIR}${year}/${month}/${day}`;
};

@Injectable()
export class MulterOptionsFactory implements IMulterOptionsFactory {
  createMulterOptions(): MulterModuleOptions {
    return {
      limits: {
        fileSize: +process.env.MAX_FILE_SIZE * 1024 * 1024,
      },
      fileFilter: (
        _: Express.Request,
        file: Express.Multer.File,
        cb: (error: Error, acceptFile: boolean) => void,
      ): void => {
        const types = /jpg|jpeg|png|gif|webp|svg|pdf|msword|zip|x-7z-compressed|text|mp4|mp3/;
        const ext = types.test(extname(file.originalname).toLowerCase());
        if (types.test(file.mimetype) && ext) {
          // Allow storage of file
          return cb(null, true);
        } else {
          // Reject file
          return cb(
            new BadRequestException(
              `Unsupported file type ${extname(file.originalname)}`,
            ),
            false,
          );
        }
      },
      storage: multer.diskStorage({
        destination: (_, __, cb) => {
          const dir = getDirImage();

          mkdirp(dir).then(() => cb(null, dir));
        },
        filename: (_, file, cb) => {
          const filePath = getDirImage() + '/' + file.originalname;
          if (!existsSync(filePath)) {
            cb(null, file.originalname);
          } else {
            const randomName = randomBytes(8).toString('hex');
            cb(null, randomName + '-' + file.originalname);
          }
        },
      }),
    };
  }
}
