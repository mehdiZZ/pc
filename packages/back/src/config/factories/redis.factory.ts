import { RedisOptionsFactory as IRedisOptionsFactory } from '@backend/core/redis/interfaces';
import { RedisOptions } from 'ioredis';

export class RedisOptionsFactory implements IRedisOptionsFactory {
  createRedisOptions(): RedisOptions {
    return {
      host: process.env.REDIS_HOST,
      port: parseInt(process.env.REDIS_PORT),
      password: process.env.REDIS_PASSWORD,
      keyPrefix: process.env.REDIS_PREFIX,
    };
  }
}
