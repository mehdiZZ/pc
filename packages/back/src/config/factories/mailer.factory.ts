import {
  MailerOptionsFactory as IMailerOptionsFactory,
  MailerOptions,
} from '@backend/core/mailer/interfaces';

export class MailerOptionsFactory implements IMailerOptionsFactory {
  crateMailerOptions(): MailerOptions {
    return {
      host: process.env.MAIL_HOST,
      port: parseInt(process.env.MAIL_PORT, 10),
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD,
      },
      from: process.env.MAIL_FROM,
    };
  }
}
