import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';
// import { ValidationError } from 'class-validator';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const isHttpException = exception instanceof HttpException;

    let status = isHttpException
      ? exception.getStatus()
      : HttpStatus.INTERNAL_SERVER_ERROR;

    let messages: string[] = [];
    if (isHttpException) {
      const res: any = exception.getResponse();
      //
      // if (res.message) {
      //   messages = [response.__('errors.validations.' + res.message)];
      // } else {
      //   messages = res.map((m: { key: string; property: string }) => {
      //     return response.__(m.key, m.property);
      //   });
      // }

      if (typeof res.message === 'string') {
        messages = [response.__(res.message)];
      } else {
        messages = res.message.map((m: string) => {
          return response.__(m);
        });
      }
    } else {
      if (exception?.code) {
        switch (exception.code) {
          case '23505':
            messages = [response.__('Entity already exists'), exception.detail];
            status = 400;
          case '23503':
            messages = [
              response.__('This item is using somewhere else'),
              exception.detail,
            ];
            status = 400;
        }
      } else {
        console.log(exception);
        messages = ['Internal Server Error.'];
      }
    }

    response.status(status).json({
      statusCode: status,
      messages: [...messages],
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
