// import {
//   Injectable,
//   NestInterceptor,
//   ExecutionContext,
//   CallHandler,
//   Logger,
// } from "@nestjs/common";
// import { Observable } from "rxjs";
// import { tap } from "rxjs/operators";
// @Injectable()
// export class LoggingInterceptor implements NestInterceptor {
//   intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
//     const ctx = context.switchToHttp();
//     const request = ctx.getRequest();
//     const now = Date.now();

//     if (request) {
//       return next.handle().pipe(
//         tap(() => {
//           Logger.log(
//             `${request.method} ${request.url} ${Date.now() - now}ms`,
//             context.getClass().name,
//           );
//         }),
//       );
//     } else {
//       const ctx2: any = GqlExecutionContext.create(context);
//       const resolverName = ctx2.constructorRef.name;
//       const info = ctx2.getInfo();
//       const req: any = ctx2.getContext().req;

//       return next.handle().pipe(
//         tap(() => {
//           Logger.log(
//             `[${info.parentType} to ${info.fieldName}] [Data ${JSON.parse(
//               JSON.stringify(req.body.query)
//                 .replace(/\\n/g, "")
//                 .replace(/\s\s+/g, " "),
//             )}] [In ${Date.now() - now}ms]`,
//             resolverName,
//           );
//         }),
//       );
//     }
//   }
// }
