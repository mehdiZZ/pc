import { User } from '@backend/core/user/entities';
import { Request } from 'express';

export interface IRequest extends Request {
  user?: User;
}
