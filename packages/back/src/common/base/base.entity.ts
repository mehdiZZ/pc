import {
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
} from 'typeorm';
import { CrudValidationGroups } from '@nestjsx/crud';
import { IsEmpty } from 'class-validator';
const { CREATE } = CrudValidationGroups;
// @Entity()
export class ExtendedBaseEntity extends BaseEntity {
  @IsEmpty({ groups: [CREATE] })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ nullable: true, name: 'created_at' })
  createdAt?: Date;

  @UpdateDateColumn({ nullable: true, name: 'updated_at' })
  updatedAt?: Date;

  // @DeleteDateColumn({ nullable: true })
  // deletedAt?: Date;
}
