// // import { Injectable, NestMiddleware } from '@nestjs/common';
// import i18next from 'i18next';
// import i18nextHttpMiddleware from 'i18next-http-middleware';
// import Backend from 'i18next-node-fs-backend';
// // import { en, fa } from '@backend/common/i18n/locales';
// import { resolve } from 'path';

// i18next
//   .use(Backend)
//   .use(i18nextHttpMiddleware.LanguageDetector)
//   .init({
//     whitelist: ['fa', 'en'],
//     backend: {
//       loadPath: resolve(__dirname, '../i18n/locales/{{lng}}/{{ns}}.json'),
//     },
//     fallbackLng: 'fa',
//     preload: ['fa', 'en'],
//     // lng: 'fa',
//     saveMissing: true,
//     debug: false,
//     // debug: true,
//     ns: ['common', 'errors'],
//     defaultNS: 'common',
//   });

// export const I18nextMiddleware = i18nextHttpMiddleware.handle(i18next);
