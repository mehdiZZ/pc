import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from '@backend/core/user/_role/entities/role.entity';
import { Permission } from '@backend/core/user/_permission/entities/permission.entity';
import { User } from '@backend/core/user/entities/user.entity';
import { PermissionModule } from '@backend/core/user/_permission/permission.module';
import { RoleModule } from '@backend/core/user/_role/role.module';
import { SeedService } from './seed.service';
import { SettingModule } from '@backend/modules/setting/setting.module';
import { ContentModule } from '@backend/modules/content/content.module';
import { TaxonomyModule } from '@backend/modules/content/_taxonomy/taxonomy.module';
import { CommentModule } from '@backend/modules/content/_comment/comment.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Role, Permission, User]),
    PermissionModule,
    RoleModule,
    ContentModule,
    TaxonomyModule,
    CommentModule,
    SettingModule,
  ],
  providers: [SeedService],
  exports: [SeedService],
})
export class SeedModule {}
