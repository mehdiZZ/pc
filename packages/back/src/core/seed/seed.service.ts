import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@backend/core/user/entities/user.entity';
import { Repository } from 'typeorm';
import { Permission } from '@backend/core/user/_permission/entities/permission.entity';
import { Role } from '@backend/core/user/_role/entities/role.entity';
import { RoleService } from '@backend/core/user/_role/role.service';
import { UserService } from '@backend/core/user/user.service';
import { PermissionService } from '@backend/core/user/_permission/permission.service';
import {
  permissionsSeed,
  rolesSeed,
  usersSeed,
  settingSeed,
  tagSeed,
  categorySeed,
  pageSeed,
  postSeed,
  commentSeed,
} from './seed.data';

import { req } from './seed.mock';
import { SettingService } from '@backend/modules/setting/setting.service';
import { TagService } from '@backend/modules/content/_taxonomy/taxonomy.tag.service';
import { CategoryService } from '@backend/modules/content/_taxonomy/taxonomy.category.service';
import { PostService } from '@backend/modules/content/content.post.service';
import { PageService } from '@backend/modules/content/content.page.service';
import { PostCommentService } from '@backend/modules/content/_comment/comment.service';

@Injectable()
export class SeedService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Permission)
    private readonly permissionRepo: Repository<Permission>,
    @InjectRepository(Role) private readonly roleRepo: Repository<Role>,
    private readonly roleService: RoleService,
    private readonly userService: UserService,
    private readonly permissionService: PermissionService,
    private readonly settingService: SettingService,
    private readonly tagService: TagService,
    private readonly categoryService: CategoryService,
    private readonly postService: PostService,
    private readonly pageService: PageService,
    private readonly commentService: PostCommentService,
  ) {}

  getRandomNumber(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  async insertPermissionsList(): Promise<void> {
    for (const i of permissionsSeed) {
      await this.permissionService.createOne(req, i);
    }
  }

  async insertRoles(): Promise<void> {
    const permissions = await this.permissionService.find();

    for (const i of rolesSeed) {
      await this.roleService.createOne(req, {
        ...i,
        permissions:
          i.slug === 'admin'
            ? permissions
            : permissions.filter((permission) =>
                permission.slug.includes(i.slug.replace('-manager', '')),
              ),
      });
    }
  }

  async insertUsers(): Promise<void> {
    const roles = await this.roleService.find();
    for (const i of usersSeed) {
      const randRol = this.getRandomNumber(roles.length);
      await this.userService.createOne(req, {
        ...i,
        password: await this.userService.hashPassword(i.password),
        roles: roles.filter((role, i) => i - 1 <= randRol),
      });
    }
  }

  async insertTags(): Promise<void> {
    for (const i of tagSeed) {
      await this.tagService.createOne(req, i);
    }
  }

  async insertCategories(): Promise<void> {
    for (const i of categorySeed) {
      await this.categoryService.createOne(req, i);
    }
  }

  async insertPages(): Promise<void> {
    const users = await this.userService.find();
    for (const i of pageSeed) {
      const randUser = this.getRandomNumber(users.length);
      await this.pageService.createOne(req, {
        ...i,
        user: users.find((user, i) => i === randUser),
      });
    }
  }

  async insertPostComments(): Promise<void> {
    const users = await this.userService.find();
    const posts = await this.postService.find();
    for (const i of commentSeed) {
      const randUser = this.getRandomNumber(users.length);
      const randPost = this.getRandomNumber(posts.length);
      await this.commentService.createOne(req, {
        ...i,
        user: users.find((user, i) => i === randUser),
        post: posts.find((post, i) => i === randPost),
      });
    }
  }

  async insertPosts(): Promise<void> {
    const tags = await this.tagService.find();
    const categories = await this.categoryService.find();
    const comments = await this.commentService.find();
    const users = await this.userService.find();
    for (const i of postSeed) {
      const randTag = this.getRandomNumber(tags.length);
      const randCat = this.getRandomNumber(categories.length);
      const randComment = this.getRandomNumber(comments.length);
      const randUser = this.getRandomNumber(users.length);

      await this.postService.createOne(req, {
        ...i,
        tags: tags.filter((tag, i) => i - 1 <= randTag),
        categories: categories.filter((category, i) => i - 1 <= randCat),
        comments: comments.filter((comment, i) => i - 1 <= randComment),
        user: users.find((user, i) => i === randUser),
      });
    }
  }

  async insertSettings(): Promise<void> {
    for (const i of settingSeed) {
      await this.settingService.create(i);
    }
  }
}
