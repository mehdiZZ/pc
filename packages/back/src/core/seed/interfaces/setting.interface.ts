import { SettingsType } from '@backend/modules/setting/entities';

export interface ISetting {
  slug: string;
  type: SettingsType;
}
