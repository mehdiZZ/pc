import { User } from '@backend/core/user/entities';
import { Comment } from '@backend/modules/content/_comment/entities';
import { Category, Tag } from '@backend/modules/content/_taxonomy/entities';

export interface IPost {
  title: string;
  slug: string;
  content: string;
  status: number;
  user: Partial<User>;
  comments?: Partial<Comment>[];
  tags?: Partial<Tag>[];
  categories?: Partial<Category>[];
}
