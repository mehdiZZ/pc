export * from './permission-slugs.interface';
export * from './category.interface';
export * from './tag.interface';
export * from './post.interface';
export * from './comment.interface';
export * from './setting.interface';
export * from './page.interface';
