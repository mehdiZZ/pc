import { User } from '@backend/core/user/entities';

export interface IPage {
  title: string;
  slug: string;
  content: string;
  user: Partial<User>;
}
