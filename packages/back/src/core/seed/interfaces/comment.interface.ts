import { User } from '@backend/core/user/entities';
import { Post } from '@backend/modules/content/entities';
import { CommentStatus } from '@backend/modules/content/_comment/entities';

export interface IComment {
  content: string;
  status: CommentStatus;
  user?: Partial<User>;
  post: Partial<Post>;
}
