declare const permissionSlug: [
  'user.list-read',
  //
  'user.item-read',
  'user.item-create',
  'user.item-update',
  'user.item-delete',
  // --------------------
  'role.list-read',
  //
  'role.item-read',
  'role.item-create',
  'role.item-update',
  'role.item-delete',
  // --------------------
  'permission.list-read',
  //
  'permission.item-read',
  'permission.item-create',
  'permission.item-update',
  'permission.item-delete',
  // --------------------
  'category.list-read',
  //
  'category.item-read',
  'category.item-create',
  'category.item-update',
  'category.item-delete',
  // --------------------
  'post.list-read',
  //
  'post.item-read',
  'post.item-create',
  'post.item-update',
  'post.item-delete',
  // --------------------
  'page.list-read',
  //
  'page.item-read',
  'page.item-create',
  'page.item-update',
  'page.item-delete',
  // --------------------
  'tag.list-read',
  //
  'tag.item-read',
  'tag.item-create',
  'tag.item-update',
  'tag.item-delete',
  // --------------------
  'comment.list-read',
  //
  'comment.item-read',
  'comment.item-create',
  'comment.item-update',
  'comment.item-delete',
  // --------------------
  'media.list-read',
  //
  'media.item-create',
  'media.item-delete',
  // --------------------
  'setting.item-read',
  'setting.item-update',
  'setting.item-create',
  'setting.item-delete',

  // --------------------
  'product.list-read',
  //
  'product.item-read',
  'product.item-create',
  'product.item-update',
  'product.item-delete',
  // --------------------
];

export declare type IPermissionSlug = typeof permissionSlug[number];
