import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from '../../app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SeedService } from './seed.service';

(async () => {
  const logger = new Logger('Seed-Log');
  logger.log('Start seeding... 🚀');
  //
  const app: NestExpressApplication = await NestFactory.create(AppModule);
  const seedService: SeedService = await app.get(SeedService);
  //
  await seedService.insertPermissionsList();
  await seedService.insertRoles();
  await seedService.insertUsers();

  // -----------
  await seedService.insertTags();
  await seedService.insertCategories();
  await seedService.insertPosts();
  await seedService.insertPostComments();
  // -----------
  await seedService.insertPages();
  // -----------
  await seedService.insertSettings();
  // -----------
})();
