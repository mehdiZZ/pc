import {
  IPermissionSlug,
  ICategory,
  ITag,
  IPost,
  IPage,
  IComment,
  ISetting,
} from '@backend/core/seed/interfaces';

export const permissionsSeed: { name: string; slug: IPermissionSlug }[] = [
  //
  // --------------------------------
  //
  { name: 'User List Read', slug: 'user.list-read' },
  //
  { name: 'User Item Read', slug: 'user.item-read' },
  { name: 'User Item Create', slug: 'user.item-create' },
  { name: 'User Item Update', slug: 'user.item-update' },
  { name: 'User Item Delete', slug: 'user.item-delete' },
  //
  // --------------------------------
  //
  { name: 'Role List Read', slug: 'role.list-read' },
  //
  { name: 'Role Item Read', slug: 'role.item-read' },
  { name: 'Role Item Create', slug: 'role.item-create' },
  { name: 'Role Item Update', slug: 'role.item-update' },
  { name: 'Role Item Delete', slug: 'role.item-delete' },
  //
  // --------------------------------
  //
  { name: 'Permission List Read', slug: 'permission.list-read' },
  //
  { name: 'Permission Item Read', slug: 'permission.item-read' },
  { name: 'Permission Item Create', slug: 'permission.item-create' },
  { name: 'Permission Item Update', slug: 'permission.item-update' },
  { name: 'Permission Item Delete', slug: 'permission.item-delete' },
  //
  // --------------------------------
  //
  { name: 'Post List Read', slug: 'post.list-read' },
  //
  { name: 'Post Item Read', slug: 'post.item-read' },
  { name: 'Post Item Create', slug: 'post.item-create' },
  { name: 'Post Item Update', slug: 'post.item-update' },
  { name: 'Post Item Delete', slug: 'post.item-delete' },
  //
  //
  { name: 'PostComments List Read', slug: 'comment.list-read' },
  //
  { name: 'PostComments Item Read', slug: 'comment.item-read' },
  { name: 'PostComments Item Create', slug: 'comment.item-create' },
  { name: 'PostComments Item Update', slug: 'comment.item-update' },
  { name: 'PostComments Item Delete', slug: 'comment.item-delete' },
  //
  // --------------------------------
  // --------------------------------
  //
  { name: 'Page List Read', slug: 'page.list-read' },
  //
  { name: 'Page Item Read', slug: 'page.item-read' },
  { name: 'Page Item Create', slug: 'page.item-create' },
  { name: 'Page Item Update', slug: 'page.item-update' },
  { name: 'Page Item Delete', slug: 'page.item-delete' },
  //
  // --------------------------------
  //
  { name: 'Tag List Read', slug: 'tag.list-read' },
  //
  { name: 'Tag Item Read', slug: 'tag.item-read' },
  { name: 'Tag Item Create', slug: 'tag.item-create' },
  { name: 'Tag Item Update', slug: 'tag.item-update' },
  { name: 'Tag Item Delete', slug: 'tag.item-delete' },
  //
  // --------------------------------
  //
  { name: 'Category List Read', slug: 'category.list-read' },
  //
  { name: 'Category Item Read', slug: 'category.item-read' },
  { name: 'Category Item Create', slug: 'category.item-create' },
  { name: 'Category Item Update', slug: 'category.item-update' },
  { name: 'Category Item Delete', slug: 'category.item-delete' },
  //
  // --------------------------------
  //
  { name: 'Media List Read', slug: 'media.list-read' },
  //
  { name: 'Media Item Create', slug: 'media.item-create' },
  { name: 'Media Item Delete', slug: 'media.item-delete' },
  //
  // --------------------------------
  //
  { name: 'Setting Read', slug: 'setting.item-read' },
  { name: 'Setting Create', slug: 'setting.item-create' },
  { name: 'Setting Update', slug: 'setting.item-update' },
  { name: 'Setting Delete', slug: 'setting.item-delete' },
  //
  //
  // --------------------------------
  //
  { name: 'Product List Read', slug: 'product.list-read' },
  //
  { name: 'Product Item Read', slug: 'product.item-read' },
  { name: 'Product Item Create', slug: 'product.item-create' },
  { name: 'Product Item Update', slug: 'product.item-update' },
  { name: 'Product Item Delete', slug: 'product.item-delete' },
];

export const rolesSeed = [
  { name: 'Admin', slug: 'admin' },
  { name: 'User', slug: 'user-manager' },
  { name: 'Media Manager', slug: 'media-manager' },
];

export const usersSeed = [
  {
    username: 'mehdi.zeynali69@gmail.com',
    email: 'mehdi.zeynali69@gmail.com',
    password: 'Mehdi15812',
    confirmPassword: 'Mehdi15812',
    isActive: true,
  },
  {
    username: 'test.zeynali69@gmail.com',
    email: 'test.zeynali69@gmail.com',
    password: 'Mehdi15812',
    confirmPassword: 'Mehdi15812',
    isActive: true,
  },
  {
    username: 'yo.zeynali69@gmail.com',
    email: 'yo.zeynali69@gmail.com',
    password: 'Mehdi15812',
    confirmPassword: 'Mehdi15812',
    isActive: false,
  },
];

export const categorySeed: ICategory[] = [
  { name: 'Frontend', slug: 'frontend' },
  { name: 'Backend', slug: 'backend' },
  { name: 'Help', slug: 'help' },
  { name: 'Database', slug: 'database' },
  { name: 'Design', slug: 'design' },
  { name: 'Beginner', slug: 'beginner' },
];

export const tagSeed: ITag[] = [
  { name: 'CSS animation', slug: 'css-animation' },
  { name: 'Snippets', slug: 'snippets' },
  { name: 'SVG', slug: 'svg' },
  { name: 'Canvas', slug: 'canvas' },
  { name: 'Best practices', slug: 'best-practices' },
];

export const postSeed: IPost[] = [
  {
    title: 'First Post',
    slug: 'first-post',
    categories: [{ slug: 'nothing' }],
    status: 1,
    user: { username: 'nothing' },
    content: 'Test Content',
  },
  {
    title: 'Second Post',
    slug: 'second-post',
    categories: [{ slug: 'nothing' }],
    status: 2,
    user: { username: 'nothing' },
    content: 'Test Content 2',
  },
  {
    title: 'Third Post',
    slug: 'third-post',
    categories: [{ slug: 'nothing' }],
    status: 3,
    user: { username: 'nothing' },
    content: 'Test Content 3',
  },
];

export const pageSeed: IPage[] = [
  {
    title: 'First Page',
    slug: 'first-page',
    content: 'Test Content',
    user: { username: 'nothing' },
  },
  {
    title: 'Second Page',
    slug: 'second-page',
    content: 'Test Content 2',
    user: { username: 'nothing' },
  },
];

export const commentSeed: IComment[] = [
  {
    post: { slug: 'nothing' },
    content: 'My comment content',
    status: 1,
    user: { username: 'nothing' },
  },
  {
    post: { slug: 'nothing' },
    content: 'My comment content 2',
    status: 2,
    user: { username: 'nothing' },
  },
  {
    post: { slug: 'nothing' },
    content: 'My comment content 3',
    status: 3,
    user: { username: 'nothing' },
  },
];

export const settingSeed: ISetting[] = [
  {
    slug: 'general',
    type: 1,
  },
  {
    slug: 'main-menu',
    type: 2,
  },
];
