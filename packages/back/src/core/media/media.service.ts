import { Media, CroppedMedia } from './entities';
import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import sharp from 'sharp';
import { dirname, join, extname } from 'path';
import { unlinkSync } from 'fs';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { imageSize } from 'image-size';
import { promisify } from 'util';
const sizeOf = promisify(imageSize);

@Injectable()
export class MediaService extends TypeOrmCrudService<Media> {
  constructor(
    @InjectRepository(Media) private readonly mediaRepo: Repository<Media>,
    @InjectRepository(CroppedMedia)
    private readonly croppedMediaRepo: Repository<CroppedMedia>,
  ) {
    super(mediaRepo);
  }
  private readonly imageTypes = [
    'image/bmp',
    'image/jpeg',
    'image/png',
    'image/webp',
  ];

  async upload(files: Express.Multer.File[]): Promise<Media[]> {
    if (!files || !files.length)
      throw new BadRequestException('Please upload some files');

    const uploadedFileIds = files.map(async (file) => {
      //

      const media = await this.mediaRepo
        .create({
          fileName: file.filename,
          path: file.path,
          size: Number((file.size / (1024 * 1024)).toFixed(2)),
          mimeType: file.mimetype, // todo add width and height
        })
        .save();

      //if it's is an image, crop it
      if (this.imageTypes.includes(media.mimeType)) {
        // get dimensions
        // imageSize(file.path, (err, dimensions) => {
        //   if (err) throw new InternalServerErrorException('Invalid image type');
        const dimensions = await sizeOf(file.path);

        media.width = dimensions.width;
        media.height = dimensions.height;
        await media.save();

        [
          { width: 1200, label: 'large' },
          { width: 600, label: 'medium' },
          { width: 250, label: 'thumbnail' },
        ].forEach(async (fileSize) => {
          if (dimensions.width <= fileSize.width) return;

          //
          const fileName = `${media.fileName.replace(/\.[^/.]+$/, '')}-${
            fileSize.width
          }${extname(media.fileName)}`;

          //
          const filePath = join(dirname(media.path), fileName);

          //
          await sharp(media.path)
            .resize(fileSize.width, null, {
              withoutEnlargement: true,
            })
            .toFile(filePath, (err) => {
              if (err) {
                throw new InternalServerErrorException(
                  `Can not crop ${media.fileName}`,
                );
              }
            });

          //
          await this.croppedMediaRepo
            .create({
              path: filePath,
              fileName,
              sizeLabel: fileSize.label,
              media,
            })
            .save();
        });
        // });
      }

      return media;
    });

    return Promise.all(uploadedFileIds).then(async (media) => {
      return media;
    });
  }

  async deleteFiles(ids: string[]): Promise<Media[]> {
    const files = await this.mediaRepo.findByIds(ids, {
      relations: ['cropped'],
    });
    if (files && files.length) {
      try {
        const deletedFiles = files.map(async (file) => {
          const deleted = await file.remove();
          await unlinkSync(file.path);
          if (file.cropped && file.cropped.length) {
            file.cropped.forEach(async (cropped) => {
              await unlinkSync(cropped.path);
              await cropped.remove();
            });
          }
          return deleted;
        });

        return Promise.all(deletedFiles).then(async (media) => {
          return media;
        });
      } catch {
        throw new InternalServerErrorException('Can not delete your file.');
      }
    } else {
      throw new NotFoundException('Files not found');
    }
  }
}
