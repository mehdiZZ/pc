import { Entity, Column, OneToMany, ManyToMany } from 'typeorm';
import { CroppedMedia } from './cropped-media.entity';
import { ExtendedBaseEntity } from '@backend/common/base';
import {
  IsString,
  IsNumber,
  IsOptional,
  IsNotEmpty,
  MaxLength,
} from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { User } from '@backend/core/user/entities';
import { Page, Post } from '@backend/modules/content/entities';
const { CREATE, UPDATE } = CrudValidationGroups;

@Entity('media')
export class Media extends ExtendedBaseEntity {
  @IsOptional({ groups: [UPDATE] })
  @IsNotEmpty({ groups: [CREATE] })
  @IsString({ always: true })
  @MaxLength(300, { always: true })
  @Column({ type: 'varchar', length: 300, name: 'file_name' })
  fileName!: string;

  @IsOptional({ groups: [UPDATE] })
  @IsNotEmpty({ groups: [CREATE] })
  @IsString({ always: true })
  @Column({ type: 'varchar' })
  path!: string;

  @IsOptional({ always: true })
  @IsString({ always: true })
  @Column({ type: 'varchar', name: 'mime_type' })
  mimeType!: string;

  @IsOptional({ always: true })
  @IsNumber({}, { always: true })
  @Column({
    type: 'float',
    nullable: true,
    comment: 'in megabyte',
  })
  size?: number;

  @IsOptional({ always: true })
  @IsNumber({}, { always: true })
  @Column({
    type: 'float',
    nullable: true,
  })
  width?: number;

  @IsOptional({ always: true })
  @IsNumber({}, { always: true })
  @Column({
    type: 'float',
    nullable: true,
  })
  height?: number;

  @OneToMany(() => CroppedMedia, (cropped_media) => cropped_media.media)
  cropped?: CroppedMedia[];

  //users
  @ManyToMany(() => User, (user) => user.picture, { onDelete: 'SET NULL' })
  users?: User[];

  //posts
  @ManyToMany(() => Post, (post) => post.thumbnail, { onDelete: 'SET NULL' })
  postsThumbnails?: Post[];

  //pages
  @ManyToMany(() => Page, (page) => page.thumbnail, { onDelete: 'SET NULL' })
  pagesThumbnails?: Page[];
}
