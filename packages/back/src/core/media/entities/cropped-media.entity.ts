import { Entity, Column, ManyToOne } from 'typeorm';
import { Media } from './media.entity';
import { ExtendedBaseEntity } from '@backend/common/base';
import { CrudValidationGroups } from '@nestjsx/crud';
import {
  IsOptional,
  IsNotEmpty,
  IsString,
  MaxLength,
  IsNumber,
} from 'class-validator';
const { CREATE, UPDATE } = CrudValidationGroups;

@Entity('cropped_media')
export class CroppedMedia extends ExtendedBaseEntity {
  @IsOptional({ groups: [UPDATE] })
  @IsNotEmpty({ groups: [CREATE] })
  @IsString({ always: true })
  @MaxLength(300, { always: true })
  @Column({ type: 'varchar', length: 300, name: 'file_name' })
  fileName!: string;

  @IsOptional({ groups: [UPDATE] })
  @IsNotEmpty({ groups: [CREATE] })
  @IsString({ always: true })
  @Column({ type: 'varchar' })
  path!: string;

  @IsOptional({ always: true })
  @IsString({ always: true })
  @Column({ type: 'varchar', name: 'size_label' })
  sizeLabel!: string;

  @ManyToOne(() => Media, (media) => media.cropped, { onDelete: 'CASCADE' })
  media!: Media;
}
