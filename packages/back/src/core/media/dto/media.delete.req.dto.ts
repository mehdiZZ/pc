import { IsUUID, ArrayMinSize } from 'class-validator';

export class DeleteFilesReq {
  @IsUUID(4, {
    each: true,
  })
  @ArrayMinSize(1)
  ids: string[];
}
