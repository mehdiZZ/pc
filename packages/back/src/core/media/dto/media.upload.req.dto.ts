import { ApiProperty } from '@nestjs/swagger';

export class UploadFilesReq {
  @ApiProperty({ type: 'array', items: { type: 'string', format: 'binary' } })
  files: any[];
}
