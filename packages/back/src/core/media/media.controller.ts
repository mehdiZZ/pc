import {
  Post,
  UseInterceptors,
  UploadedFiles,
  Controller,
  Body,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { MediaService } from './media.service';
import { Media } from './entities';
import { Crud, CrudController } from '@nestjsx/crud';
import {
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DeleteFilesReq, UploadFilesReq } from './dto';
import { PermissionGuard, PermissionsList } from '../user/_permission';
import { JwtGuard } from '../user/_auth';

@Crud({
  model: { type: Media },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    maxLimit: 100,
    alwaysPaginate: true,
    sort: [{ field: 'createdAt', order: 'DESC' }],
    join: {
      cropped: { eager: true },
    },
  },
  routes: {
    exclude: ['createManyBase', 'createOneBase', 'deleteOneBase'],
    getManyBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('media.list-read'),
      ],
    },
  },
})
@ApiTags('/v1/media')
@Controller('/v1/media')
export class MediaController implements CrudController<Media> {
  constructor(public readonly service: MediaService) {}

  @Post('upload')
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('media.item-create')
  @ApiCreatedResponse()
  @UseInterceptors(FilesInterceptor('files', 25))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadFilesReq,
  })
  uploadFiles(@UploadedFiles() files: Express.Multer.File[]): Promise<Media[]> {
    return this.service.upload(files);
  }

  @Delete('deleteFiles')
  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('media.item-delete')
  @ApiOkResponse()
  deleteFiles(@Body() body: DeleteFilesReq): Promise<Media[]> {
    return this.service.deleteFiles(body.ids);
  }
}
