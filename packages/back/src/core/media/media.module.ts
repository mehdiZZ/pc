import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Media } from './entities/media.entity';
import { CroppedMedia } from './entities/cropped-media.entity';
import { MediaController } from './media.controller';
import { MediaService } from './media.service';
import { MulterModule } from '@nestjs/platform-express';
import { MulterOptionsFactory } from '@backend/config/factories';

@Module({
  imports: [
    TypeOrmModule.forFeature([Media, CroppedMedia]),
    MulterModule.registerAsync({
      useClass: MulterOptionsFactory,
    }),
  ],
  providers: [MediaService],
  controllers: [MediaController],
  exports: [MediaService],
})
export class MediaModule {}
