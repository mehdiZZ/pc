import { Injectable, Inject, OnApplicationShutdown } from '@nestjs/common';
import Redis from 'ioredis';
import { format } from 'util';
import { User } from '@backend/core/user/entities/user.entity';
import { REDIS_CONFIG_OPTS } from './constants';
import { Setting } from '@backend/modules/setting/entities';

class CacheKeys {
  readonly user: string = 'user:%s';
  readonly settings: string = 'settings';
}

@Injectable()
export class RedisService implements OnApplicationShutdown {
  readonly client: Redis.Redis;
  readonly cacheKeys: CacheKeys;

  constructor(@Inject(REDIS_CONFIG_OPTS) private redisOptions) {
    this.client = new Redis(this.redisOptions);
    this.cacheKeys = new CacheKeys();
  }

  onApplicationShutdown(): void {
    this.client.disconnect();
  }

  async getUser(userID: string): Promise<User> {
    const cacheKey = format(this.cacheKeys.user, userID);
    const userStr = await this.client.get(cacheKey);
    if (!userStr) {
      return null;
    }
    return JSON.parse(userStr);
  }

  async setUser(user: User) {
    const cacheKey = format(this.cacheKeys.user, user.id);
    return await this.client.set(
      cacheKey,
      JSON.stringify(user),
      'EX',
      3 * 24 * 60 * 60, // 3 days
    );
  }

  async delUser(userId: string): Promise<number> {
    const cacheKey = format(this.cacheKeys.user, userId);
    return await this.client.del(cacheKey);
  }

  async getSettings(): Promise<Setting[]> {
    const settings = await this.client.get(this.cacheKeys.settings);
    if (!settings) {
      return null;
    }
    return JSON.parse(settings);
  }

  async setSettings(settings: Setting[]) {
    return await this.client.set(
      this.cacheKeys.settings,
      JSON.stringify(settings),
      'EX',
      3 * 24 * 60 * 60, // 3 days
    );
  }
}
