import { ModuleMetadata, Type } from "@nestjs/common/interfaces";
import { RedisOptions } from "ioredis";

export interface RedisModuleAsyncOptions
  extends Pick<ModuleMetadata, "imports"> {
  inject?: any[];
  useClass?: Type<RedisOptionsFactory>;
  useExisting?: Type<RedisOptionsFactory>;
  useFactory?: (...args: any[]) => Promise<RedisOptions> | RedisOptions;
}

export interface RedisOptionsFactory {
  createRedisOptions(): Promise<RedisOptions> | RedisOptions;
}
