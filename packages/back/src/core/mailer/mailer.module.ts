import { Global, Module, DynamicModule, Provider } from "@nestjs/common";
import { MailerService } from "./mailer.service";
import { MAILER_CONFIG_OPTS } from "./constants";
import {
  MailerOptions,
  MailerModuleAsyncOptions,
  MailerOptionsFactory,
} from "./interfaces";

@Global()
@Module({
  providers: [MailerService],
  exports: [MailerService],
})
export class MailerModule {
  static forRoot(options: MailerOptions): DynamicModule {
    return {
      module: MailerModule,
      providers: [
        {
          provide: MAILER_CONFIG_OPTS,
          useValue: options,
        },
      ],
    };
  }

  static forRootAsync(options: MailerModuleAsyncOptions): DynamicModule {
    return {
      module: MailerModule,
      imports: options.imports || [],
      providers: this.createAsyncProviders(options),
    };
  }

  private static createAsyncProviders(
    options: MailerModuleAsyncOptions,
  ): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [this.createAsyncOptionsProvider(options)];
    }
    return [
      this.createAsyncOptionsProvider(options),
      {
        provide: options.useClass,
        useClass: options.useClass,
      },
    ];
  }

  private static createAsyncOptionsProvider(
    options: MailerModuleAsyncOptions,
  ): Provider {
    if (options.useFactory) {
      return {
        provide: MAILER_CONFIG_OPTS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      };
    }

    return {
      name: MAILER_CONFIG_OPTS,
      provide: MAILER_CONFIG_OPTS,
      useFactory: async (optionsFactory: MailerOptionsFactory) => {
        return optionsFactory.crateMailerOptions();
      },
      inject: [options.useExisting || options.useClass],
    };
  }
}
