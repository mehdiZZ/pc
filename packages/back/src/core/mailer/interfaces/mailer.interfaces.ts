import { ModuleMetadata, Type } from '@nestjs/common/interfaces';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface MailerOptions extends SMTPTransport.Options {}

export interface MailerModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  inject?: any[];
  useClass?: Type<MailerOptionsFactory>;
  useExisting?: Type<MailerOptionsFactory>;
  useFactory?: (...args: any[]) => Promise<MailerOptions> | MailerOptions;
}

export interface MailerOptionsFactory {
  crateMailerOptions(): Promise<MailerOptions> | MailerOptions;
}
