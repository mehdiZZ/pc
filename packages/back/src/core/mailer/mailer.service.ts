import {
  Injectable,
  Inject,
  InternalServerErrorException,
} from '@nestjs/common';
import { createTransport, Transporter, SendMailOptions } from 'nodemailer';
import { MAILER_CONFIG_OPTS } from './constants';
@Injectable()
export class MailerService {
  private transporter: Transporter;
  private clientIsValid: boolean;

  constructor(@Inject(MAILER_CONFIG_OPTS) private mailerOptions) {
    this.transporter = createTransport({
      host: mailerOptions.host,
      port: mailerOptions.port,
      // secure: false,
      auth: {
        user: mailerOptions.auth.user,
        pass: mailerOptions.auth.pass,
      },
      // tls: {
      //   ciphers: "SSLv3",
      // },
    });

    process.env.NODE_ENV === 'production' && this.verifyClient();
  }

  private async verifyClient() {
    try {
      this.clientIsValid = await this.transporter.verify();
    } catch (err) {
      this.clientIsValid = false;
      console.error('Email server not responding:');
      console.error(err);
    }
  }

  public async sendMail(sendMailOptions: SendMailOptions): Promise<boolean> {
    if (process.env.NODE_ENV === 'production' && !this.clientIsValid) {
      return false;
    }
    try {
      const options = { ...sendMailOptions, from: this.mailerOptions.from };
      await this.transporter.sendMail(options);
      return true;
    } catch (err) {
      console.log(err);
      throw new InternalServerErrorException('Can not send your email');
    }
  }
}
