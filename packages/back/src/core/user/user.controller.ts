import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './entities/';
import {
  CrudController,
  Crud,
  Override,
  ParsedRequest,
  CrudRequest,
  ParsedBody,
  CrudRequestInterceptor,
} from '@nestjsx/crud';
import { ApiBody, ApiTags, PartialType } from '@nestjs/swagger';
import { UserCreateReq, UserRegisterReq, UserUpdateReq } from './dto';
import { JwtGuard } from './_auth';
import { IRequest } from '@backend/common/interfaces';
import { PermissionGuard, PermissionsList } from './_permission';

@Crud({
  model: { type: User },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    exclude: ['createManyBase'],
    getOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('user.item-read'),
      ],
    },
    getManyBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('user.list-read'),
      ],
    },
    createOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('user.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('user.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('user.item-delete'),
      ],
      returnDeleted: true,
    },
  },
  query: {
    maxLimit: 100,
    limit: 20,
    alwaysPaginate: true,
    join: {
      roles: { eager: true },
      'roles.permissions': {},
      picture: {},
    },
    sort: [{ field: 'createdAt', order: 'DESC' }],
  },
})
@ApiTags('/v1/users')
@Controller('/v1/users')
export class UserController implements CrudController<User> {
  constructor(public service: UserService) {}

  get base(): CrudController<User> {
    return this;
  }

  @Override('createOneBase')
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UserCreateReq,
  ): Promise<User> {
    dto.password = await this.service.hashPassword(dto.password);
    return this.service.createOne(req, dto);
  }

  @Override('getOneBase')
  async getOne(@ParsedRequest() req: CrudRequest): Promise<User> {
    const user = await this.service.getOne(req);
    user.flatPermissions = await this.service.flatPermissionsList(user);
    return user;
  }

  @Override('updateOneBase')
  async updateOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UserUpdateReq,
  ): Promise<User> {
    if (dto.password) {
      dto.password = await this.service.hashPassword(dto.password);
    }
    const user = await this.service.updateOne(req, dto);

    this.service.updateRedis(user);

    return user;
  }

  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('user.item-update')
  @UseInterceptors(CrudRequestInterceptor)
  @Patch('profile/:id')
  async updateProfile(
    @ParsedRequest() req: CrudRequest,
    @Body() dto: UserUpdateReq,
    @Req() actualReq: IRequest,
    @Param('id') userId: string,
  ): Promise<User> {
    if (!userId || userId !== actualReq?.user?.id)
      throw new ForbiddenException();

    if (dto.password) {
      dto.password = await this.service.hashPassword(dto.password);
    }
    const user = await this.service.updateOne(req, dto);

    this.service.updateRedis(user);

    return user;
  }

  @HttpCode(201)
  @UseInterceptors(CrudRequestInterceptor)
  @Post('register')
  async register(
    @ParsedRequest() req: CrudRequest,
    @Body() dto: UserRegisterReq,
  ): Promise<User> {
    return await this.service.register(req, dto);
  }

  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('user.item-read')
  @Get('username/:username')
  async getByUserName(@Param('username') username: string): Promise<User> {
    return await this.service.getByUserName(username);
  }
}
