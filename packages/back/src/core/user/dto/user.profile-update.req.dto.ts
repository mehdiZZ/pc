import { OmitType } from '@nestjs/swagger';
import { UserUpdateReq } from './user.update.req.dto';

export class UserProfileUpdateReq extends OmitType(UserUpdateReq, [
  'roles',
] as const) {}
