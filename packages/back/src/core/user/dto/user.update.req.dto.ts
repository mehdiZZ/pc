import { PartialType } from '@nestjs/swagger';
import { UserCreateReq } from './user.create.req.dto';

export class UserUpdateReq extends PartialType(UserCreateReq) {
  id?: string; // only for frontend
}
