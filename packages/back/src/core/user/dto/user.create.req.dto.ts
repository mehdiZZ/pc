import { Match } from '@backend/common/decorators';
import { Media } from '@backend/core/media/entities';
import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsString,
  IsUUID,
  Length,
  Matches,
  MaxLength,
} from 'class-validator';
import { Role } from '../_role/entities';

export class UserCreateReq {
  @IsString()
  @MaxLength(40)
  username!: string;

  @IsEmail()
  @MaxLength(60)
  email!: string;

  @IsString()
  @Length(8, 30)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak',
  })
  password!: string;

  @IsString()
  @Length(8, 30)
  @Match('password')
  confirmPassword!: string;

  @IsBoolean()
  @IsOptional()
  isActive?: boolean;

  @IsOptional()
  picture?: Media[];

  @IsOptional()
  roles?: Role[];
}
