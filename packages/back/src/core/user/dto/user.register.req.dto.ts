import { Match } from '@backend/common/decorators';
import {
  IsEmail,
  IsOptional,
  IsString,
  Length,
  Matches,
  MaxLength,
} from 'class-validator';

export class UserRegisterReq {
  @IsEmail()
  @MaxLength(40)
  email!: string;

  @IsString()
  @Length(8, 30)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak',
  })
  password!: string;

  @IsString()
  @Length(8, 30)
  @Match('password')
  confirmPassword!: string;
}
