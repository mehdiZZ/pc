import { AuthService } from './auth.service';
import {
  AuthJwtTokenRes,
  AuthPasswordResetMailTokenReq,
  AuthLoginUserReq,
  AuthRefreshTokenReq,
  AuthPasswordResetNewPasswordReq,
  AuthActiveAccountMailTokenReq,
  AuthActiveAccountReq,
  AuthLogoutUserReq,
} from './dto';
import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiTags, ApiBody } from '@nestjs/swagger';
import { JwtBothTokens } from './interfaces';

@ApiTags('/v1/auth')
@Controller('/v1/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() input: AuthLoginUserReq): Promise<AuthJwtTokenRes> {
    return await this.authService.login(input);
  }

  @Post('logout')
  async logout(@Body() input: AuthLogoutUserReq): Promise<boolean> {
    return await this.authService.logout(input);
  }

  @Post('mail-reset-token')
  async passwordResetMailToken(
    @Body() input: AuthPasswordResetMailTokenReq,
  ): Promise<boolean> {
    return await this.authService.mailToken(input);
  }

  @Post('reset-password')
  async resetPassword(
    @Body() input: AuthPasswordResetNewPasswordReq,
  ): Promise<boolean> {
    return await this.authService.resetPassword(input);
  }

  @Post('mail-active-token')
  async activeAccountMailToken(
    @Body() input: AuthActiveAccountMailTokenReq,
  ): Promise<boolean> {
    return await this.authService.mailActiveToken(input.email);
  }

  @Post('active-account')
  async activeAccount(@Body() input: AuthActiveAccountReq): Promise<boolean> {
    return await this.authService.activeAccount(input);
  }

  // public route
  @Post('refresh-token')
  @HttpCode(201)
  async refreshToken(
    @Body() input: AuthRefreshTokenReq,
  ): Promise<JwtBothTokens> {
    return this.authService.refreshToken(input);
  }
}
