import {
  Injectable,
  ExecutionContext,
  // CanActivate,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from '@backend/core/user/entities/user.entity';

@Injectable()
export class JwtGuard extends AuthGuard('jwt') {
  async canActivate(context: ExecutionContext): Promise<any> {
    return super.canActivate(context);
  }

  handleRequest(err: any, user: User /*info*/): any {
    if (err || !user) {
      throw err || new UnauthorizedException('INVALID_TOKEN');
    }
    return user;
  }
}
