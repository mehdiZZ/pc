import { IsUUID } from 'class-validator';

export class AuthLogoutUserReq {
  @IsUUID(4)
  id: string;
}
