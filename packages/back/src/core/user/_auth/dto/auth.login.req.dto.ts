import { Length, MaxLength, IsEmail, IsString } from 'class-validator';

export class AuthLoginUserReq {
  @IsEmail()
  @MaxLength(40)
  email!: string;

  @IsString()
  @Length(8, 30)
  password!: string;
}
