import { IsString } from 'class-validator';

export class AuthRefreshTokenReq {
  @IsString()
  refreshToken!: string;
}
