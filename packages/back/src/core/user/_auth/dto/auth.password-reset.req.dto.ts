import {
  IsEmail,
  IsString,
  Length,
  Matches,
  IsNotEmpty,
} from 'class-validator';
import { Match } from '@backend/common/decorators';

export class AuthPasswordResetMailTokenReq {
  @IsEmail()
  @IsNotEmpty()
  email!: string;
}

export class AuthPasswordResetNewPasswordReq {
  @IsEmail()
  @IsNotEmpty()
  email!: string;

  @IsString()
  @IsNotEmpty()
  token!: string;

  @IsString()
  @Length(8, 30)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak',
  })
  password!: string;

  @IsString()
  @Length(8, 30)
  @Match('password')
  confirmPassword!: string;
}
