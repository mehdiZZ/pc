import { IsEmail, IsString } from 'class-validator';

export class AuthActiveAccountMailTokenReq {
  @IsEmail()
  email!: string;
}

export class AuthActiveAccountReq {
  @IsString()
  token!: string;
}
