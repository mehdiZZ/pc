export class AuthJwtTokenRes {
  tk: string;
  rtk: string;
  username: string;
  email: string;
  id: string;
  flatPermissions: string[];
}
