import { Injectable, ForbiddenException } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { AuthService } from '../auth.service';
import { User } from '@backend/core/user/entities/user.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get<string>('auth.jwtTokenSecret'),
    });
  }

  async validate({ sub }: { sub: string }): Promise<User | boolean> {
    const user = await this.authService.validToken(sub);

    if (!user) {
      throw new ForbiddenException();
    }

    return user;
  }
}
