export interface JwtPayload {
  sub: string;
  iat?: number;
  exp?: number;
}

export interface JwtRefreshPayload {
  sub: string;
  tv: number;
  //ip: string;
}

export interface JwtBothTokens {
  accessToken: string;
  refreshToken: string;
}
