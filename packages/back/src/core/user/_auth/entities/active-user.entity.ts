import { Entity, Column, ManyToOne } from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';
import { User } from '../../entities/user.entity';

@Entity({ name: 'active_user_tokens' })
export class ActiveUserToken extends ExtendedBaseEntity {
  @Column()
  token!: string;

  @Column('timestamptz', { name: 'expires_at' })
  expiresAt!: Date;

  @ManyToOne(() => User)
  user!: User;
}
