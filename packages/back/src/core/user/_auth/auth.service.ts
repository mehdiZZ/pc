import {
  Injectable,
  Inject,
  forwardRef,
  BadRequestException,
  NotFoundException,
  InternalServerErrorException,
  ForbiddenException,
  UnauthorizedException,
} from '@nestjs/common';
import { Connection, Raw } from 'typeorm';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import { ConfigService } from '@nestjs/config';
import { UserService } from '@backend/core/user/user.service';
import {
  AuthLoginUserReq,
  AuthJwtTokenRes,
  AuthPasswordResetMailTokenReq,
  AuthLogoutUserReq,
  AuthPasswordResetNewPasswordReq,
  AuthActiveAccountReq,
  AuthRefreshTokenReq,
} from './dto';
import { JwtPayload, JwtRefreshPayload, JwtBothTokens } from './interfaces';
import { RedisService } from '@backend/core/redis/redis.service';
import { User } from '@backend/core/user/entities/';
import { PasswordResetToken, ActiveUserToken } from './entities/';
import { MailerService } from '@backend/core/mailer/mailer.service';
import { JwtService } from '@nestjs/jwt';
import { __ } from 'i18n';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    private readonly configService: ConfigService,
    private readonly redisService: RedisService,
    private readonly jwtService: JwtService,
    private readonly mailerService: MailerService,
    private readonly connection: Connection,
  ) {}

  async login(input: AuthLoginUserReq): Promise<AuthJwtTokenRes> {
    const user = await this.userService.findOne(
      { email: input.email },
      { relations: ['roles'] },
    );

    if (!user) {
      throw new ForbiddenException(
        'Please check your credentials and try again',
      );
    }

    const areEqual = await this.userService.comparePassword(
      user.password,
      input.password,
    );

    if (!user.isActive) {
      throw new ForbiddenException(__('User is not active'));
    }

    if (areEqual) {
      const { refreshToken, accessToken } = await this.createTokens(user.id);
      return {
        tk: accessToken,
        rtk: refreshToken,
        id: user.id,
        username: user.username,
        email: user.email,
        flatPermissions: await this.userService.flatPermissionsList(user),
      };
    } else {
      throw new ForbiddenException(
        __('Please check your credentials and try again'),
      );
    }
  }

  async logout(input: AuthLogoutUserReq): Promise<boolean> {
    const user = await this.userService.incrementTokenVersion(input.id);
    this.redisService.delUser(input.id);
    if (user) return true;

    //
    throw new NotFoundException('User not found');
  }

  async createTokens(userId: string): Promise<JwtBothTokens> {
    const user = await this.userService.incrementTokenVersion(userId);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const payload: JwtPayload = {
      sub: user.id,
    };

    const refreshPayload: JwtRefreshPayload = {
      sub: user.id,
      tv: user.tokenVersion,
    };

    return {
      accessToken: this.jwtService.sign(payload),
      refreshToken: jwt.sign(
        refreshPayload,
        this.configService.get<string>('auth.jwtRefreshTokenSecret'),
        {
          expiresIn: this.configService.get<string>(
            'auth.jwtRefreshTokenExpiresIn',
          ),
        },
      ),
    };
  }

  async refreshToken(input: AuthRefreshTokenReq): Promise<JwtBothTokens> {
    try {
      const decodedToken = jwt.verify(
        input.refreshToken,
        this.configService.get<string>('auth.jwtRefreshTokenSecret'),
      ) as JwtRefreshPayload;

      const { sub, tv } = decodedToken;

      const user = await this.userService.findOne({ id: sub });
      if (tv !== user.tokenVersion) {
        throw new ForbiddenException('Invalid user data');
      }
      return this.createTokens(user.id);
    } catch (err) {
      throw new ForbiddenException('Token is invalid');
    }
  }

  async validToken(sub: string): Promise<User | boolean> {
    const cachedUser: User = await this.redisService.getUser(sub);
    if (cachedUser) {
      return cachedUser;
    } else {
      const user: User = await this.userService.findOne(
        {
          id: sub,
        },
        {
          relations: ['roles'],
        },
      );

      if (!user.isActive) return false;

      user.flatPermissions = await this.userService.flatPermissionsList(user);

      if (user.password) delete user.password;

      await this.redisService.setUser(user);

      return user;
    }
  }

  async mailToken(input: AuthPasswordResetMailTokenReq): Promise<boolean> {
    //
    const user: User = await User.findOne({ where: { username: input.email } });
    if (!user) {
      throw new NotFoundException('Email does not exists');
    }
    //
    const alreadyHasToken = await this.connection
      .getRepository(PasswordResetToken)
      .findOne({
        expiresAt: Raw((alias) => `${alias} > NOW()`),
        user,
      });

    if (alreadyHasToken) {
      throw new BadRequestException('We already sent you an email');
    }
    //
    const expiresAt = new Date();
    expiresAt.setMinutes(expiresAt.getMinutes() + 20);

    const token = crypto.randomBytes(20).toString('hex');

    const userToken = await PasswordResetToken.insert({
      user,
      token,
      expiresAt,
    });

    if (userToken) {
      // send email
      const subject = 'request for change password';
      const to = user.email;
      //const from = process.env.FROM_EMAIL;
      const link = process.env.WEBSITE_URL + 'forgot-password/' + token;
      const html = `<p>Hi 🙋🏿‍♀️</p>
                    <p>Please click on the following <a href="${link}">link</a> to reset your password.</p>
                    <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>`;

      const res = await this.mailerService.sendMail({
        subject,
        to,
        html,
      });

      if (!res) {
        await PasswordResetToken.delete({ token });
        throw new InternalServerErrorException(
          'Can not send your email. please try again',
        );
      }
      return true;
    }

    throw new InternalServerErrorException(
      'Can not send your email. please try again',
    );
  }

  async resetPassword(
    input: AuthPasswordResetNewPasswordReq,
  ): Promise<boolean> {
    const token = await PasswordResetToken.findOne({
      where: {
        token: input.token,
        expiresAt: Raw((alias) => `${alias} > NOW()`),
      },
      relations: ['user'],
    });

    if (!token || !token.user || token.user.email !== input.email)
      throw new NotFoundException('Token is invalid');

    token.user.password = await this.userService.hashPassword(input.password);

    await token.user.save();
    await token.remove();
    return true;
  }

  async mailActiveToken(email: string): Promise<boolean> {
    const user: User = await User.findOne({ where: { username: email } });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    //
    const alreadyHasToken = await this.connection
      .getRepository(ActiveUserToken)
      .findOne({
        expiresAt: Raw((alias) => `${alias} > NOW()`),
        user,
      });

    if (alreadyHasToken) {
      throw new BadRequestException('We already sent you an email');
    }
    //
    const expiresAt = new Date();
    expiresAt.setMinutes(expiresAt.getMinutes() + 20);

    const token = crypto.randomBytes(20).toString('hex');

    const UserToken = await ActiveUserToken.insert({
      user,
      token,
      expiresAt,
    });

    if (UserToken) {
      // send email
      const subject = 'active your account';
      const to = user.email;
      //const from = process.env.FROM_EMAIL;
      const link = process.env.WEBSITE_URL + '/auth/active/' + token;
      const html = `<p>Hi</p>
                    <p>Please click on the following <a href="${link}">link</a> to active your account.</p>
                    <p>If you did not request this email.</p>`;

      return await this.mailerService.sendMail({
        subject,
        to,
        html,
      });
    }
  }

  async activeAccount(input: AuthActiveAccountReq): Promise<boolean> {
    const token = await ActiveUserToken.findOne({
      where: {
        token: input.token,
        expiresAt: Raw((alias) => `${alias} > NOW()`),
      },
      relations: ['user'],
    });

    if (!token || !token.user) throw new NotFoundException('Token is invalid');

    token.user.isActive = true;

    await token.user.save();
    await token.remove();
    return true;
  }
}
