import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/';
import { RoleService } from './_role/role.service';
import { RedisService } from '@backend/core/redis/redis.service';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { UserRegisterReq } from './dto';

@Injectable()
export class UserService extends TypeOrmCrudService<User> {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    private readonly roleService: RoleService,
    private readonly redisService: RedisService,
  ) {
    super(userRepo);
  }

  async updateRedis(user: User): Promise<void> {
    if (await this.redisService.getUser(user.id)) {
      const flatPermissions = await this.flatPermissionsList(user);
      this.redisService.setUser({ ...user, flatPermissions } as User);
    }
  }

  async hashPassword(password: string): Promise<string> {
    return await bcrypt.hash(password, 10);
  }

  async comparePassword(
    dbPassword: string,
    inputPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compare(inputPassword, dbPassword);
  }

  async flatPermissionsList(user: User): Promise<string[]> {
    if (!user || !user.roles) return [];

    const roleIds = user.roles.map((r) => r.id);
    const permissions = await this.roleService.permissionsByRoleIds(roleIds);

    if (!permissions) return [];

    return [...new Set(permissions.map((permission) => permission.slug))];
  }

  async incrementTokenVersion(id: string): Promise<User> {
    const user: User = await this.userRepo.findOne(id);
    user.tokenVersion += 1;
    return this.userRepo.save(user);
  }

  async register(req: CrudRequest, dto: UserRegisterReq): Promise<User> {
    const { email } = dto;
    const user = await this.userRepo.findOne({
      where: {
        email,
      },
    });

    if (user) {
      throw new BadRequestException('User already exists');
    }

    //if already have an admin
    // if (dto.isAdmin === true) {
    //   const hasAdmin = await this.userRepo.findOne({ isAdmin: true });
    //   if (hasAdmin) dto.isAdmin = false;
    // }

    dto.password = await this.hashPassword(dto.password);
    delete dto['confirmPassword'];
    return await super.createOne(req, { ...dto, email, username: email });
  }

  async getByUserName(username: string): Promise<User> {
    const user = await this.userRepo.findOne(
      { username },
      { relations: ['roles', 'picture', 'picture.cropped'] },
    );
    if (!user) throw new NotFoundException('User not found');
    return user;
  }

  async getOneByUsername(username: string): Promise<User> {
    return this.userRepo.findOne({
      relations: ['roles', 'picture'],
      where: { username },
    });
  }
}
