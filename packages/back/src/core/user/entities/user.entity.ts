import { Entity, Column, ManyToMany, JoinTable, Index } from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';
import { Role } from '../_role/entities/role.entity';
//

import { Exclude } from 'class-transformer';
import { Media } from '@backend/core/media/entities';

@Entity('users')
export class User extends ExtendedBaseEntity {
  @Column({ type: 'varchar', length: 100, unique: true, name: 'user_name' })
  @Index({ unique: true })
  username!: string;

  @Column({ type: 'varchar', length: 100 })
  @Exclude({ toPlainOnly: true })
  password!: string;

  @Column({ type: 'varchar', length: 100, unique: true })
  @Index({ unique: true })
  email!: string;

  @Column({ type: 'boolean', default: false, name: 'is_active' })
  isActive?: boolean;

  @Column({ nullable: true, default: 0, name: 'token_version' })
  @Exclude()
  tokenVersion?: number;

  // @IsOptional({ always: true })
  // @IsBoolean({ always: true })
  // @Column({ type: 'boolean', default: false, name: 'is_admin' })
  // isAdmin?: boolean;

  @ManyToMany(() => Role, (role) => role.users)
  @JoinTable({
    name: 'users_roles',
    joinColumn: {
      name: 'user_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'role_id',
      referencedColumnName: 'id',
    },
  })
  roles?: Role[];

  @ManyToMany(() => Media, (media) => media.users)
  @JoinTable({
    name: 'users_pictures',
    joinColumn: {
      name: 'user_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'media_id',
      referencedColumnName: 'id',
    },
  })
  picture?: Media[];

  flatPermissions?: string[];
}
