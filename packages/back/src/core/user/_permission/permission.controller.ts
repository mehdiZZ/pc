import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';
//
import { JwtGuard } from '@backend/core/user/_auth/guards';
import { UseGuards, Controller, Get, Param } from '@nestjs/common';
//
import { Permission } from './entities/';
import { PermissionService } from './permission.service';
import { PermissionCreateReq, PermissionUpdateReq } from './dto';
import { PermissionsList } from './decorators';
import { PermissionGuard } from './guards';

@Crud({
  model: { type: Permission },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    exclude: ['createManyBase'],
    getManyBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('permission.list-read'),
      ],
    },
    getOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('permission.item-read'),
      ],
    },
    createOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('permission.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('permission.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('permission.item-delete'),
      ],
      returnDeleted: true,
    },
  },
  dto: {
    create: PermissionCreateReq,
    update: PermissionUpdateReq,
  },
  query: {
    maxLimit: 100,
    limit: 20,
    alwaysPaginate: true,
    // join: {
    //   roles: {},
    // },
    sort: [
      { field: 'createdAt', order: 'DESC' },
      { field: 'slug', order: 'ASC' },
    ],
  },
})
@ApiTags('/v1/permissions')
@Controller('/v1/permissions')
export class PermissionController implements CrudController<Permission> {
  constructor(public service: PermissionService) {}

  get base(): CrudController<Permission> {
    return this;
  }

  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('permission.item-read')
  @Get('slug/:slug')
  async getBySlug(@Param('slug') slug: string) {
    return await this.service.getBySlug(slug);
  }
}
