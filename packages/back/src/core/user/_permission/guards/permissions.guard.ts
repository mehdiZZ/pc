import { Reflector } from '@nestjs/core';
import {
  ExecutionContext,
  CanActivate,
  Injectable,
  ForbiddenException,
} from '@nestjs/common';
import { User } from '@backend/core/user/entities/user.entity';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const permissions = this.reflector.get<string[]>(
      'permissions',
      context.getHandler(),
    );

    const req = context.switchToHttp().getRequest();

    const user: User | undefined = req?.user;

    if (!user) throw new ForbiddenException('User is not authenticated');

    // if (user.isAdmin) return true;

    if (
      !user.isActive ||
      !user.flatPermissions ||
      !user.flatPermissions.length
    ) {
      throw new ForbiddenException('You can not access this route');
    }

    return user.flatPermissions.some(
      (permission) => permissions && permissions.includes(permission),
    );
  }
}
