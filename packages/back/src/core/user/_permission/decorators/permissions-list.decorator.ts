import { IPermissionSlug } from '@backend/core/seed';
import { SetMetadata, CustomDecorator } from '@nestjs/common';

export const PermissionsList = (
  ...permissions: IPermissionSlug[]
): CustomDecorator<string> => {
  return SetMetadata('permissions', permissions);
};
