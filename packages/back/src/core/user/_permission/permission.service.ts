import { Repository, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Permission } from './entities/';

@Injectable()
export class PermissionService extends TypeOrmCrudService<Permission> {
  constructor(
    @InjectRepository(Permission)
    private readonly permissionRepo: Repository<Permission>,
  ) {
    super(permissionRepo);
  }

  // async transSlugsToIds(slugs: string[]): Promise<{ id: string }[]> {
  //   let permissionIds: { id: string }[] = [];

  //   const permissions = await this.permissionRepo.find({
  //     slug: In(slugs),
  //   });

  //   if (permissions && permissions.length > 0) {
  //     permissionIds = permissions.map(p => {
  //       return { id: p.id };
  //     });
  //   }

  //   return permissionIds;
  // }

  async getBySlug(slug: string): Promise<Permission> {
    const permission = await this.permissionRepo.findOne({ slug });
    if (!permission) throw new NotFoundException('Permission not found');
    return permission;
  }
}
