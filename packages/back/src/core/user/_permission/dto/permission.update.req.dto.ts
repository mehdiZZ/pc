import { PartialType } from '@nestjs/swagger';
import { PermissionCreateReq } from './permission.create.req.dto';

export class PermissionUpdateReq extends PartialType(PermissionCreateReq) {
  id?: string; // for frontend only
}
