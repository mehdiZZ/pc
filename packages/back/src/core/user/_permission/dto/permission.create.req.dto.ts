import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class PermissionCreateReq {
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  name!: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  slug!: string;
}
