import { Entity, Column, ManyToMany, Index } from 'typeorm';
import { Role } from '@backend/core/user/_role/entities/role.entity';
import { ExtendedBaseEntity } from '@backend/common/base';
//
import { ApiHideProperty } from '@nestjs/swagger';
//
@Entity('permissions')
export class Permission extends ExtendedBaseEntity {
  @Column({ type: 'varchar', length: 100, unique: true })
  name!: string;

  @Column({ type: 'varchar', length: 100, unique: true })
  @Index({ unique: true })
  slug!: string;

  @ApiHideProperty()
  @ManyToMany(() => Role, (role) => role.permissions)
  roles?: Role[];
}
