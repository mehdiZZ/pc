import { Entity, Column, ManyToMany, JoinTable, Index } from 'typeorm';
import { ExtendedBaseEntity } from '@backend/common/base';
import { User } from '@backend/core/user/entities/user.entity';
import { Permission } from '@backend/core/user/_permission/entities/permission.entity';
//

@Entity('roles')
export class Role extends ExtendedBaseEntity {
  @Column({ type: 'varchar', length: 100, unique: true })
  name!: string;

  @Column({ type: 'varchar', length: 100, unique: true })
  @Index({ unique: true })
  slug!: string;

  @ManyToMany(() => Permission, (permission) => permission.roles)
  @JoinTable({
    name: 'roles_permissions',
    joinColumn: {
      name: 'role_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'permission_id',
      referencedColumnName: 'id',
    },
  })
  permissions?: Permission[];

  @ManyToMany(() => User, (user) => user.roles)
  users?: User[];
}
