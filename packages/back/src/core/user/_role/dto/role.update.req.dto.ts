import { PartialType } from '@nestjs/swagger';
import { RoleCreateReq } from './role.create.req.dto';

export class RoleUpdateReq extends PartialType(RoleCreateReq) {
  id?: string; // for frontend only
}
