import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { Permission } from '../../_permission/entities';

export class RoleCreateReq {
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  name!: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  slug!: string;

  @IsOptional()
  permissions?: Permission[];
}
