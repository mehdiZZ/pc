import { Repository, In } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Permission } from '@backend/core/user/_permission/entities/permission.entity';

import { Role } from './entities/';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class RoleService extends TypeOrmCrudService<Role> {
  constructor(
    @InjectRepository(Role)
    private readonly roleRepo: Repository<Role>,
  ) {
    super(roleRepo);
  }

  async permissionsByRoleIds(
    roleIds: string[],
  ): Promise<Permission[] | undefined> {
    const roles = await this.roleRepo.findByIds(roleIds, {
      relations: ['permissions'],
    });

    let nextPermissions: Permission[] = [];

    roles.forEach((r) => {
      if (r.permissions && r.permissions.length > 0) {
        nextPermissions = nextPermissions.concat(r.permissions);
      }
    });

    return nextPermissions;
  }

  // async getOneBySlug(slug: string): Promise<Role | undefined> {
  //   return this.roleRepo.findOne({
  //     relations: ['permissions'],
  //     where: { slug },
  //   });
  // }

  // async transSlugsToIds(slugs: string[]): Promise<{ id: string }[]> {
  //   let roleIds: { id: string }[] = [];

  //   const roles = await this.roleRepo.find({
  //     slug: In(slugs),
  //   });

  //   if (roles && roles.length > 0)
  //     roleIds = roles.map((p) => {
  //       return { id: p.id };
  //     });

  //   return roleIds;
  // }

  async getBySlug(slug: string): Promise<Role> {
    const role = await this.roleRepo.findOne(
      { slug },
      { relations: ['permissions'] },
    );
    if (!role) throw new NotFoundException(slug);
    return role;
  }

  // async createRole(role: CreateRoleInput): Promise<Role> {
  //   try {
  //     const relations: { permissions?: Permission[] } = {};

  //     if (role.permissionIds) {
  //       const rolePermissions = await this.permissionService.findByIds(
  //         role.permissionIds,
  //       );
  //       if (rolePermissions) {
  //         relations.permissions = rolePermissions;
  //         return await this.create(role, relations);
  //       } else {
  //         throw new Error();
  //       }
  //     } else {
  //       return await this.create(role);
  //     }
  //   } catch {
  //     throw new BadRequestException('check your input data');
  //   }
  // }

  // async updateRole(role: UpdateRoleInput): Promise<Role> {
  //   const { permissionIds = [], id, ...others } = role;

  //   const newRole: DeepPartial<Role> = others;

  //   if (permissionIds.length > 0) {
  //     const relations: { permissions?: Permission[] } = {};

  //     const rolePermissions: Permission[] = await this.permissionService.findByIds(
  //       permissionIds,
  //     );
  //     if (rolePermissions) {
  //       relations.permissions = rolePermissions;
  //       return await this.update(id, newRole, relations);
  //     } else {
  //       throw new BadRequestException();
  //     }
  //   } else {
  //     return await this.update(id, newRole);
  //   }
  // }
}
