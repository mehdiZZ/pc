import { Crud, CrudController } from '@nestjsx/crud';
//
import { Role } from './entities/';
import { RoleService } from './role.service';
import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { RoleCreateReq, RoleUpdateReq } from './dto';
import { JwtGuard } from '../_auth';
import { PermissionGuard, PermissionsList } from '../_permission';

@Crud({
  model: { type: Role },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  routes: {
    exclude: ['createManyBase'],
    getOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('role.item-read'),
      ],
    },
    getManyBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('role.list-read'),
      ],
    },
    createOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('role.item-create'),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('role.item-update'),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(JwtGuard, PermissionGuard),
        PermissionsList('role.item-delete'),
      ],
      returnDeleted: true,
    },
  },
  dto: {
    create: RoleCreateReq,
    update: RoleUpdateReq,
  },
  query: {
    maxLimit: 100,
    limit: 20,
    alwaysPaginate: true,
    join: {
      permissions: { eager: true },
    },
    sort: [
      { field: 'createdAt', order: 'DESC' },
      { field: 'slug', order: 'ASC' },
    ],
  },
})
@ApiTags('/v1/roles')
@Controller('/v1/roles')
export class RoleController implements CrudController<Role> {
  constructor(public service: RoleService) {}

  get base(): CrudController<Role> {
    return this;
  }

  @UseGuards(JwtGuard, PermissionGuard)
  @PermissionsList('role.item-read')
  @Get('slug/:slug')
  async getBySlug(@Param('slug') slug: string) {
    return await this.service.getBySlug(slug);
  }
}
