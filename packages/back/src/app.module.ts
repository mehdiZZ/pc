import { Module } from '@nestjs/common';
//configs
import { ConfigModule } from '@nestjs/config';
//
import GlobalConfig from '@backend/config/global.config';
import AuthConfig from '@backend/config/auth.config';
//
import { PermissionModule } from '@backend/core/user/_permission/permission.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  TypeormOptionsFactory,
  RedisOptionsFactory,
  MailerOptionsFactory,
} from '@backend/config/factories';
import { RoleModule } from '@backend/core/user/_role/role.module';
import { UserModule } from '@backend/core/user/user.module';
import { RedisModule } from '@backend/core/redis/redis.module';
import { MailerModule } from '@backend/core/mailer/mailer.module';
import { AuthModule } from '@backend/core/user/_auth/auth.module';
import { SeedModule } from 'core/seed/seed.module';
import { TaxonomyModule } from '@backend/modules/content/_taxonomy/taxonomy.module';
import { ContentModule } from 'modules/content/content.module';
import { MediaModule } from '@backend/core/media/media.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { CommentModule } from './modules/content/_comment/comment.module';
import { SettingModule } from './modules/setting/setting.module';

const configs = [GlobalConfig, AuthConfig];

const otherModules = [
  //user stuff
  UserModule,
  RoleModule,
  PermissionModule,
  AuthModule,
  // common
  SeedModule,
  // CommonModule,
  MediaModule,
  //content
  ContentModule,
  // settings
  SettingModule,
];

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: configs,
    }),
    TypeOrmModule.forRootAsync({
      useClass: TypeormOptionsFactory,
    }),
    RedisModule.forRootAsync({
      useClass: RedisOptionsFactory,
    }),
    MailerModule.forRootAsync({
      useClass: MailerOptionsFactory,
    }),
    // ServeStaticModule.forRoot({
    //   rootPath: join(__dirname, '../..', 'front/public'),
    //   serveRoot: '/public',
    //   serveStaticOptions: { index: false },
    // }),
    ...otherModules,
  ],
})
export class AppModule {}
